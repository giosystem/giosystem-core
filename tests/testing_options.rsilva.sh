#!/usr/bin/env bash

# initialization for py.test tests
#
# Instead of using several pytest.ini, we load the correspondent variables
# as environment variables. This makes it easier to store different settings
# for testing in a code repository.
#
# Be sure to source this file before running the tests.
#
# Run py.test -h in order to discover which options are available.

export PYTEST_ADDOPTS="\
    --capture=no \
    --verbose \
    --catalogue-type=geonetwork
    --catalogue-url=http://geoland2.meteo.pt/geonetwork \
    --catalogue-user=admin \
    --catalogue-password=g2admin1234 \
    --catalogue-sample-record-path=/home/ricardo/dev/copsystem/giosystem-core/tests/data/sample_record.xml \
"
