#!/bin/bash

# A simple program used to test the GIOLocalHost's ability to run external
# programs.

sleep_time=1
iterations=2

echo "Running the bash test program..."
for i in $(seq $iterations); do
    echo "iteration $i"
    sleep $sleep_time
done
echo "Done!"
