!A simple program used to test the GIOLocalHost's ability to run external
!programs.
program example_program
      integer :: i
      integer :: sleep_seconds = 1
      character*20 :: shell_comm

      write(*,*),"Running the FORTRAN test program..."
      !write(*,*),"outra coisa"
      do i=0, 1
          write(*,'(a11,i3)'),"iteration: ",i
          write(shell_comm,'(a6,i3)'),"sleep ",sleep_seconds
          call system(trim(shell_comm))
      end do
      write(*,*),"Done!"
end program
