/*
 * A simple program used to test the GIOLocalHost's ability to run external
 * programs.
 */

#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

int main()
{
    cout << "Running the cpp test program...\n";
    int sleep_time = 1;
    int iterations = 2;
    for(int i=0; i < iterations; i++)
    {
        cout << "iteration " << i+1 << endl;
        this_thread::sleep_for(chrono::seconds(sleep_time));
    }
    cout << "Done!\n";
    return 0;
}
