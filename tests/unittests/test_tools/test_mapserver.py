"""Unit tests for the giosystemcore.tools.mapserver package."""

import pytest
import mock

from giosystemcore.tools.mapserver import mapserver


@pytest.mark.unit
class TestMapfileLayer(object):

    def test_mapfilelayer_from_layer_obj_no_bands(self):
        fake_name = "fake layer name"
        fake_data = "/fake/data/path"
        fake_projection = "fake_projection"
        fake_bands = None
        fake_numclasses = 10
        fake_class = "Nothing"
        fake_metadata = {
            "ows_title": "fake ows title",
            "ows_layer_group": "fake ows layer_group",
            "ows_abstract": "fake abstract"
        }
        fake_copernicus_id = "no_id"
        fake_timeslot = "no time"

        id_path = ("giosystemcore.tools.mapserver.mapserver."
                   "retrieve_copernicus_product_identifier")
        timeslot_path = ("giosystemcore.tools.mapserver.mapserver."
                         "retrieve_timeslot")
        metadata_path = ("giosystemcore.tools.mapserver.mapserver."
                         "get_node_metadata")
        layer_obj_path = ("giosystemcore.tools.mapserver.mapserver."
                          "mapscript.layerObj")
        class_obj_path = ("giosystemcore.tools.mapserver.mapserver."
                          "mapscript.classObj")

        with mock.patch(id_path, autospec=True) as mocked_retrieve_id, \
                mock.patch(timeslot_path,
                           autospec=True) as mocked_retrieve_ts, \
                mock.patch(metadata_path,
                           autospec=True) as mocked_get_metadata, \
                mock.patch(layer_obj_path,
                           autospec=True) as mocked_layer_obj, \
                mock.patch(class_obj_path, autospec=True) as mocked_class_obj:

            mocked_get_metadata.return_value = fake_metadata
            mocked_retrieve_id.return_value = fake_copernicus_id
            mocked_retrieve_ts.return_value = fake_timeslot
            mock_class_obj = mocked_class_obj.return_value
            mock_class_obj.convertToString.return_value = fake_class
            mock_layer_obj = mocked_layer_obj.return_value
            mock_layer_obj.name = fake_name
            mock_layer_obj.data = fake_data
            mock_layer_obj.getProjection.return_value = fake_projection
            mock_layer_obj.getProcessingKey.return_value = fake_bands
            mock_layer_obj.getClass.return_value = mock_class_obj
            mock_layer_obj.numclasses = fake_numclasses
            layer = mapserver.MapfileLayer.from_layer_obj(mock_layer_obj)

            assert layer.name == fake_name
            assert layer.data_path == fake_data
            assert layer.projection == fake_projection
            assert layer.title == fake_metadata["ows_title"]
            assert layer.copernicus_product_id == fake_copernicus_id
            assert layer.timeslot == fake_timeslot
            assert layer.bands == fake_bands
            assert layer.classes == [fake_class] * fake_numclasses
            assert layer.layer_group == fake_metadata["ows_layer_group"]

    def test_mapfilelayer_from_layer_obj_one_band(self):
        fake_name = "fake layer name"
        fake_data = "/fake/data/path"
        fake_projection = "fake_projection"
        fake_band = 100  # just an integer, the actual value is not relevant
        fake_numclasses = 10
        fake_class = "Nothing"
        fake_metadata = {
            "ows_title": "fake ows title",
            "ows_layer_group": "fake ows layer_group",
            "ows_abstract": "fake abstract"
        }
        fake_copernicus_id = "no_id"
        fake_timeslot = "no time"

        id_path = ("giosystemcore.tools.mapserver.mapserver."
                   "retrieve_copernicus_product_identifier")
        timeslot_path = ("giosystemcore.tools.mapserver.mapserver."
                         "retrieve_timeslot")
        metadata_path = ("giosystemcore.tools.mapserver.mapserver."
                         "get_node_metadata")
        layer_obj_path = ("giosystemcore.tools.mapserver.mapserver."
                          "mapscript.layerObj")
        class_obj_path = ("giosystemcore.tools.mapserver.mapserver."
                          "mapscript.classObj")

        with mock.patch(id_path, autospec=True) as mocked_retrieve_id, \
                mock.patch(timeslot_path,
                           autospec=True) as mocked_retrieve_ts, \
                mock.patch(metadata_path,
                           autospec=True) as mocked_get_metadata, \
                mock.patch(layer_obj_path,
                           autospec=True) as mocked_layer_obj, \
                mock.patch(class_obj_path, autospec=True) as mocked_class_obj:

            mocked_get_metadata.return_value = fake_metadata
            mocked_retrieve_id.return_value = fake_copernicus_id
            mocked_retrieve_ts.return_value = fake_timeslot
            mock_class_obj = mocked_class_obj.return_value
            mock_class_obj.convertToString.return_value = fake_class
            mock_layer_obj = mocked_layer_obj.return_value
            mock_layer_obj.name = fake_name
            mock_layer_obj.data = fake_data
            mock_layer_obj.getProjection.return_value = fake_projection
            mock_layer_obj.getProcessingKey.return_value = str(fake_band)
            mock_layer_obj.getClass.return_value = mock_class_obj
            mock_layer_obj.numclasses = fake_numclasses
            layer = mapserver.MapfileLayer.from_layer_obj(mock_layer_obj)

            assert layer.name == fake_name
            assert layer.data_path == fake_data
            assert layer.projection == fake_projection
            assert layer.title == fake_metadata["ows_title"]
            assert layer.copernicus_product_id == fake_copernicus_id
            assert layer.timeslot == fake_timeslot
            assert layer.bands == [fake_band]
            assert layer.classes == [fake_class] * fake_numclasses
            assert layer.layer_group == fake_metadata["ows_layer_group"]

    def test_retrieve_copernicus_product_identifier(self):
        fake_copernicus_id = ("urn:cgls:global:lst_v1_0.045degree:"
                              "LST_201602210000_GLOBE_GEO_V1.2")
        fake_abstract = "Copernicus product ID: {}".format(fake_copernicus_id)
        retrieved = mapserver.retrieve_copernicus_product_identifier(
            fake_abstract)
        assert retrieved == fake_copernicus_id



