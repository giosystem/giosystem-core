"""Unit tests for the giosystemcore.hosts.proxies.sshproxy module."""

import unittest

from giosystemcore.hosts.proxies import sshproxy


class TestSSHProxy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.user = "dummy user"
        cls.ip = "dummy ip address"

    def setUp(self):
        self.instance = sshproxy.SSHProxy(user=self.user, ip=self.ip)

    def test_setup(self):
        user = "a"
        host = "b"
        other_options = "c"
        self.instance.setup(user=user, host=host, other_options=other_options)
        self.assertEqual(user, self.instance.user)
        self.assertEqual(host, self.instance.host)
        self.assertEqual(other_options, self.instance.other_options)

    @unittest.skip("not implemented")
    def test_connect_first_time(self):
        raise NotImplementedError

    @unittest.skip("not implemented")
    def test_connect_already_connected(self):
        raise NotImplementedError

    @unittest.skip("not implemented")
    def test_run_command(self):
        raise NotImplementedError

    @unittest.skip("not implemented")
    def test_list(self):
        raise NotImplementedError
