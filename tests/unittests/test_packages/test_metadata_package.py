"""
Unit tests for the giosystemcore package that generates metadata records.
"""

import pytest
import mock

from giosystemcore.packages import metadata
from giosystemcore.catalogue import cswinterface


@pytest.mark.unit
class TestMetadataCreator(object):

    def test_get_catalogues(self):
        fake_catalogue_instance = "fake"
        fake_name1 = "fake_catalogue1"
        fake_type1 = "geonetwork"
        fake_endpoint1 = "http://fake/endpoint1"
        fake_user1 = "fake_user1"
        fake_pass1 = "fake_password1"
        fake_name2 = "fake_catalogue2"
        fake_type2 = "pycsw"
        fake_endpoint2 = "http://fake/endpoint2"
        fake_user2 = "fake_user2"
        fake_pass2 = "fake_password2"
        package = metadata.MetadataCreator("test_package")
        package.parameters = {
            fake_name1: " ".join((fake_type1, fake_endpoint1, fake_user1,
                                 fake_pass1)),
            fake_name2: " ".join((fake_type2, fake_endpoint2, fake_user2,
                                  fake_pass2))
        }
        with mock.patch(
                "giosystemcore.packages.metadata.cswinterface.get_catalogue",
                autospec=True) as mock_get_catalogue:
            mock_get_catalogue.return_value = fake_catalogue_instance
            catalogues = package.get_catalogues()
            assert list(catalogues) == [fake_catalogue_instance,
                                        fake_catalogue_instance]
            # these assertions only work after consuming the `catalogues`
            # generator
            mock_get_catalogue.assert_any_call(
                cswinterface.CatalogueType.GEONETWORK,
                fake_endpoint1,
                user=fake_user1,
                password=fake_pass1)
            mock_get_catalogue.assert_any_call(
                cswinterface.CatalogueType.PYCSW,
                fake_endpoint2,
                user=fake_user2,
                password=fake_pass2)

    def test_execute(self):
        fetched = {
            "fake_gio_file1": "fake_path1",
            "fake_gio_file2": "fake_path2"
        }
        fake_metadata = "fake"

        fake_catalogue = mock.create_autospec(cswinterface.CswInterfacePycsw,
                                              instance=True)
        fake_catalogue.replace_record.return_value = (True, "")

        with mock.patch("giosystemcore.packages.metadata.metadata."
                        "NetCdfMetadataGenerator",
                        autospec=True) as mock_generator:
            mock_gen = mock_generator.return_value
            mock_gen.generate_metadata.return_value = fake_metadata
            package = metadata.MetadataCreator("test_package")
            with mock.patch.object(package, "get_catalogues") as mocked:
                mocked.return_value = [fake_catalogue]
                result, details = package.execute(fetched=fetched)
                print("fake_catalogue.replace_record called {} "
                      "times".format(fake_catalogue.replace_record.call_count))
                assert result
                call_args = fake_catalogue.replace_record.call_args_list
                for call_args, call_kwargs in call_args:
                    assert call_args == (fake_metadata,)
