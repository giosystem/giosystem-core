"""Tests for giosystemcore.hosts.hostfactory"""

import pytest
import mock

from giosystemcore.hosts import hostfactory
from giosystemcore.errors import InvalidSettingsError


@pytest.mark.unit
def test_get_host_no_name_no_settings():
    fake_name = "fake"
    with mock.patch.multiple("giosystemcore.hosts.hostfactory",
                             _hosts={},
                             get_settings=mock.DEFAULT,
                             hosts=mock.DEFAULT,
                             socket=mock.DEFAULT) as mocks:
        mocks["get_settings"].return_value.get_host_settings.side_effect = (
            InvalidSettingsError())
        mocks["socket"].gethostname.return_value = fake_name
        mock_host_class = mocks["hosts"].GioLocalhost
        h = hostfactory.get_host()
        assert mock_host_class.called_with(fake_name)


@pytest.mark.unit
def test_get_host_no_name_with_settings_local():
    fake_name = "fake"
    with mock.patch.multiple("giosystemcore.hosts.hostfactory",
                             _hosts={},
                             get_settings=mock.DEFAULT,
                             hosts=mock.DEFAULT,
                             socket=mock.DEFAULT) as mocks:
        mocks["socket"].gethostname.return_value = fake_name
        mocks["get_settings"].return_value.get_host_settings.return_value = {
            "active": True}
        mock_host_class = mocks["hosts"].GioLocalhost
        h = hostfactory.get_host()
        assert mock_host_class.called_with(fake_name)


@pytest.mark.unit
def test_get_host_with_name_with_settings_local():
    fake_name = "fake"
    with mock.patch.multiple("giosystemcore.hosts.hostfactory",
                             _hosts={},
                             get_settings=mock.DEFAULT,
                             hosts=mock.DEFAULT,
                             socket=mock.DEFAULT) as mocks:
        mocks["socket"].gethostname.return_value = fake_name
        mocks["get_settings"].return_value.get_host_settings.return_value = {
            "active": True}
        mock_host_class = mocks["hosts"].GioLocalhost
        h = hostfactory.get_host(fake_name)
        assert mock_host_class.called_with(fake_name)


@pytest.mark.unit
def test_get_host_with_name_with_settings_remote():
    fake_name = "fake"
    fake_host_name = "fake_host"
    with mock.patch.multiple("giosystemcore.hosts.hostfactory",
                             _hosts={},
                             get_settings=mock.DEFAULT,
                             hosts=mock.DEFAULT,
                             socket=mock.DEFAULT) as mocks:
        mocks["socket"].gethostname.return_value = fake_host_name
        mocks["get_settings"].return_value.get_host_settings.return_value = {
            "active": True}
        mock_host_class = mocks["hosts"].GioRemoteHost
        h = hostfactory.get_host(fake_name)
        assert mock_host_class.called_with(fake_name)
