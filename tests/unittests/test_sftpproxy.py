"""Unit tests for the giosystemcore.hosts.proxies.sftpproxy module."""

import unittest
import os

import mock

from giosystemcore.hosts.proxies import sftpproxy


class TestSftpProxyStateless(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.local_host = "dummy local host"
        cls.remote_host = "dummy remote host"
        cls.proxy_instance = sftpproxy.SftpProxyStateless(
            local=cls.local_host,
            remote=cls.remote_host
        )

    def setUp(self):
        # pysftp_patcher = mock.patch(
        #     "giosystemcore.hosts.proxies.sftpproxy.pysftp",
        #     autospec=True
        # )
        # self.mock_pysftp = pysftp_patcher.start()
        # self.addCleanup(pysftp_patcher.stop)
        connection_patcher = mock.patch(
                "giosystemcore.hosts.proxies.sftpproxy.pysftp.Connection",
                autospec=True
        )
        self.mock_connection = connection_patcher.start()
        self.addCleanup(connection_patcher.stop)

    def test_find(self):
        path = "/some/dummy/path"
        self.proxy_instance.find(path)
        self.mock_connection.__enter__.assert_called_with(host=None,
                                                          username=None,
                                                          password=None)
        self.mock_connection.chdir.assert_called_with(os.path.dirname(path))

    def test_fetch(self):
        raise NotImplementedError

    def test_send(self):
        raise NotImplementedError

    def test_delete_file(self):
        raise NotImplementedError

    def test_run_command(self):
        raise NotImplementedError

    def test_close_connection(self):
        raise NotImplementedError

    def test_create_remote_dirs(self):
        raise NotImplementedError

