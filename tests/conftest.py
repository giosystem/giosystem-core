"""pytest configuration file."""

import codecs
import datetime as dt

import pytest
import mock

from giosystemcore.catalogue import cswinterface
from giosystemcore.packages.packagefactory import get_package
from giosystemcore.settings import get_settings


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "integration: run only integration tests"
    )
    config.addinivalue_line(
        "markers",
        "unit: run only unit tests"
    )


def pytest_addoption(parser):
    parser.addoption(
        "--catalogue-type",
        choices=["pycsw", "geonetwork"],
        default="pycsw",
        help="Type of CSW catalogue to use in the tests. Defaults "
             "to %(default)s."
    )
    parser.addoption(
        "--catalogue-url",
        help="URL of the CSW catalogue to use in the tests. Defaults "
             "to %(default)s."
    )
    parser.addoption(
        "--catalogue-sample-record-path",
        help="Full path to a previously generated sample XML record that "
             "will be used for integration tests."
    )
    parser.addoption(
        "--catalogue-user",
        help="Username of the user that interacts with the CSW "
             "catalogue used in testing. Defaults to %(default)s."
    )
    parser.addoption(
        "--catalogue-password",
        help="Password for the user that interacts with the CSW "
             "catalogue used in testing. Defaults to %(default)s."
    )
    parser.addoption(
        "--settings-url",
        help="URL for the giosystem settings API to be used in "
             "integration tests."
    )
    parser.addoption(
        "--timeslot",
        default="201601010000",
        help="Timeslot to be used in integration tests. Must be specified "
             "with the format YYYYMMDDHHMM. Defaults to %(default)s."
    )


@pytest.fixture()
def catalogue_interface_integration(request):
    if request.config.getoption("--catalogue-type") == "pycsw":
        iface = cswinterface.CswInterfacePycsw(
            request.config.getoption("--catalogue-url"),
        )
    else:  # geonetwork catalogue
        iface = cswinterface.CswInterfaceGeonetwork(
            request.config.getoption("--catalogue-url"),
            user=request.config.getoption("--catalogue-user"),
            password=request.config.getoption("--catalogue-password")
        )
    return iface


@pytest.fixture()
def catalogue_sample_record(request):
    record_path = request.config.getoption("--catalogue-sample-record-path")
    record = ""
    with codecs.open(record_path, encoding="utf-8") as fh:
        record = fh.read()
    return record


@pytest.fixture()
def catalogue_interface(request):
    """Return a catalogue interface with mocked http connections."""
    requests_patcher = mock.patch(
        "giosystemcore.catalogue.cswinterface.requests.Session",
        autospec=True
    )
    mocked_session_class = requests_patcher.start()
    request.addfinalizer(requests_patcher.stop)
    if request.config.getoption("--catalogue-type") == "pycsw":
        iface = cswinterface.CswInterfacePycsw(
                request.config.getoption("--catalogue-url"),
        )
    else:  # geonetwork catalogue
        iface = cswinterface.CswInterfaceGeonetwork(
            request.config.getoption("--catalogue-url"),
            user=request.config.getoption("--catalogue-user"),
            password=request.config.getoption("--catalogue-password")
        )
    return iface


@pytest.fixture()
def mock_owslib_csw(request):
    #patcher = mock.patch("owslib.csw.CatalogueServiceWeb", autospec=True)
    patcher = mock.patch(
        "giosystemcore.catalogue.cswinterface.csw.CatalogueServiceWeb",
        autospec=True
    )
    mocked_class = patcher.start()
    mocked = mocked_class.return_value
    request.addfinalizer(patcher.stop)
    return mocked


@pytest.fixture(name="settings_url")
def fixture_settings_url(request):
    return request.config.getoption("--settings-url")


@pytest.fixture(name="timeslot")
def fixture_timeslot(request):
    return dt.datetime.strptime(request.config.getoption("timeslot"),
                                "%Y%m%d%H%M")


@pytest.fixture()
def latest_wms_package(settings_url, timeslot):
    get_settings(settings_url, initialize_logging=False)
    package = get_package("prepare_wms_latest_lst", timeslot)
    package.use_archive_for_searching = False
    package.use_io_buffer_for_searching = False
    package.extra_hosts_to_copy_outputs = []
    yield package
    package._clean_up()


@pytest.fixture()
def create_quicklooks_lst_package(settings_url, timeslot):
    get_settings(settings_url, initialize_logging=False)
    package = get_package("create_quicklooks_lst", timeslot)
    package.use_archive_for_searching = False
    package.use_io_buffer_for_searching = False
    package.extra_hosts_to_copy_outputs = []
    yield package
    package._clean_up()
