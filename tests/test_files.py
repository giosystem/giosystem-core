"""Unit tests for the GIOFile class."""

import os
import unittest

import giosystemcore.settings
import giosystemcore.files as ff

class TestGIOFile(unittest.TestCase):

    def setUp(self):
        settings_url = 'http://geo2.meteo.pt/giosystem/settings/api/v1/'
        self.settings = giosystemcore.settings.get_settings(settings_url)

    def test_creation_from_settings(self):
        for file_name in self.settings['files'].keys():
            f = ff.GIOFile.from_settings(file_name, self.settings)
            self.assertIsInstance(f, giosystemcore.files.GIOFile)

    def test_find_static_file_in_archive(self):
        some_static_file_name = ''
        for file_name, params in self.settings['files'].iteritems():
            if params['frequency'] == 'static' and params['system_input']:
                some_static_file_name = file_name
                break
        static_file = ff.GIOFile.from_settings(some_static_file_name,
                                               self.settings)
        found = static_file.find_in_archive()
        self.assertTrue(found) # make sure that the list is not empty

if __name__ == '__main__':
    unittest.main()
