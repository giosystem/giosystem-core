"""This module holds some utility functions for the giosystem package.

In order to use logging inside any function defined here, remember to call
the _get_logger function() first.

"""

from calendar import monthrange
import datetime as dt
import glob
import inspect
import logging
import os
import re

import requests

from . import settings


def write_namelists(namelists, parameters):
    """Generate a Fortran namelist string with the inputs.

    Parameters
    ----------
    namelists: list
        An iterable with dicts holding the namelists' structure. Each
        dict should have the following keys: 'name', 'default_values'. The
        contents of the 'default_values' key must be a dict with parameter
        names as keys and respective values as values
    parameters: dict
        A mapping with the values to use for the various namelist
        parameters. The keys that are missing from this dictionary will
        be filled with the values coming from the 'default_values' section
        of the input `namelists`

    Returns
    -------
    str
        A string with the namelist contents

    """

    result = []
    for namelist in namelists:
        result.extend(write_single_namelist(namelist, parameters=parameters))
        result.append("/\n")
    return result


def create_directory_tree(target_directory):
    """Create a directory tree

    .. note::

    This function has been moved out of the hosts module in order to start a
    needed refactoring of the hosts.

    """

    if not os.path.isdir(target_directory):
        try:
            os.makedirs(target_directory)
        except OSError as e:
            if e.errno == os.errno.EEXIST:
                pass


def write_single_namelist(namelist_spec, parameters):
    result = ["&NAM_{0}\n".format(namelist_spec["name"])]
    for parameter, default_value in namelist_spec["default_values"].items():
        value = parameters.get(parameter, default_value)
        if isinstance(value, (int, float)):
            content = "{parameter} = {value}\n".format(parameter=parameter,
                                                       value=value)
        elif isinstance(value, (tuple, list)):
            try:  # are we dealing with a sequence of numbers?
                float(value[0])
                number_sequence = ",".join((str(i) for i in value))
                content = "{parameter} = {number_sequence}\n".format(
                    parameter=parameter, number_sequence=number_sequence)
            except ValueError:  # no, we are dealing with a sequence of strings
                sequence_of_reprs = ["{0!r}".format(i) for i in value]
                formatted_sequence = ",\n".join(sequence_of_reprs)
                content = "{parameter} = {formatted_sequence}\n".format(
                    parameter=parameter,
                    formatted_sequence=formatted_sequence
                )
        elif value in (".FALSE.", ".F.",".TRUE.", ".T."):
            content = "{parameter} = {value}\n".format(parameter=parameter,
                                                       value=value)
        else:
            content = "{parameter} = {value!r}\n".format(parameter=parameter,
                                                         value=value)
        result.append(content)
    return result


def unzip_dictionary(dictionary):
    return zip(*dictionary.items())


def get_mimetype(path):
    mime_map = {
        ".nc": "application/netcdf",
        ".h5": "application/x-hdf",
        ".png": "image/png",
        ".bz2": "application/x-bzip2",
    }
    extension = os.path.splitext(path)[-1]
    return mime_map.get(extension, "")


def replace_age_path(the_string, timeslot):
    new_string = the_string
    patterns = [
        ('\\d{4}/\\d{2}/\\d{2}/\\d{2}/\\d{2}', timeslot.strftime('%Y/%m/%d/%H'
         '/%M')),
        ('\\d{4}/\\d{2}/\\d{2}/\\d{2}', timeslot.strftime('%Y/%m/%d/%H')),
        ('\\d{4}/\\d{2}/\\d{2}', timeslot.strftime('%Y/%m/%d')),
        ('\\d{4}/\\d{2}', timeslot.strftime('%Y/%m')),
        ('\\d{4}', timeslot.strftime('%Y')),
    ]
    for patt, value in patterns:
        if patt in the_string:
            new_string = new_string.replace(patt, value)
    return new_string


def parse_marks(the_string, base_objects=[], clean_special_characters=True):
    '''Will convert the marks on input string to meaningful values.

    Marks are inserted in the input string by wrapping expressions in
    {}.

    Inputs:

        the_string - the string that will be converted. This string will
            typically be:
                1 - the output directory of the package that originated
                    this file;
                2 - the search_path of this file, in case it is an input to
                    the system;
                3 - the search pattern of this file;

        base_objects - a list of objects or strings that specify which
            objects will be used for attribute susbtitution. Objects will
            be tried in sequence and the first to match is kept.

    Example strings:

        1 - OUTPUTS/PROCESSING/{external_code-name}/V{external_code-version}/{year}/{month}/{day}
        1 - OUTPUTS/PROCESSING/{name}/V{external_code-version}/{year}/{month}/{day}
            In this example provides, {name} refers to the name of the originating package,
            not to the name of the file. The same is true for the {year}, {month} and {day} marks.
            When the string to parse refers to a directory, and the GIOFile is an output from 
            some package, the attributes will be refering to the package and not the GIOFile.
        2 - raw_inputs/GOESE
        3 - HDF5_GEOLAND2_{source-name}_CMa_{source-disk_name}_{timeslot}.h5

    '''

    new_string = the_string
    token_pattern = re.compile(r'\{(\D.*?)\}')
    for match_obj in token_pattern.finditer(the_string):
        token = match_obj.group(1)
        value = process_token(token, base_objects)
        if clean_special_characters:
            if value == '':
                value = '.*'
            new_string = re.sub(match_obj.group(), value, new_string)
        else:
            if value != '':
                new_string = re.sub(match_obj.group(), value, new_string)
    return new_string


def process_token(token, base_objects):
    token_levels = token.split('-')
    expr = ''
    current_level = 0
    while current_level < len(token_levels):
        expr = '.'.join((expr, token_levels[current_level]))
        current_level += 1
    current_base = 0
    value = ''
    while current_base < len(base_objects) and value == '':
        try:
            value = eval('base_objects[current_base]%s' % expr)
            if value is None:
                value = ''
        except AttributeError as e:
            try:
                first, sep, param = expr.rpartition('.')
                parameter_expr = '%s.parameters["%s"]' % (first, param)
                value = eval('base_objects[current_base]%s' % parameter_expr)
            except (AttributeError, KeyError):
                    pass
        current_base += 1
    return value


def get_file_originator_package(file_name, settings):
    output_from = None
    for p, p_settings in settings.get('packages', dict()).iteritems():
        if p_settings is not None:
            outs = [out_['name'] for out_ in p_settings.get('outputs', [])]
            if file_name in outs:
                output_from = p
    return output_from


def get_packages_that_use_file(file_name, settings):
    input_to = set()
    for p, p_settings in settings.get('packages', dict()).iteritems():
        if p_settings is not None:
            ins = [inp_['name'] for inp_ in p_settings.get('inputs', [])]
            if file_name in ins:
                input_to = p
    return input_to


def get_parent_output_dirs(file_name, settings):
    '''
    Get the output dirs for a GIOFile based on its parent package.

    Inputs:

        file_name - A string with the name of the GIOFile

        settings - A dictionary holding the global settings

        timeslot - A datetime.datetime object with the timeslot of the
            GIOFile.
    '''

    output_dirs = []
    output_from = get_file_originator_package(file_name, settings)
    if output_from is not None:
        out_dirs = settings['packages'][output_from].get('output_directories',
                                                         dict())
        for dir_name, dir_path in out_dirs.iteritems():
            output_dirs.append(dir_path)
    return output_dirs


# deprecate this function?
def revert_timeslot(timeslot, revert_rules):
    if revert_rules.get('package') is not None:
        unit = revert_rules['package'].get('unit', 'minute')
        value = revert_rules['package'].get('value', 0)
        operation = revert_rules['package'].get('operation', 'subtract')
        if operation == 'add':
            reverse_operation = 'subtract'
        else:
            reverse_operation = 'add'
        reverted_timeslot = displace_timeslot(timeslot, unit,
                                              reverse_operation,
                                              value)
    elif revert_rules.get('temporal') is not None:
        reverted_timeslot = revert_timeslot_temporal_rule()
    else:
        reverted_timeslot = None
    return reverted_timeslot


# deprecate this function?
def apply_timeslot_offset(original, operation, unit, value):
    '''
    Offset a timeslot according to input parameters

    Inputs:

        original - a datetime.datetime object to offset

        operation - a string with the arithmetic operation to perform.
            Can be either 'ADD' or 'SUBTRACT'

        unit - a string with the temporal unit to use when offsetting the
            timeslot. Can be either MINUTE, HOUR or DAY

        value - an int with the value to use for offsetting.
    '''

    unit_map = {
        'MINUTE': dt.timedelta(seconds=value*60),
        'HOUR': dt.timedelta(seconds=value*60*60),
        'DAY': dt.timedelta(days=value),
    }
    if operation == 'ADD':
        offset_timeslot = original + unit_map[unit]
    elif operation == 'SUBTRACT':
        offset_timeslot = original - unit_map[unit]
    else:
        raise ValueError('Invalid operation argument: %s' % operation)
    return offset_timeslot


# deprecate this function?
def displace_timeslot(timeslot, unit, operation, value):
    '''
    Inputs:

        timeslot - A datetime.datetime object with the timeslot to displace

        unit - A string with the unit of time to use. Accepted values are:
            - minute
            - hour
            - day

        operation - A string with the time displacement operation that is to
            be used. Accepted values are:
            - add
            - subtract

        value - An integer specifying how many units of time is the timeslot
            to be displaced
    '''

    if unit == 'minute':
        delta = dt.timedelta(seconds=value*60)
    elif unit == 'hour':
        delta = dt.timedelta(seconds=value*60*60)
    elif unit == 'day':
        delta = dt.timedelta(days=value)
    else:
        raise ValueError('Invalid unit argument: %s' % unit)
    if operation == 'add':
        reverted_timeslot = timeslot + delta
    elif operation == 'subtract':
        reverted_timeslot = timeslot - delta
    else:
        raise ValueError('Invalid operation argument: %s' % operation)
    return reverted_timeslot


def revert_timeslot_temporal_rule():
    raise NotImplementedError


# TODO
# rethink this function...
def find_area(search_string, settings):
    '''
    Find the area by inspecting the input search string.

    Available area names are retrieved from the settings

    Inputs:

        search_string - the string to process

        settings - a giosystemcore.settings.SequentialSettings object

    Returns:

        A string with the name of the area or None.
    '''

    areas = settings.get_all_areas_settings()
    the_area = None
    for area_name in areas.keys():
        if area_name in search_string:
            the_area = area_name
    return the_area


def extract_timeslot_from_path(path):
    '''
    Extract a datetime object from a path

    The timeslot is first searched in the basename of the path and
    if nothing is found it searches in the dirname.
    '''

    dir_name, file_name = os.path.split(path)
    # first process the file name and try to get a timeslot from there
    # if not found, process the directory paths
    timeslot = extract_timeslot_from_file_name(file_name)
    if timeslot is None:
        timeslot = extract_timeslot_from_directory_structure(dir_name)
    return timeslot


def extract_timeslot_from_file_name(file_name):
    ts_pattern = re.compile(r'_(\d{12})')
    re_obj = ts_pattern.search(file_name)
    timeslot = None
    if re_obj is not None:
        timeslot = dt.datetime.strptime(re_obj.groups()[0], '%Y%m%d%H%M')
    return timeslot


def extract_product_name_from_file_name(file_name):
    manager = settings.get_settings()
    all_products = manager.get_all_products_settings()
    the_product = None
    for s in all_products.values():
        product_name = s['short_name']
        if re.search(product_name, file_name) is not None:
            the_product = product_name
    return the_product


def extract_timeslot_from_directory_structure(directory):
    ts_patterns = [
        r'(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/' \
          '(?P<hour>\d{2})/(?P<minute>\d{2})',
        r'(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<hour>\d{2})',
        r'(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})',
        r'(?P<year>\d{4})/(?P<month>\d{2})',
    ]
    timeslot = None
    count = 0
    while count < len(ts_patterns) and timeslot is None:
        current_pattern = ts_patterns[count]
        re_obj = re.search(current_pattern, directory)
        if re_obj is not None:
            fields = re_obj.groupdict()
            timeslot = dt.datetime(
                int(fields.get('year')),
                int(fields.get('month')),
                int(fields.get('day', '1')),
                int(fields.get('hour', '0')),
                int(fields.get('minute', '0')),
            )
        count += 1
    return timeslot


def _get_logger():
    '''
    This function should be called by every function in this module in order
    to retreive a properly configured logger.

    It uses some magic from the inspect module to get the name of the function
    that called this one and sets the name of the logger appropriately.
    '''

    caller_name = inspect.stack()[1][0].f_code.co_name
    return logging.getLogger('.'.join((__name__, caller_name)))


def offset_timeslot(timeslot, offset_years=0, offset_months=0,
                    offset_days=0, offset_hours=0, offset_minutes=0,
                    offset_decades=0):
    '''
    Offset a timeslot.

    The 'offset_decades' argument is applied first, before the other offsets.

    Inputs:

        timeslot - the timeslot to offset

        offset_years - an integer specifying how many years to
            offset the timeslot with

        offset_months - an integer specifying how many months to
            offset the timeslot with

        offset_days - an integer specifying how many days to
            offset the timeslot with

        offset_hours - an integer specifying how many hours to
            offset the timeslot with

        offset_minutes - an integer specifying how many minutes to
            offset the timeslot with

        offset_decades - an integer specifying how many decades to
            offset the timeslot with.
            
            There are three decades in each month, starting at the 
            1st, 11th and 21st days. The third decade's length depends 
            on the number of days in the month.

            The default value is None. If it is set 0, The timeslot
            is reduced to the 1st day of the current decade.

    Returns:

        A new timeslot.
    '''

    new_year = timeslot.year + offset_years
    if offset_months != 0:
        month_years = (timeslot.month + offset_months - 1) // 12
        new_year += month_years
    new_month = (timeslot.month + offset_months) % 12
    if new_month == 0:
        new_month = 12
    month_n_days = monthrange(new_year,new_month)[1]
    new_day = timeslot.day
    if month_n_days < timeslot.day:
        new_day = month_n_days
    temp_slot = dt.datetime(new_year, new_month, new_day,
                            timeslot.hour, timeslot.minute)
    off_seconds = (offset_hours * 60 * 60) + offset_minutes * 60

    if not offset_decades is None:
        if timeslot.day < 11:
            decade_1st_day = 1
        elif timeslot.day < 21:
            decade_1st_day = 11
        else:
            decade_1st_day = 21
        timeslot_1st_day_of_the_decade = dt.datetime(timeslot.year, timeslot.month,
                                       decade_1st_day, timeslot.hour,
                                       timeslot.minute)
        timeslot_end = timeslot_1st_day_of_the_decade
        if offset_decades != 0:
            offset_sign = offset_decades/abs(offset_decades)
            for decade in range (abs(offset_decades)):
                # offset_sign:  1 -> forward in time
                # offset_sign: -1 -> backward in time
                if (timeslot_end.day < 21 and offset_sign > 0) or \
                   (timeslot_end.day > 1 and offset_sign < 0):
                    timeslot_end += offset_sign*dt.timedelta(days=10)
                else:
                    if offset_sign > 0:
                    # forward in time
                        n_days = monthrange(timeslot_end.year, timeslot_end.month)[1] - 20
                    else:
                    # backward in time
                        timeslot_month_before = timeslot_end - dt.timedelta(days=1)
                        n_days = monthrange(timeslot_month_before.year, timeslot_month_before.month)[1] - 20

                    timeslot_end += offset_sign * dt.timedelta(days=n_days)
            offset_days +=  offset_sign * abs((timeslot_end - timeslot).days)
    delta = dt.timedelta(days=offset_days, seconds=off_seconds)
    result = temp_slot + delta
    return result


def get_unofficial_creation_date(record_identifier, catalogue_hacks_api_url):
    """
    Retrieve a record's creation date.

    This function uses an unofficial API to reach the catalogue's database
    because the CSW protocol defines a record's creation date as only of type
    Date, while we need it to be a DateTime.

    :return:
    """

    catalogue_hacks_api_url = catalogue_hacks_api_url.rstrip("/")
    url = "/".join((catalogue_hacks_api_url, "metadata", record_identifier))
    payload = {"format": "json"}
    response = requests.get(url, params=payload)
    creation_date = None
    if response.status_code == 200:
        values = response.json()
        creation_date = values["created_on"]
        if creation_date is not None:
            creation_date = dt.datetime.strptime(creation_date,
                                                 "%Y-%m-%dT%H:%M:%S")
    return creation_date


def get_last_modified_time(file_path):
    """Get the datetime of the last modification of the file."""
    timestamp = os.path.getmtime(file_path)
    return dt.datetime.fromtimestamp(timestamp)


def get_directory_contents(directory):
    """Yield the files that are contained in a directory.

    This is a generator function. It yields results one by one. It was
    designed to cope with directories that store a large number of files. It
    does not keep the list of files that are inside the input directory in
    memory.

    Parameters
    ----------
    directory: str
        The path to the directory to scan

    Yields
    ------
    str
        The full path to each of the directory's contents.
    """

    for item_name in glob.iglob(os.path.join(directory, "*")):
        path = os.path.join(directory, item_name)
        yield path
