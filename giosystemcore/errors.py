'''
This module holds all the custom exceptions for the giosystem package.
'''

class GiosystemCoreError(Exception):
    pass


class SettingsNotFoundError(GiosystemCoreError):
    pass


class InvalidSettingsError(GiosystemCoreError):
    pass


class InvalidExecutionTimeslot(GiosystemCoreError):
    pass


class InvalidClassPathError(GiosystemCoreError):
    pass


class InvalidFileType(GiosystemCoreError):
    pass


class ExternalProgramError(GiosystemCoreError):
    pass


class OutputNotFoundError(GiosystemCoreError):
    pass


class FileNotFoundError(GiosystemCoreError):
    pass


class FileNotSentError(GiosystemCoreError):
    pass


class ProcessingError(GiosystemCoreError):
    pass


class RemoteHostNotFoundError(GiosystemCoreError):
    pass


class ProductNotDefinedError(GiosystemCoreError):
    pass


class UndefinedExternalAlgorithmError(GiosystemCoreError):
    pass


class CswLoginFailedError(GiosystemCoreError):
    pass
