"""This module creates Copernicus compliant metadata records suitable for
ingestion by IPMA's CSW catalogue.
"""

from __future__ import absolute_import
import re
import os
import datetime as dt
from collections import namedtuple
import logging

import pystache

from netcdf_cglops_tools import netcdfanalyzer

logger = logging.getLogger(__name__)


ContactDetails = namedtuple(
    "ContactDetails",
    "name short_name position delivery_point city postal_code "
    "country country_code e_mail website role"
)


class NetCdfMetadataGenerator(object):

    def generate_metadata(self, nc_path):
        """Generate an XML with metadata regarding the input product.

        This method will extract the relevant metadata details from the input
        product and produce an XML document suitable for ingestion into a
        catalogue.

        Parameters
        ----------
        nc_path: str
            The full path to the Copernicus NetCDF product

        Returns
        -------
        str
            A string with the generated XML document

        """
        renderer = pystache.Renderer(
            file_encoding="utf8",
            search_dirs=os.path.join(os.path.dirname(__file__),
                                     'metadata_templates')
        )
        metadata = netcdfanalyzer.ProductInfo(nc_path)
        template = renderer.load_template(metadata.name.lower())
        context = self._get_context(metadata)
        rendered = renderer.render(template, context)
        return rendered

    def _get_context(self, metadata):
        """Get the metadata that applies to the XML generating template

        This method searches an input mapping for the keys and sub-mappings
        necessary in order to supply data for a template to render an XML
        document with metadata suitable for ingestion on a CSW catalogue.
        """

        timeslot = os.path.basename(metadata.gdal_path).split("_")[3]
        now = dt.datetime.utcnow().strftime("%Y-%m-%d")
        download_url, quicklook_url = self._build_urls(metadata.name, timeslot)
        ctx = {
            "fileIdentifier": metadata.identifier,
            "parentIdentifier": metadata.parent_identifier,
            "dateStamp": now,
            "spatialRepresentationInfo": {
                "rowSize": metadata.y_size,
                "columnSize": metadata.x_size,
                "resolution": round(metadata.resolution, 5),
                "upperLeftLat": round(metadata.bounding_box.upper_left_lat),
                "upperLeftLon": round(metadata.bounding_box.upper_left_lon),
            },
            "identificationInfo": {
                "pointOfContact": [
                    dict(c.__dict__) for c in metadata.contacts],
                "citation": {
                    "title": metadata.title,
                    "date": now,
                    "code": metadata.identifier,
                },
                "abstract": metadata.comment,
                "purpose": metadata.purpose,
                "credit": metadata.credit,
                "graphicOverview": quicklook_url,
                "resolution": round(metadata.resolution, 5),
                "uncontrolledKeywords": [
                    {"keyword": timeslot},
                ],
                "temporalExtent": {
                    "beginPosition":
                        metadata.time_coverage_start.strftime(
                            "%Y-%m-%dT%H:%M:%SZ"),
                    "endPosition":
                        metadata.time_coverage_end.strftime(
                            "%Y-%m-%dT%H:%M:%SZ"),
                },
                "isoTopicCategory": [{"isoCategory": cat} for cat in
                                     metadata.iso19115_topic_categories],
                "spatialExtent": {
                    "westLongitude": round(
                        metadata.bounding_box.upper_left_lon),
                    "eastLongitude": round(
                        metadata.bounding_box.lower_right_lon),
                    "southLatitude": round(
                        metadata.bounding_box.lower_right_lat),
                    "northLatitude": round(
                        metadata.bounding_box.upper_left_lat),
                },
            },
            "contentInfo": [],
            "distributionInfo": download_url
        }
        for name, variable_info in metadata.variables.items():
                ctx["contentInfo"].append(
                    self._get_dataset_context(variable_info))
        return ctx

    def _get_dataset_context(self, variable_metadata):
        units = variable_metadata.units or ""
        scaling_factor = variable_metadata.scale_factor
        add_offset = variable_metadata.add_offset
        minimum, maximum = (int(i) for i in variable_metadata.valid_range)
        real_minimum = add_offset + minimum * scaling_factor
        real_maximum = add_offset + maximum * scaling_factor
        bits_per_value = variable_metadata.dtype.itemsize * 8
        ctx = {
            "recordType": variable_metadata.long_name,
            # FIXME: add this attribute to the product, check issues #486, #618
            "contentType": getattr(variable_metadata, "coverage_content_type",
                                   None),
            "dimension": [
                {
                    "MemberName": "Digital Number",
                    "TypeName": "value type",
                    "descriptor": "Significant digital value range",
                    "maxValue": maximum,
                    "minValue": minimum,
                    "units": units,
                    "bitsPerValue": bits_per_value,
                    "scaleFactor": scaling_factor,
                    "offset": variable_metadata.add_offset,
                    "id": "Digital_Number_{}".format(variable_metadata.name)
                },
                {
                    "MemberName": "Physical Value",
                    "TypeName": "value type",
                    "descriptor": variable_metadata.name,
                    "maxValue": real_maximum,
                    "minValue": real_minimum,
                    "units": units,
                    "id": "Physical_Value_{}".format(variable_metadata.name)
                },
                {
                    "MemberName": "Invalid",
                    "TypeName": "flag type",
                    "descriptor": "Invalid",
                    "maxValue": variable_metadata.fill_value,
                    "minValue": variable_metadata.fill_value,
                    "units": units,
                    "bitsPerValue": bits_per_value,
                    "id": "Invalid_{}".format(variable_metadata.name)
                }
            ]
        }
        return ctx

    def _build_urls(self, product_name, timeslot):
        base_url = "http://downloads.cglops.ipma.pt/{product}/{slot}".format(
            product=product_name, slot=timeslot)
        download_url = "{}/product/".format(base_url)
        quicklook_url = "{}/quicklook/".format(base_url)
        return download_url, quicklook_url

    def _extract_node_attributes(self, nc_node):
        """Extract the attributes of a netcdf node into a dict"""
        attrs = {}
        for name in nc_node.ncattrs():
            value = getattr(nc_node, name)
            if name in ("time_coverage_start", "time_coverage_end",
                        "date_created"):
                value = dt.datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")
            elif name == "algorithm_version":
                alg_version_genexp = (p.split("_v") for p in value.split(","))
                value = {alg: version for alg, version in alg_version_genexp}
            elif name in ("platform", "sensor", "gemet_keywords",
                          "gcmd_keywords", "other_keywords", "orbit_type",
                          "iso19115_topic_categories", "ancillary_variables"):
                value = value.split(",")
            elif name == "contacts":
                contacts = []
                for contact in value.splitlines():
                    contacts.append(self._extract_contact_details(contact))
                value = contacts
            attrs[name] = value
        return attrs

    def _extract_contact_details(self, contact_expression):
        rest, sep, website = contact_expression.rpartition(";")
        pattern = (r"(?P<role>[\w ]+)\(?(?P<position>[\w -]*)\)?: "
                   r"(?P<email>[\w.@-]+); (?P<name>[\w -]+) \("
                   r"(?P<short_name>[\w-]+)\); "
                   r"(?P<delivery_point>[\w -',.]+); "
                   r"(?P<city>[\w ]+); (?P<postal_code>[\w-]+); "
                   r"(?P<country>\w+) \((?P<country_code>\w+)\);")
        found = re.search(pattern, rest,
                          flags=re.UNICODE).groupdict()
        role = found["role"].strip()
        position = found["position"] if found["position"] != "" else role
        contact = ContactDetails(
            name=found["name"], short_name=found["short_name"],
            position=position, delivery_point=found["delivery_point"],
            city=found["city"], postal_code=found["postal_code"],
            country=found["country"], country_code=found["country_code"],
            e_mail=found["email"], website=website.strip(), role=role,
        )
        return contact
