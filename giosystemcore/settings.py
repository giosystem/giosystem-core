# Copyright 2014 Ricardo Garcia Silva
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""A module for reading settings stored in an online REST API.

This module provides a global interface for accessing giosystem settings stored
in the giosystem-settings-django app, which is another component of giosystem.

Usually all that is necessary is to instantiate a :py:class:`SettingsManager`
object, providing it with the correct URL for the giosystem-settings-django
app. This is achieved by calling the module's :py:func:`get_settings` function.

.. code:: python

   import giosystemcore.settings
   
   settings_url = 'http://geo2.meteo.pt/giosystem/settings/api/v1/'
   giosystemcore.settings.get_settings(settings_url)
"""

import logging
import logging.config

import urllib
import requests

import errors

_settings_manager = None


def get_settings(base_url=None, initialize_logging=True):
    """Return the settings manager.

    If the settings manager has already been created by a previous call,
    it will be returned. If not, the base_url argument must be provided
    so that a new settings manager can be created from the settings present
    at the URL. This ensures that multiple calls to this function will
    not trigger unneeded network traffic.

    :arg base_url: URL to query the REST API and get the defined settings.
                   A value of None will assume that the previously cached 
                   settings should be returned instead.
    :type base_url: str
    """

    global _settings_manager
    if base_url is not None:
        _settings_manager = SettingsManager(base_url, initialize_logging)
    if _settings_manager is None:
        raise errors.InvalidSettingsError('Undefined settings')
    return _settings_manager


class SettingsManager(object):
    """
    Read the settings stored online in the django app.

    Settings are fetched when needed instead of all in one go.
    """

    def __init__(self, url, initialize_logging=True):
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.logger.addHandler(logging.NullHandler())
        self._packages = dict()
        self._files = dict()
        self._products = dict()
        self._product_files = dict()
        self._sources = dict()
        self._hosts = dict()
        self._organizations = dict()
        self._categories = dict()
        self._suites = dict()
        self._geographic_areas = dict()
        self._palettes = dict()
        self._ecflow_servers = dict()
        self._logging = None
        self._io_buffer = None
        self._data_receiver = None
        self._archive = None
        self._ftp_server = None
        self._order_server = None
        self._web_server = None
        self._home_organization = None
        if not url.endswith('/'):
            url += '/'
        self._settings_url = url
        if initialize_logging:
            self._initialize_logging()
        else:
            logging.getLogger("requests").propagate = False
            logging.getLogger("paramiko").propagate = False

    def _initialize_logging(self):
        """
        Set up logging.
        """

        if self._logging is None:
            self._logging = self._get_single_instance_model('logging')
        log_level = self._logging['log_level']
        format_ = self._logging['log_format']
        settings_dict = {
            'version': 1,
            'formatters': {
                'short_format': {
                    'format': format_,
                },
            },
            'handlers': {
                'console': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'short_format',
                    'level': log_level,
                },
            },
            'root': {
                'level': 'NOTSET',
                'handlers': ['console'],
            },
            'loggers': {
                'paramiko': {
                    'level': 'ERROR',
                    'handlers': ['console'],
                },
                'pycountry': {
                    'level': 'ERROR',
                    'handlers': ['console'],
                },
                'requests': {
                    'level': 'ERROR',
                    'handlers': ['console'],
                },
                'PyWPS': {
                    'level': 'ERROR',
                    'handlers': ['console'],
                },
                'giosystemcore': {
                    'level': 'NOTSET',
                },
                'giosystemecflow': {
                    'level': 'NOTSET',
                },
                'celery.task': {
                    'level': 'NOTSET',
                    'handlers': ['console'],
                },
                'giosystemprocessing': {
                    'level': 'NOTSET',
                },
            },
        }
        logging.config.dictConfig(settings_dict)

    def get_package_settings(self, name):
        """
        Return the settings for the package.
        """

        pack_settings = self._get_instance_settings_by_name(self._packages,
                                                            'package', name)
        if isinstance(pack_settings['exclude_hours'], basestring):
            pack_settings['exclude_hours'] = [
                int(h) for h in pack_settings['exclude_hours'].split(',') if
                h != ''
            ]
        input_uris = [f['input'] for f in pack_settings['inputs'] if
                      isinstance(f['input'], basestring)]
        output_uris = [f['output'] for f in pack_settings['outputs'] if
                       isinstance(f['output'], basestring)]
        if any(input_uris):
            inputs_settings = self._get_package_io_files_settings(input_uris)
            for inp in pack_settings['inputs']:
                for inp_uri, inp_settings in inputs_settings.iteritems():
                    if inp['input'] == inp_uri:
                        inp['input'] = inp_settings
        if any(output_uris):
            outputs_settings = self._get_package_io_files_settings(output_uris)
            for out in pack_settings['outputs']:
                for out_uri, out_settings in outputs_settings.iteritems():
                    if out['output'] == out_uri:
                        out['output'] = out_settings
        return pack_settings

    def get_file_settings(self, name):
        """
        Return the settings for the file.
        """

        return self._get_instance_settings_by_name(self._files, 'file', name)

    def get_organization_settings(self, uri):
        return self._get_instance_settings(self._organizations, 'organization',
                                           uri)

    def get_product_settings(self, uri):
        product_settings = self._get_instance_settings(self._products,
                                                       'product', uri)
        contact_keys = ('point_of_contact', 'owner', 'responsible',
                        'originator', 'distributor', 'principal_investigator')
        for k in contact_keys:
            org_uri = product_settings[k]['organization']
            if not isinstance(org_uri, dict):
                org_settings = self.get_organization_settings(org_uri)
                product_settings[k]['organization'] = org_settings
        return product_settings

    def get_product_files_settings(self, product_short_name, file_type):
        if self._product_files.get(product_short_name) is None:
            payload = {
                "product__short_name": product_short_name.upper(),
                "file_type__iexact": file_type,
            }
            url = "{}file/".format(self._settings_url)
            raw_settings = self._get_raw_settings(url, payload)
            self._product_files[product_short_name] = raw_settings["objects"]
        return self._product_files[product_short_name]

    def get_product_settings_by_name(self, short_name):
        """
        Get product settings given its short name.
        """

        product = self._get_instance_settings_with_filter(
            self._products, 'product', 'short_name', short_name)
        complete = self.get_product_settings(product['resource_uri'])
        return complete

    def get_host_settings(self, name):
        """
        Return the settings for the host.
        """

        return self._get_instance_settings_by_name(self._hosts, 'host', name)

    def get_all_packages_settings(self):
        """
        Return the settings for all of the defined GioPackages

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._packages):
            self._get_all_instances(self._packages, 'package')
        return self._packages

    def get_all_organizations_settings(self):
        """
        Return the settings for all of the defined organizations

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._organizations):
            self._get_all_instances(self._organizations, 'organization')
        return self._organizations

    def get_all_palettes_settings(self):
        """
        Return the settings for all of the defined palettes

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._palettes):
            self._get_all_instances(self._palettes, 'mapserverpalette')
        return self._palettes

    def get_all_files_settings(self):
        """
        Return the settings for all of the defined GioFiles

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._files):
            self._get_all_instances(self._files, 'file')
        return self._files

    def get_all_hosts_settings(self):
        """
        Return the settings for all of the defined hosts

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._hosts):
            self._get_all_instances(self._hosts, 'host')
        return self._hosts

    def get_all_sources_settings(self):
        """
        Return the settings for all of the defined sources

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._sources):
            self._get_all_instances(self._sources, 'source')
        return self._sources

    def get_all_areas_settings(self):
        """
        Return the settings for all of the defined areas

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._geographic_areas):
            self._get_all_instances(self._geographic_areas, 'geographicarea')
        return self._geographic_areas

    def get_all_ecflow_servers_settings(self):
        """
        Return the settings for all of the defined ecflow servers

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._ecflow_servers):
            self._get_all_instances(self._ecflow_servers, 'ecflowserver')
            for server_settings in self._ecflow_servers.values():
                host_uri = server_settings['host']
                server_settings['host'] = self._get_instance_settings(
                    self._hosts, 'host', host_uri
                )
        return self._ecflow_servers

    def get_all_products_settings(self):
        """
        Return the settings for all of the defined products

        This method will only query the online REST API if its cache is empty.
        """

        if not any(self._products):
            self._get_all_instances(self._products, 'product')
        return self._products

    def get_source_settings(self, uri):
        return self._get_instance_settings(self._sources, 'source',
                                           uri)

    def get_source_settings_by_name(self, name):
        """
        Get source settings given its name.
        """

        return self._get_instance_settings_by_name(self._sources, 'source',
                                                   name)

    def get_suite_settings_by_name(self, name):
        """
        Get a suite's settings given its name.
        """

        return self._get_instance_settings_by_name(self._suites, 'suite', name)

    def get_suite_settings(self, uri):
        return self._get_instance_settings(self._suites, 'suite', uri)

    def get_io_buffer_settings(self):
        """
        Get the specific settings for the io buffer host.
        """

        if self._io_buffer is None:
            settings = self._get_host_role_settings('iobufferhost')
            self._io_buffer = settings
        return self._io_buffer

    def get_data_receiver_settings(self):
        """
        Get the specific settings for the data receiver host.
        """

        if self._data_receiver is None:
            settings = self._get_host_role_settings('datareceiverhost')
            self._data_receiver = settings
        return self._data_receiver

    def get_archive_settings(self):
        """
        Get the specific settings for the archive host.
        """

        if self._archive is None:
            settings = self._get_host_role_settings('archivehost')
            self._archive = settings
        return self._archive

    def get_ftp_server_settings(self):
        """
        Get the specific settings for the ftp server host.
        """

        if self._ftp_server is None:
            settings = self._get_host_role_settings('externalftphost')
            self._ftp_server = settings
        return self._ftp_server

    def get_order_server_settings(self):
        """
        Get the specific settings for the ordering server host.
        """

        if self._order_server is None:
            settings = self._get_host_role_settings('orderinghost')
            self._order_server = settings
        return self._order_server

    def get_web_server_settings(self):
        """
        Get the specific settings for the web server host.
        """

        if self._web_server is None:
            settings = self._get_host_role_settings('webserverhost')
            self._web_server = settings
        return self._web_server

    def get_palette_settings(self, uri):
        return self._get_instance_settings(self._palettes, 'mapserverpalette',
                                           uri)

    def _get_host_role_settings(self, host_role_uri):
        role_settings = self._get_single_instance_model(host_role_uri)
        host_uri = role_settings['host']
        cached_host = self._find_cached_item_by_property(self._hosts,
                                                         'resource_uri',
                                                         host_uri)
        if cached_host is None:
            self._get_all_instances(self._hosts, 'host')
            cached_host = self._find_cached_item_by_property(
                self._hosts,
                'resource_uri',
                host_uri,
                raise_=True
            )
        role_settings['host'] = cached_host
        return role_settings

    @staticmethod
    def _find_cached_item_by_property(cache, property_name, property_value,
                                      raise_=False):
        """
        """

        found = None
        for name, settings in cache.iteritems():
            if settings[property_name] == property_value:
                found = settings
        if found is None and raise_:
            raise errors.InvalidSettingsError('"%s: %s" is not defined in '
                                              'the settings' %
                                              (property_name, property_value))
        return found

    def _get_raw_settings(self, url, payload=None):
        """
        Query the REST API and return the response as a JSON object.
        """

        self.logger.debug('Fetching settings from %s' % url)
        response = requests.get(url, params=payload)
        if response.status_code == 200:
            settings = response.json()
        else:
            raise errors.SettingsNotFoundError('Couldn\'t retrieve settings '
                                               'from %s: %s - %s' %
                                               (url, response.status_code,
                                                response.reason))
        return settings

    def _get_package_io_files_settings(self, resource_uris):
        return self._get_multiple_instance_settings(self._files, 'file',
                                                    resource_uris)

    def get_multiple_package_settings(self, package_uris):
        return self._get_multiple_instance_settings(self._packages, 'package',
                                                    package_uris)

    def get_multiple_host_settings(self, host_uris):
        return self._get_multiple_instance_settings(self._hosts, 'host',
                                                    host_uris)

    def _get_instance_settings(self, cache, model_uri, instance_uri):
        """
        Return a single instance by its uri.
        """

        instance = None
        for settings in cache.values():
            if settings['resource_uri'] == instance_uri:
                instance = settings
        if instance is None:
            instance_id = instance_uri.rsplit('/', 2)[-2]
            url = '%s%s/%s/?format=json' % (self._settings_url, model_uri,
                                            instance_id)
            instance = self._get_raw_settings(url)
            cache[instance['name']] = instance
        return instance

    def _get_instance_settings_by_name(self, cache, model_uri, instance_name):
        """
        Return a single instance by name.
        """

        instance = cache.get(instance_name)
        if instance is None:
            escaped_name = urllib.quote(instance_name)
            url = '%s%s/?name=%s&format=json' % (self._settings_url,
                                                 model_uri, escaped_name)
            self.logger.debug('url: {}'.format(url))
            raw = self._get_raw_settings(url)
            instance = raw['objects'][0] if len(raw['objects']) > 0 else None
            if instance is not None:
                cache[instance_name] = instance
            else:
                raise errors.InvalidSettingsError('"%s" is not defined in '
                                                  'the settings' % 
                                                  instance_name)
        return instance

    def _get_instance_settings_with_filter(self, cache, model_uri,
                                           filter_property, filter_value):
        """
        Return a single instance using a filter

        This method shows potential for improving the overall settings fetching
        mechanism. However it currently has an important flaw: it assumes every
        instance has a 'name' property and that this name is unique. Its
        shortcomings would be better addressed by using each instance's URI as
        a key in the cache dictionary. Something to review in the future.
        """

        instance = self._find_cached_item_by_property(cache, filter_property,
                                                      filter_value)
        if instance is None:
            escaped_property = urllib.quote(filter_property)
            escaped_value = urllib.quote(filter_value)
            url = '{}{}/?{}={}&format=json'.format(self._settings_url,
                                                   model_uri, escaped_property,
                                                   escaped_value)
            self.logger.debug('url: {}'.format(url))
            raw = self._get_raw_settings(url)
            instance = raw['objects'][0] if len(raw['objects']) > 0 else None
            if instance is not None:
                cache[instance.get('name')] = instance
            else:
                raise errors.InvalidSettingsError('"%s" is not defined in '
                                                  'the settings' % 
                                                  instance.name)
        return instance

    def _get_multiple_instance_settings(self, cache, model_uri,
                                        instance_uris):
        """
        Return multiple instances by id.

        Query the settings REST API for the values for each object
        given its unique id.
        """

        to_get = []
        for uri in instance_uris:
            found = False
            for name in cache.keys():
                try:
                    if cache[name]['resource_uri'] == uri:
                        found = True
                except KeyError:
                    pass
            if not found:
                id_ = uri.rsplit('/', 2)[-2]
                to_get.append(id_)
        if any(to_get):
            url = '%s%s/set/%s/?format=json' % (self._settings_url,
                                                model_uri,
                                                ';'.join(to_get))
            raw = self._get_raw_settings(url)
            for instance in raw['objects']:
                cache[instance['name']] = instance
            not_found = raw.get('not_found', [])
            if any(not_found):
                raise errors.InvalidSettingsError('%s is not defined in '
                                                  'the settings' %
                                                  ', '.join(not_found))
        result = dict()
        for uri in instance_uris:
            for name in cache.keys():
                try:
                    if cache[name]['resource_uri'] == uri:
                        result[uri] = cache[name]
                except KeyError:
                    pass
        return result

    def _get_multiple_instance_settings_by_name(self, cache, model_uri,
                                                instance_names):
        to_get = [n for n in instance_names if cache.get(n) is None]
        if any(to_get):
            url = '%s%s/?name__in=%s&format=json' % (self._settings_url,
                                                     model_uri,
                                                     ','.join(to_get))
            raw = self._get_raw_settings(url)
            for instance in raw['objects']:
                cache[instance['name']] = instance
        result = dict()
        for name in instance_names:
            inst = cache.get(name)
            if inst is not None:
                result[name] = cache[name]
            else:
                raise errors.InvalidSettingsError('"%s" is not defined in '
                                                  'the settings' % 
                                                  instance.name)
        return result

    def _get_all_instances(self, cache, model_uri):
        """
        Query the REST API and return all instances of a given model.

        Cache the fetched instances to prevent multiple queries to the
        API.
        """

        url = '%s%s/?format=json&limit=0' % (self._settings_url, model_uri)
        raw = self._get_raw_settings(url)
        instances = raw['objects']
        for instance in instances:
            cache[instance['name']] = instance
        return instances

    def _get_single_instance_model(self, model_uri):
        """
        Query the REST API and return the instance of a model that has
        only one element.
        """

        url = '%s%s/?format=json' % (self._settings_url, model_uri)
        raw = self._get_raw_settings(url)
        return raw['objects'][0]

    def __getstate__(self):
        d = self.__dict__.copy()
        del d['logger']
        return d

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.logger.addHandler(logging.NullHandler())
