'''
This module holds the GioSource class, which is used to represent the remote
sources of information for the giosystem.
'''

from giosystemcore.settings import get_settings

class GioSource(object):

    def __init__(self, name):
        self.name = name
        self.description = ''
        self.parameters = dict()
        self.search_patterns = []

    @classmethod
    def from_settings(cls, name):
        '''
        Create a new instance from the input name.

        Inputs:

            name - The name of the source to create
        '''

        settings_manager = get_settings()
        source_settings = settings_manager.get_source_settings_by_name(name)
        s = cls(name)
        s.description = source_settings.get('description', '')
        for parameter in source_settings['parameters']:
            s.parameters[parameter['name']] = parameter['value']
        for search_pattern in source_settings.get('search_patterns', []):
            s.search_patterns.append(search_pattern['pattern'])
        return s

    def __repr__(self):
        return '%r(name=%r)' % (self.__class__.__name__, self.name)
