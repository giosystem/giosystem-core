'''
This module holds the GIOFile base class and subclasses for the giosystem 
package.
'''

import os
import re
import logging
import datetime as dt

import sources
import errors
import utilities
import products
from giosystemcore.settings import get_settings
import hosts.hostfactory as hostfactory
import packages.selectionrules as selectionrules


logger = logging.getLogger(__name__)

def get_file(name, timeslot):
    '''
    A factory for returning GioFile objects.
    '''

    gio_file = GioFile.from_settings(name)
    gio_file.selection_rule = selectionrules.OffsetRule(timeslot)
    return gio_file


def get_product_files(product_short_name, timeslot, file_type="netcdf"):
    settings_manager = get_settings()
    files_settings = settings_manager.get_product_files_settings(
        product_short_name, file_type=file_type)
    return [get_file(fs["name"], timeslot) for fs in files_settings]


#TODO
# - DON'T ADD A HOST TO GIOFile's attributes!
# - A GIOFile's timeslot is not to be a mandatory initialization parameter
# The timeslot of a GIOFile will drive other properties like the search paths
# and search pattern
# - A GIOFile's source is not to be a mandatory initialization parameter
# - A GIOFile is to have an 'input_to' attribute. It will be a dictionary with
# package names as keys and a dictionary with specific settings for each input
# as values. There will be an 'optional' attribute in these specific settings
# that will specify if the GIOFile is optional or not
# - A GIOFile is to have an 'output_from' attribute. It will be a dictionary
# with a 'package' key and with other specific settings for that package with
# that GIOFile, like the timeslot alterations
class GioFile(object):

    _source = None
    _product = None
    _search_path = ''
    _search_pattern = ''
    _area = None
    _settings_manager = None

    @property
    def selection_rule(self):
        return self._selection_rule

    @selection_rule.setter
    def selection_rule(self, rule):
        self._selection_rule = rule

    @property
    def timeslot(self):
        return self.selection_rule.timeslot

    @timeslot.setter
    def timeslot(self, timeslot):
        self.selection_rule.base_timeslot = timeslot

    @property
    def year(self):
        return self.selection_rule.year

    @property
    def month(self):
        return self.selection_rule.month

    @property
    def day(self):
        return self.selection_rule.day

    @property
    def julian_day(self):
        return self.selection_rule.julian_day

    @property
    def hour(self):
        return self.selection_rule.hour

    @property
    def minute(self):
        return self.selection_rule.minute

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, source):
        self._source = source

    @property
    def product(self):
        return self._product

    @product.setter
    def product(self, product):
        self._product = product

    @property
    def search_path(self):
        marks_list = [self, self.source]
        search_path = utilities.parse_marks(self._search_path, marks_list)
        if self.selection_rule.timeslot is None and \
                self.selection_rule.reference_timeslot is not None:
            search_path = utilities.replace_age_path(
                search_path,
                self.selection_rule.reference_timeslot
            )
        return search_path

    @search_path.setter
    def search_path(self, path):
        self._search_path = path

    @property
    def search_pattern(self):
        the_pattern = utilities.parse_marks(self._search_pattern, [self,
                                            self.source])
        return the_pattern

    @search_pattern.setter
    def search_pattern(self, pattern):
        self._search_pattern = pattern
        self._area = utilities.find_area(self._search_pattern,
                                         self._settings_manager)

    @property
    def area(self):
        return self._area

    def __init__(self, name):
        self.logger = logger.getChild(self.__class__.__name__)
        self.selection_rule = selectionrules.SelectionRule()
        self._timeslot = None
        self._source = None
        self._product = None
        self._search_path = ''
        self._search_pattern = ''
        self._area = None
        self.name = name
        self.downloadable = False
        self.copy = True
        self.compress = True
        self.archive = True
        self.delete = True
        self.disseminate = True
        self.except_hours = []
        self.product = None
        self.file_type = ''
        self.is_system_input = False
        self.frequency = ''

    @classmethod
    def from_settings(cls, name):
        manager = get_settings()
        file_settings = manager.get_file_settings(name)
        f = cls(name)
        f._settings_manager = manager
        f.frequency = file_settings.get('frequency', 'dynamic')
        f.file_type = file_settings.get('file_type', '')
        f.downloadable = file_settings['downloadable']
        f.copy = file_settings.get('to_copy', True)
        f.compress = file_settings.get('compress', True) # this setting does not exist
        f.archive = file_settings.get('archive', True) # this setting does not exist
        f.delete = file_settings.get('delete', True) # this setting does not exist
        f.disseminate = file_settings.get('disseminate', True) # this setting does not exist
        product_uri = file_settings.get('product', None)
        if product_uri is not None:
            product_settings = manager.get_product_settings(product_uri)
            product_name = product_settings['short_name']
            f.product = products.GioProduct.from_settings(product_name)
        source_uri = file_settings.get('source')
        if source_uri is not None:
            source_settings = manager.get_source_settings(source_uri)
            source_name = source_settings['name']
            f.source = sources.GioSource.from_settings(source_name)
        f.is_system_input = file_settings.get('system_input', False)
        for hour in file_settings.get('except_hours', []):
            f.except_hours.append(hour)
        f.search_path = file_settings.get('search_path', '')
        f.search_pattern = file_settings.get('search_pattern', '')
        return f

    # FIXME - It would be better to have an 'operational' attribute on valid
    # files instead of relying on the 'downloadable' attribute. What if we want
    # testing files to be downloadable as well?
    @classmethod
    def from_file_name(cls, file_name, include_non_downloadable=False,
                       include_testing=False):
        '''
        Return a GioFile from a file name

        Since this method is mainly useful for generating files that are
        already created and are in the process of being made available for
        downloading, it is possible to filter out the files which are not
        downloadable, in order to speed up execution time. This has the added
        benefit of unmasking potential testing files that may have similar
        search_patterns as the operational ones.
        '''

        timeslot = utilities.extract_timeslot_from_file_name(file_name)
        product_name = utilities.extract_product_name_from_file_name(file_name)
        if timeslot is None:
            raise ValueError('Could not extract a timeslot from the file_name')
        possible_files = []
        manager = get_settings()
        for fs in manager.get_all_files_settings().values():
            allowed = False
            if fs["downloadable"] and not fs["testing"]:
                allowed = True
            elif not fs["downloadable"] and include_non_downloadable:
                # we may still make it
                if not fs["testing"]:
                    allowed = True
                elif fs["testing"] and include_testing:
                    allowed = True
            if not allowed:
                continue
            f = fs['search_pattern']
            f = re.sub(r'\{year\}', timeslot.strftime('%Y'), f)
            f = re.sub(r'\{month\}', timeslot.strftime('%m'), f)
            f = re.sub(r'\{day\}', timeslot.strftime('%d'), f)
            f = re.sub(r'\{hour\}', timeslot.strftime('%H'), f)
            f = re.sub(r'\{minute\}', timeslot.strftime('%M'), f)
            f = re.sub(r'\{.*?\}', r'.*', f)
            if re.search(f, file_name) is not None:
                chosen_file = fs
                possible_files.append(chosen_file.copy())
        gio_file = None
        for s in possible_files:
            gf = get_file(s['name'], timeslot)
            try:
                gf_product = gf.product.short_name
                products_match = product_name == gf_product
            except AttributeError: # this gio_file does not have a product
                gf_product = None
                products_match = product_name is None
            if products_match and (re.search(gf.search_pattern,
                    file_name) is not None):
                gio_file = gf
        return gio_file

    def find_in_local_directory(self, directory):
        '''
        This method should be called when searching for the file in specific
        directories on the local host's filesystem. Directories such as a
        package's working_dir. It is not necessary to use this method for 
        finding the file in the standard locations specified in the settings.
        Use the find() method for that.
        '''

        local_host = hostfactory.get_host()
        local_host.logger = self.logger.manager.getLogger(
            '{}:{}'.format(self.logger.name.partition(':')[0],
                           local_host.logger.name.rpartition(':')[2])
        )
        path_pattern = os.path.join(directory, self.search_pattern)
        found_files = local_host.find(path_pattern)
        if any(found_files):
            result = self.selection_rule.filter_paths(found_files)
        else:
            result = None
        return result

    def find_in_host(self, host, protocol=None):
        path_pattern = os.path.join(self.search_path, self.search_pattern)
        if host.__class__.__name__ == "GioRemoteHost":
            found_files = host.find(path_pattern, protocol)
        else:
            found_files = host.find(path_pattern)
        return found_files

    def find_in_data_receiver(self):
        '''
        Find the files specified by this instance in the data receiver host.
        '''

        data_receiver = hostfactory.get_data_receiver()
        path_pattern = os.path.join(data_receiver.inputs_base_dir,
                                    self.search_path,
                                    self.search_pattern)
        found = data_receiver.host.find(path_pattern)
        return found

    def find_in_archive(self):
        '''
        Find the files specified by this instance in an archive host

        Since we cannot know in advance if the file being searched for
        is an input, output, or backup we have to check all the possible
        directories and stop at the first hit.

        Returns:

            A list with the full paths to the files that have been found
        '''

        archive = hostfactory.get_archive()
        archive.host.logger = self.logger.manager.getLogger(
            '{}:{}'.format(self.logger.name.partition(':')[0],
                           archive.host.logger.name.rpartition(':')[2])
        ) 
        found = []
        directories = [
            archive.inputs_base_dir,
            archive.outputs_base_dir,
            archive.backups_base_dir,
        ]
        current_dir = 0
        while not any(found) and current_dir < len(directories):
            path_pattern = os.path.join(directories[current_dir],
                                        self.search_path,
                                        self.search_pattern)
            found = archive.host.find(path_pattern)
            current_dir +=1
        return found

    def find(self, use_io_buffer=True, use_data_receiver=True,
             use_archive=False):
        '''
        Look for this file in the designated hosts.

        When the corresponding arguments are given, searching order is as
        follows:

        1. search the local host
        2. if this file has the `is_system_input` set to True, search the
           `data_receiver` host
        3. search the `io_buffer` host
        4. search the `archive` host

        :arg use_data_receiver: Should the data_receiver be searched when
                                looking for the files? Defaults to True, but
                                please note that only files which have their
                                `is_system_input` attribute set will be
                                searched in the data_receiver host
        :type use_data_receiver: bool

        :arg use_io_buffer: Should the io_buffer host be searched when looking
                            for the files? Defaults to True, as it is expected
                            that most of the files would reside in the 
                            io_buffer when using a distributed system.
        :type use_io_buffer: bool

        :arg use_archive: Should the archive host be searched when looking for
                          the files? Defaults to False, as the io_buffer 
                          should be responsible for supplying most of the 
                          needs for files.
        :type use_archive: bool

        :return: A two-element tuple with the host in which files were found 
                 and a list with the full paths to the files.
        :rtype: (GioHost, [str])
        '''

        the_host = None
        local_host = hostfactory.get_host()
        self.logger.info('Looking for {} - {} ...'.format(self.name,
                         self.search_pattern))
        self.logger.info('Finding in local host...')
        found = self.find_in_host(local_host)
        if any(found):
            the_host = local_host
        elif use_data_receiver and self.is_system_input:
            self.logger.info('Finding in the data_receiver host...')
            found = self.find_in_data_receiver()
            if any(found):
                data_receiver = hostfactory.get_data_receiver()
                the_host = data_receiver.host
        if not any(found) and use_io_buffer:
            self.logger.info('Finding in the io_buffer host...')
            io_buffer = hostfactory.get_io_buffer()
            io_buffer.host.logger = self.logger.manager.getLogger(
                '{}:{}'.format(self.logger.name.partition(':')[0],
                               io_buffer.host.logger.name.rpartition(':')[2])
            )
            if io_buffer.host is not local_host:
                found = self.find_in_host(io_buffer.host)
            if any(found):
                the_host = io_buffer.host
        if not any(found) and use_archive:
            self.logger.info('Finding in the archive host...')
            archive = hostfactory.get_archive()
            archive.host.logger = self.logger.manager.getLogger(
                '{}:{}'.format(self.logger.name.partition(':')[0],
                               archive.host.logger.name.rpartition(':')[2])
            )
            if archive.host is not local_host:
                found = self.find_in_archive()
            if any(found):
                the_host = archive.host
        return the_host, found

    def fetch_from_host(self, source_path, target_directory, host,
                        decompress=True, force=False, protocol=None):
        '''
        Fetch this file from the input host to the target_directory.

        Inputs:

            source_path - A string with the path of the file to fetch

            target_directory - A directory path on the local filesystem where
                the file is to be copied to.

            host - A GIOHost object representing the remote filesystem where
                the file is.

            decompress - A boolean indicating if the returned file should be
                decompressed. This is only aplicable for files which can be
                copied. Files that have the 'copy' attribute set to False will
                not be decompressed.

            force - A boolean indicating if the files must really be fetched
                even if they are already present in the target_directory

        Returns:

            The full path to the fetched file.
        '''


        result = None
        local_host = hostfactory.get_host()
        already_there = self.find_in_local_directory(target_directory)
        if already_there is not None and not force:
            self.logger.debug('file is already there')
            fetched = already_there
        else:
            fetched = local_host.fetch(source_path,
                                       target_directory,
                                       host, protocol)
        result = fetched
        if decompress and (fetched.endswith(".bz2") or 
                           fetched.endswith(".gz")):
            # may need to delete the compressed file
            result = local_host.decompress(result)
        return result

    def fetch(self, target_directory, use_io_buffer=True,
              use_data_receiver=True, use_archive=False, decompress=True,
              force=False, disregard_copy_attribute=False):
        '''
        Fetch this file to the target_directory.

        This method finds files using GIOFile.find() method. After finding,
        it selects the apropriate file to fetch and either copies the file
        over to target_directory or returns a reference to its current path.

        The `target_directory` will be created in case it does not exist.

        The behaviour of this method is overriden by the instance's 'copy'
        attribute.

            - If self.copy is True files will be copied regardless of
                their location being local or remote.

            - If self.copy is False, files are searched in the local host only
                and, if found, their current path is returned.


        :arg target_directory: A directory path on the local filesystem where
                               the file is to be copied to.
        :type target_directory: str

        :arg use_io_buffer: Should the io_buffer host be searched when looking
                            for the files? Defaults to True, as it is expected
                            that most of the files would reside in the 
                            io_buffer when using a distributed system.
        :type use_io_buffer: bool

        :arg use_data_receiver: Should the data_receiver be searched when
                                looking for the files? Defaults to True, but
                                please note that only files which have their
                                `is_system_input` attribute set will be
                                searched in the data_receiver host
        :type use_data_receiver: bool

        :arg use_archive: Should the archive host be searched when looking for
                          the files? Defaults to False, as the io_buffer 
                          should be responsible for supplying most of the 
                          needs for files.
        :type use_archive: bool

        :arg decompress: Should the fetched files be decompressed? This is only
                         aplicable for files which can be copied. Files that 
                         have the 'copy' attribute set to False will not be 
                         decompressed.
        :type decompress: bool

        :arg force: A boolean indicating if the files must really be fetched
                    even if they are already present in the target_directory
        :type force: bool

        :arg disregard_copy_attribute: Should this instance's `copy` attribute 
                                       be diregarded? If True, the files will
                                       be fetched regardless of the `copy` 
                                       attribute.

        :return: The full path to the fetched file
        :rtype: str
        '''

        result = None
        if self.copy or disregard_copy_attribute:
            host, found = self.find(use_io_buffer=use_io_buffer,
                                    use_data_receiver=use_data_receiver,
                                    use_archive=use_archive)
            self.logger.critical("host: {}, found: {}".format(host, found))
            if any(found):
                the_file = self.selection_rule.filter_paths(found)
                if the_file is not None:
                    self.logger.critical("fetching {0}...".format(the_file))
                    result = self.fetch_from_host(the_file, target_directory,
                                                  host, decompress, force)
                else:
                    self.logger.debug('%s: No files are available after '
                                      'applying the selection rule %s' % 
                                      (self.name, self.selection_rule))
        else:
            self.logger.debug('%s does not have the "copy" attribute set. '
                              'Returning the current path for the file' % \
                              self.name)
            local_host = hostfactory.get_host()
            found = self.find_in_host(local_host)
            if any(found):
                result = self.selection_rule.filter_paths(found)
        return result

    def send_to_host(self, path, target_host, target_directory, compress=True):
        '''
        Copy the file to a directory on the target host.

        Inputs:

            path - the full path to the file to send

            target_host - the GioHost object where the file will be sent to

            target_directory - the directory on the target_host where the file
                will be sent to. It will be created if it does not exist

            compress - a boolean specifying if the file should be compressed
                before sending

        Returns:

            A two-value tuple with a boolean specifying the result of the
            operation and the full path to the file on the target host.
        '''

        local_host = hostfactory.get_host()
        if compress:
            path = local_host.compress(path)
        protocol = target_host.remote_protocol
        result, sent_path = local_host.send(path, target_directory,
                                            target_host, protocol=protocol)
        return result, sent_path

    def send(self, host, target_directory, compress=True):
        '''
        Send the file to an input host.

        Inputs:

            host - the target GIOHost where the file is to be sent to

            target_directory - directory on the target host where the file
                should be put. It will be created if it does not exist.
        '''

        local_host = hostfactory.get_host()
        found = self.find_in_host(local_host)
        result = False
        sent_path = None
        if any(found):
            path = self.selection_rule.filter_paths(found)
            if compress:
                path = local_host.compress(path)
            result, sent_path = local_host.send(path, target_directory, host)
        return result, sent_path

    def __repr__(self):
        return 'GIOFile(name=%r)' % self.name
