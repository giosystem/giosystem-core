"""
This module holds the GioPackage base class and subclasses for the 
giosystem package.

The GioPackage API presents three main interaction points:

* GioPackage.run()
* GioPackage.clean_outputs()
* GioPackage.archive()

Usage should follow the following pattern:

* Instantiate a GioPackage object, preferably using the `packagefactory`
  module
* set the relevant instance attributes
* call one of the main interaction points

.. code:: python

   from giosystemcore.settings import get_settings
   from giosystemcore.packages.packagefactory import get_package

   get_settings('http://gio-gl.meteo.pt/giosystem/coresettings/api/v1/')
   package = get_package('process_goes_ref', '201411010900')
   package.use_io_buffer_for_searching = True
   package.compress_outputs = False
   result, details = package.run()
"""

from __future__ import absolute_import
import os
import uuid
import datetime as dt
import logging

from . import selectionrules
from .. import files
from .. import errors
from ..settings import get_settings
from ..hosts import hostfactory


logger = logging.getLogger(__name__)


class GioPackage(object):
    """
    This class is the base for implementing GIO-GL product generating routines

    It is meant for subclassing, and should not be instantiated directly.

    In order to better control execution workflows, some instance
    attributes are defined and can be overriden after a package has been
    created. All of these attributes are boolean and have a default value.
    They are:

    * `use_io_buffer_for_searching` (True) - determines if the package will
      use the io_buffer host to look for files when they cannot be found
      locally;

    * `use_data_receiver_for_searching` (True) - determines if the package
      will use the data_receiver host to look for files when they cannot be
      found locally nor on the io_buffer host. Only files that have the
      `is_system_input` attribute set to True are eligible for finding in the
      data_receiver host. Other files will ignore this search option;

    * `use_archive_for_searching` (False) - determines if the package will
      use the archive host to look for files when they cannot be found
      locally. When used in conjunction with the
      use_io_buffer_for_searching attribute, the archive is only searched
      after the io_buffer and only if the files are not found in the
      io_buffer;

    * `copy_outputs_to_io_buffer` (True) - copy the package's outputs to the
      io_buffer host after execution;

    * `copy_outputs_to_archive` (False) - copy the package's outputs to the
      archive host after execution. when used in the operational processing
      lines, this should be False, as there is a dedicated family in the
      ecflow suite for archiving package outputs

    * `remove_working_directory` (True) - Delete the package's working
      directory after execution is finished. This option is mainly useful
      for debugging and testing. Normally it should be True;

    * `decompress_fetched` (True) - Should the inputs be decompressed when
      they are fetched? This should be True for most packages, but it can
      be useful to be able to disable decompression of inputs for some
      packages, like the fetchData packages;

    * `compress_outputs` (True) - Should the package outputs be compressed
      after execution?

    There is also one extra attribute, which should be a list of hosts:

    * `extra_hosts_to_copy_outputs` ([]) - A list of names of the GioHosts
      that should also receive a copy of the package's outputs after
      execution is finished. When used in the operational processing lines,
      there is no need to specify any additional hosts. However, it can be
      useful for testing purposes or for running packages for other purposes.
    """

    HOURLY_FREQUENCY = "hourly"
    HALF_DAILY_FREQUENCY = "half daily"
    DAILY_FREQUENCY = "daily"
    DECADAL_HOURLY_FREQUENCY = "decadal hourly"
    DECADAL_DAILY_FREQUENCY = "decadal daily"
    host = None
    _timeslot = None
    _inputs = []
    _outputs = []

    @property
    def timeslot(self):
        return self._timeslot

    @timeslot.setter
    def timeslot(self, timeslot):
        if isinstance(timeslot, dt.datetime):
            ts = timeslot
        else:
            ts = dt.datetime.strptime(timeslot, '%Y%m%d%H%M')
        valid_timeslot = self.is_execution_slot(self.execution_frequency,
                                                ts)
        if not valid_timeslot:
            raise errors.InvalidExecutionTimeslot("Invalid timeslot")
        self._timeslot = ts
        self._update_gio_files()

    @property
    def year(self):
        try:
            result = self.timeslot.strftime('%Y')
        except AttributeError:
            result = None
        return result

    @property
    def month(self):
        try:
            result = self.timeslot.strftime('%m')
        except AttributeError:
            result = None
        return result

    @property
    def day(self):
        try:
            result = self.timeslot.strftime('%d')
        except AttributeError:
            result = None
        return result

    @property
    def hour(self):
        try:
            result = self.timeslot.strftime('%H')
        except AttributeError:
            result = None
        return result

    @property
    def minute(self):
        try:
            result = self.timeslot.strftime('%M')
        except AttributeError:
            result = None
        return result

    @property
    def working_dir(self):
        common_path = self.name.replace(' ', '_').lower()
        if self.timeslot is not None:
            common_path = os.path.join(common_path,
                                       self.timeslot.strftime('%Y%m%d%H%M'))
        common_path = os.path.join(common_path, self.uuid)
        result = os.path.join(self.host.temp_dir, common_path)
        return result

    @property
    def inputs(self):
        shown_inputs = []
        for inp, parameters in self._inputs.items():
            if self.timeslot.hour not in parameters['except_hours']:
                shown_inputs.append(inp)
        return shown_inputs

    @property
    def outputs(self):
        shown_outputs = []
        for out, parameters in self._outputs.items():
            if self.timeslot.hour not in parameters['except_hours']:
                shown_outputs.append(out)
        return shown_outputs

    def __init__(self, name):
        self.progress_callback = None
        self.exclude_hours = []
        self.use_io_buffer_for_searching = True
        self.use_data_receiver_for_searching = True
        self.use_archive_for_searching = False
        self.copy_outputs_to_io_buffer = False
        self.copy_outputs_to_archive = False
        self.remove_working_directory = True
        self.decompress_fetched = True
        self.compress_outputs = True
        self.ecflow_limit = ""
        self.extra_hosts_to_copy_outputs = []
        self.uuid = str(uuid.uuid1())
        self.name = name
        self.execution_frequency = None
        self.description = ''
        self.category = None
        self.parameters = dict()
        self._inputs = dict()
        self._outputs = dict()
        self.external_command = None
        self.logger = logger.getChild(self.__class__.__name__)

    @classmethod
    def from_settings(cls, name):
        """
        Return a new GioPackage instance from its predefined name.

        :arg name: The name of this package, as defined in the settings
        :type name: str

        :return: The created instance
        :rtype: giosystemcore.packages.base.GioPackage
        """

        p = cls(name)
        settings_manager = get_settings()
        p._settings_manager = settings_manager
        pack_settings = settings_manager.get_package_settings(name)
        p.description = pack_settings['description']
        p.compress_outputs = pack_settings["compress_outputs"]
        p.ecflow_limit = pack_settings["ecflow_limit"]
        ex_comm = pack_settings['external_command']
        p.external_command = ex_comm if ex_comm != '' else None
        p.exclude_hours = pack_settings['exclude_hours']
        p.host = hostfactory.get_host()
        p.host.logger = p.logger.manager.getLogger(
            '{}:{}'.format(p.logger.name,
                           p.host.logger.name.rpartition(':')[2])
        ) 
        for parameter in pack_settings['parameters']:
            p.parameters[parameter['name']] = parameter['value']
            if parameter["name"] == "extra_hosts":
                extra_hosts = [h.strip() for h in
                               parameter["value"].split(",")]
                p.extra_hosts_to_copy_outputs.extend(extra_hosts)
        return p

    def check_able_to_execute(self, fetched):
        """
        Check whether a package has all the conditions to run.

        This method allows the package to check if the necessary conditions
        for execution are met.
        The default implementation checks if all of the inputs that are not 
        marked as optional have been successfully fetched to the working 
        directory. More specialized behaviour can be reimplemented in child
        classes. This method should raise an exception when the package is
        unable to proceed with the execution.

        :arg fetched: A dictionary with GIOFile objects as keys and the
            fetched paths as values.
        :type fetched: dict(giosystemcore.files.GioFile, [str])
        :returns: A string with additional details.
        :rtype: str
        :raises: `errors.GiosystemCoreError`
        """

        details = ""
        fetched_paths = [p for gio_file, p in fetched.items()]
        if not any(fetched_paths) and len(self.inputs) > 0:
            raise errors.ProcessingError("Could not fetch any of the inputs")
        else:
            missing = []
            for inp, restrictions in self._inputs.items():
                if self.timeslot.hour not in restrictions['except_hours']:
                    if not restrictions['optional'] and not fetched[inp]:
                        missing.append(inp)
            if any(missing):
                raise errors.ProcessingError(
                    "Could not fetch the following mandatory "
                    "inputs: {}".format(missing)
                )
        return details

    def execute(self, fetched=None):
        """
        Perform package specific code.

        This method must be implemented in child classes to provide the
        required functionality. The default implementation does nothing.

        :arg fetched: A dictionary with GIOFile objects as keys and the
            fetched paths as values.
        :type fetched: dict(giosystemcore.files.GioFile, [str])

        :returns: A two-element tuple with a boolean indicating the processing
            result and a string with any extra details.
        :rtype: (bool, str)
        """

        return True, 'Nothing to do'

    def _clean_up(self):
        """
        Remove any temporary files and directories created by this package.

        This method will only remove temporary files and empty directories,
        it does not touch the outputs of the package.
        """

        if self.remove_working_directory:
            self.host.remove_directory(self.working_dir)
        for out in self.outputs:
            if self.host.dir_exists(out.search_path):
                if self.host.is_empty_dir(out.search_path):
                    self.host.remove_directory(out.search_path)

    def clean(self):
        """
        Remove traces of this package's execution.

        This method is one of the main interaction points with GioPackages and
        it will normally be called to force deletion of the package's outputs
        from the local filesystem.

        This method will delete the package's outputs from the local
        filesystem and also remove other potential stuff created by executing
        the package. Child classes can subclass this method.
        """

        return self._delete_outputs()

    def run(self):
        """Execute the package.

        This is the main method for interacting with GIOPackage objects.

        Returns
        -------
        (bool, str)
            A two value tuple holding a boolean with the result of
            execution and a string with relevant details.

        Raises
        ------
        giosystemcore.errors.ProcessingError
            If there is some error with the processing
        """

        result = False
        details = ""
        try:
            self.report("Running pre execution", 3)
            self.run_pre_execution()
            self.report("Fetching inputs", 23)
            fetched = self.fetch_inputs()
            details = self.check_able_to_execute(fetched)
            self.report("Executing main process...", 28)
            result, exec_details = self.execute(fetched)
            details = self._update_details(details, exec_details)
            if not result:
                raise errors.ProcessingError(
                    "Processing failed. Details: {}".format(exec_details))
            if any(self.outputs):
                self.report("Moving outputs to their destination",
                                       68)
                m_detail = self.move_outputs()
                details = self._update_details(details, m_detail)
                if self.copy_outputs_to_io_buffer:
                    self.report("Sending outputs to io_buffer", 90)
                    io_result, io_details = self.send_outputs()
                    details = self._update_details(details, io_details)
                    if not io_result:
                        raise errors.ProcessingError(
                            "Could not send outputs to the io buffer host. "
                            "Details:{}".format(details)
                        )
                if self.copy_outputs_to_archive:
                    self.report("Sending outputs to archive", 93)
                    arch_result, arch_details = self._archive_outputs()
                    details = self._update_details(details, arch_details)
                    if not arch_result:
                        raise errors.ProcessingError(
                            "Could not send outputs to the archive host. "
                            "Details:{}".format(details)
                        )

                extra_hosts_info = self._send_outputs_to_hosts()
                details = self._update_details(details, extra_hosts_info[1])
                if not extra_hosts_info[0]:
                    raise errors.ProcessingError(
                        "Sending the outputs to the extra hosts failed. "
                        "Details: {}".format(extra_hosts_info[1])
                    )
            self.report("Running post execution", 97)
            post_result, post_details = self.run_post_execution()
            details = self._update_details(details, post_details)
            if not post_result:
                raise errors.ProcessingError(
                    "Post-processing failed. Details: {}".format(post_details))
            self.report("Cleaning up", 99)
            self._clean_up()
        except Exception as err:
            self.report("Cleaning up", 99)
            self._clean_up()
            self.report(str(err), percentage=100)
            raise
        self.report(percentage=100)
        return result, details

    def run_pre_execution(self):
        """
        Set some per-package-class initialization veriables.

        This method is called by the run() method right at the start of
        execution. The calling code has a chance to set some package
        variables that it may need. The default implementation does nothing.

        Child classes can reimplement this method when needed, in which case:

        * There shall be no inputs tho this method.
        * There shall be no return value from this method.
        """
        pass

    def run_post_execution(self):
        """
        Execute extra tasks after the package has been run.

        This method is called by the run() method after the outputs
        have been moved to their output directories and sent to the predefined
        hosts. The default implementation does nothing.
        This method should be implemented in child classes to provide extra
        functionality, in which case:

        * There shall be no inputs tho this method.
        * There shall be no return value from this method.

        :return: A two element tuple with the result of the post_execution 
            operation and a string with further details.
        :rtype: (bool, str)
        """

        result = True
        details = ''
        return result, details

    def send_outputs(self):
        """
        Send the outputs to the io_buffer host.

        :return: A tuple with the result of sending the files and a string with
                 additional details.
        :rtype: (bool, str)
        """

        self.logger.info('Sending outputs to iobuffer...')
        io_buffer = hostfactory.get_io_buffer()
        io_buffer.host.logger = self.logger.manager.getLogger(
            '{}:{}'.format(self.logger.name,
                           io_buffer.host.logger.name.rpartition(':')[2])
        )
        result, details = self._send_outputs_to_host(io_buffer.host)
        return result, details

    def _archive_outputs(self):
        """
        Send the outputs to the archive host.

        Returns
        -------
        (bool, str)
            A tuple with the result of sending the files and a string with
            additional details.

        Raises
        ------
        IOError
            If any of the files cannot be archived.

        """

        self.logger.info('Sending outputs to archive...')
        archive = hostfactory.get_archive()
        archive.host.logger = self.logger.manager.getLogger(
            '{}:{}'.format(self.logger.name,
                           archive.host.logger.name.rpartition(':')[2])
        )
        arch_host = archive.host
        base_dir = archive.outputs_base_dir
        all_result = True
        all_details = ""
        for out in self.outputs:
            if self._outputs[out]["should_be_archived"]:
                result, details = self._send_output_to_host(out, arch_host,
                                                            base_dir)
                all_result = all_result and result
                all_details = "; ".join((all_details, details))
        return all_result, all_details

    def archive(self):
        """Archive the relevant files that are produced by this package.

        This method can be reimplemented by child classes if there is a need to
        archive more things than just the package's outputs.

        Returns
        -------
        (bool, str)
            a two element tuple with the result of the whole archive
            operation and any relevant details

        Raises
        ------
        IOError
            If any of the files cannot be archived.

        """

        outputs_result, outputs_details = self._archive_outputs()
        return outputs_result, outputs_details

    def find_inputs(self):
        """Find the package's inputs.

        This method will honor the instance's use_io_buffer_for_searching and
        use_archive_for_searching attributes. As such, it can be made to
        search the following, in order:

        * the local filesystem
        * the data_receiver host
        * the io_buffer host
        * the archive host

        Finding a giosystemcore.files.GioFile involves building a search
        pattern for its corresponding file. The search path is constructed
        by concatenating:

        * the host's data_dirs attribute
        * the file's search_path attribute
        * the file's search_pattern attribute

        :return: A dictionary with outputs as keys and a two element tuple. 
                 This tuple has the GIOHost where the output was found and a 
                 list of found paths.
        :rtype: dict(giosystemcore.files.GioFile: (hosts.GioHost, [str]))
        """

        found = dict()
        for inp in self.inputs:
            found[inp] = inp.find(
                use_io_buffer=self.use_io_buffer_for_searching,
                use_data_receiver=self.use_data_receiver_for_searching,
                use_archive=self.use_archive_for_searching
            )
        return found

    def find_outputs(self):
        """Find the package's outputs.

        The rules for finding an output are the same as the ones described in
        the find_inputs() method

        :return: A dictionary with outputs as keys and a two element tuple.
                 This tuple has the GIOHost where the output was found and a
                 list of found paths.
        :rtype: dict(giosystemcore.files.GioFile: (hosts.GioHost, [str]))
        """

        found = dict()
        for out in self.outputs:
            found[out] = out.find(
                use_io_buffer=self.use_io_buffer_for_searching,
                use_archive=self.use_archive_for_searching
            )
        return found

    def fetch_inputs(self, force=False):
        """
        Fetch the inputs to the working directoy,

        Before fetching, the find_inputs() method is used to locate the files.
        Therefore, the rules for controling where the files should be fethed
        from are the same.

        :arg force: Should the inputs be fetched if they are already present in
                the working directory?
        :type force: bool
        :return: A dictionary with inputs as keys and the path to fetched 
             files as values.
        :rtype: dict(giosystemcore.files.GioFile, str)
        """

        fetched = dict()
        self.logger.info('fetching inputs...')
        for inp in self.inputs:
            fetched_files = inp.fetch(
                self.working_dir,
                use_io_buffer=self.use_io_buffer_for_searching,
                use_data_receiver=self.use_data_receiver_for_searching,
                use_archive=self.use_archive_for_searching,
                decompress=self.decompress_fetched,
                force=force
            )
            fetched[inp] = fetched_files
        return fetched

    # FIXME
    # the first else block can probably be simplified by using the 
    # self._output_must_exist method
    def move_outputs(self):
        """
        Copy the outputs from the working directory to the output directory.

        :return: A string with any additional details
        :rtype: str
        :raises: `giosystemcore.errors.FileNotSentError`,
            `giosystemcore.errors.FileNotFoundError`
        """

        self.logger.info('Moving outputs to output directories...')
        details = []
        for out in self.outputs:
            found = out.find_in_local_directory(self.working_dir)
            if found is not None:
                sent, path = out.send_to_host(found, self.host,
                                              out.search_path,
                                              compress=self.compress_outputs)
                if not sent:
                    raise errors.FileNotSentError(
                        "could not send {} from the working directory to "
                        "its final directory".format(found)
                    )
            else:
                # The file is not in the working dir. Why?
                if out.file_type is not None:
                    if self._outputs[out]['optional']:
                        # ok it wasn't there but it is optional
                        details.append('Could not move "{}" but it is ok, '
                                       'it is optional'.format(out.name))
                    else:  # BAD ROBOT WHAT U DONE NOW?
                        raise errors.FileNotFoundError(
                            "could not find {0.name} {0.timeslot} in the "
                            "working directory".format(out)
                        )
                else:
                    # we're good, there wasn't supposed to be a file
                    pass
        return "; ".join(details)

    def _find_data_in_host(self, giofiles, host_name=None, protocol=None):
        """ This method provides a single interface for
        the all the available 'finding' methods.

        The 'finding' methods are:
            find (default)
            find_in_archive (if the given host_name matches the archive host)
            find_in_host

        The 'selection_rule.filter_paths' is also applied.
        """
        host = hostfactory.get_host(host_name)
        arch = hostfactory.get_archive()
        find_in_archive = False
        if host == arch.host:
            find_in_archive = True
        found_dict = {}
        if find_in_archive:
            for gfile in giofiles:
                found = gfile.find_in_archive()
                if any(found):
                    found_dict[gfile] = (
                        arch.host,
                        gfile.selection_rule.filter_paths(found)
                    )
        elif host_name is None:
            for gfile in giofiles:
                host, found = gfile.find(
                    use_io_buffer=self.use_io_buffer_for_searching,
                    use_data_receiver=self.use_data_receiver_for_searching,
                    use_archive=self.use_archive_for_searching
                )
                if any(found):
                    found_dict[gfile] = (
                        host,
                        gfile.selection_rule.filter_paths(found)
                    )
        else:
            for gfile in giofiles:
                found = gfile.find_in_host(host, protocol)
                if any(found):
                    found_dict[gfile] = (
                        host,
                        gfile.selection_rule.filter_paths(found)
                    )
        return found_dict

    def _compress_outputs(self, host_name=None, protocol=None):
        """Search for the outputs in the search directory and 
        apply bzip2 compression.
        """
        self.logger.info('Compressing outputs...')
        details = []
        local_host = hostfactory.get_host()
        found_dict = self._find_data_in_host(self.outputs,
                                             host_name,
                                             protocol)
        for out, (host, path) in found_dict.iteritems():
            if any(path) and not path.endswith('.bz2'):
                if host.__class__.__name__ == 'GioLocalHost':
                    if any(host.find(path+'.bz2')):
                        # Compressed file already exists.
                        # Remove the decompressed file:
                        host.remove_file(path)
                    else:
                        host.compress(path)
                else:
                    if any(host.find(path+'.bz2')):
                        # Compressed file already exists.
                        # Remove the decompressed file:
                        host.remove_file(path, protocol)
                    else:
                        # Fetch the decompressed remote file to the
                        # local working_dir:
                        fetched = out.fetch_from_host(
                            path,
                            self.working_dir,
                            host,
                            protocol=protocol
                        )
                        # Apply bzip2 compression and send it
                        # back to remote location:
                        compressed = local_host.compress(fetched)
                        dirname = os.path.dirname(path)
                        local_host.send(compressed, dirname, host)
                        # Check compressed file existence at the remote
                        # location and remove the original one, if so:
                        if any(host.find(path+'.bz2')):
                            host.remove_file(path, protocol)

    def report(self, details="", percentage=None):
        """A callback to handle package execution progression."""

        msg = ""
        if self.progress_callback is not None:
            self.progress_callback(details, percentage)
        else:
            if details != "" or percentage is not None:
                if percentage is not None:
                    msg = "{}%".format(percentage)
                if details != "":
                    msg += "- {}".format(details)
                self.logger.info(msg)

    def update_info(self, details="", percentage=None):
        pass

    @staticmethod
    def _update_details(old_details, extra):
        return "; ".join([d for d in (old_details, extra) if d != ""])

    def _delete_file(self, gio_file, must_be_in_archive=False):
        """Delete a single output from the local filesystem"""

        local_paths = gio_file.find(use_io_buffer=False,
                                    use_archive=False)[1]
        deleted_paths = []
        ok_to_delete = False
        if any(local_paths):
            if must_be_in_archive:
                found_in_archive = any(gio_file.find_in_archive())
                if found_in_archive:
                    ok_to_delete = True
                else:
                    self.logger.info("Could not find the file in the archive, "
                                "not deleting")
            else:
                ok_to_delete = True
            if ok_to_delete:
                the_path = gio_file.selection_rule.filter_paths(local_paths)
                the_dir = os.path.dirname(the_path)
                self.host.remove_file(the_path)
                deleted_paths.append(the_path)
                if self.host.is_empty_dir(the_dir):
                    self.host.remove_directory(the_dir)
                    deleted_paths.append(the_dir)
        else:
            self.logger.info("There are no files to delete")
        return deleted_paths

    def _delete_outputs(self):
        deleted_paths = []
        for out in self.outputs:
            deleted = self._delete_file(out)
            deleted_paths.extend(deleted)
        return deleted_paths

    def _create_single_offset_rule(self, specific_settings):
        rule = selectionrules.OffsetRule(
            self.timeslot,
            specific_settings['offset_years'],
            specific_settings['offset_months'],
            specific_settings['offset_days'],
            specific_settings['offset_hours'],
            specific_settings['offset_minutes'],
            specific_settings['offset_decades'],
            specific_settings['single_offset_years'],
            specific_settings['single_offset_months'],
            specific_settings['single_offset_days'],
            specific_settings['single_offset_hours'],
            specific_settings['single_offset_minutes'],
            specific_settings['single_offset_decades'],
        )
        return rule

    def _create_single_age_rule(self, specific_settings):
        rule = selectionrules.AgeRule(
            self.timeslot,
            specific_settings['offset_years'],
            specific_settings['offset_months'],
            specific_settings['offset_days'],
            specific_settings['offset_hours'],
            specific_settings['offset_minutes'],
            specific_settings['offset_decades'],
            specific_settings['single_age_age'],
            specific_settings['single_age_relative'],
            specific_settings['single_age_fix_year'],
            specific_settings['single_age_fix_month'],
            specific_settings['single_age_fix_day'],
            specific_settings['single_age_fix_hour']
        )
        return rule

    def _create_multiple_arbitrary_rules(self, specific_settings):
        rules = selectionrules.create_arbitrary_interval_input_rules(
            self.timeslot,
            {
                'years': specific_settings['offset_years'],
                'months': specific_settings['offset_months'],
                'days': specific_settings['offset_days'],
                'hours': specific_settings['offset_hours'],
                'minutes': specific_settings['offset_minutes'],
                'decades': specific_settings['offset_decades'],
            },
            {
                'years': specific_settings[
                    'multiple_arbitrary_frequency_offset_years'],
                'months': specific_settings[
                    'multiple_arbitrary_frequency_offset_months'],
                'days': specific_settings[
                    'multiple_arbitrary_frequency_offset_days'],
                'hours': specific_settings[
                    'multiple_arbitrary_frequency_offset_hours'],
                'minutes': specific_settings[
                    'multiple_arbitrary_frequency_offset_minutes'],
            },
            {
                'years': specific_settings[
                    'multiple_arbitrary_start_offset_years'],
                'months': specific_settings[
                    'multiple_arbitrary_start_offset_months'],
                'days': specific_settings[
                    'multiple_arbitrary_start_offset_days'],
                'hours': specific_settings[
                    'multiple_arbitrary_start_offset_hours'],
                'minutes': specific_settings[
                    'multiple_arbitrary_start_offset_minutes'],
            },
            {
                'years': specific_settings[
                    'multiple_arbitrary_end_offset_years'],
                'months': specific_settings[
                    'multiple_arbitrary_end_offset_months'],
                'days': specific_settings[
                    'multiple_arbitrary_end_offset_days'],
                'hours': specific_settings[
                    'multiple_arbitrary_end_offset_hours'],
                'minutes': specific_settings[
                    'multiple_arbitrary_end_offset_minutes'],
            },
        )
        return rules

    def _create_multiple_decadal_rules(self, specific_settings):
        rules = selectionrules.create_decadal_interval_input_rules(
            self.timeslot,
            {
                'years': specific_settings['offset_years'],
                'months': specific_settings['offset_months'],
                'days': specific_settings['offset_days'],
                'hours': specific_settings['offset_hours'],
                'minutes': specific_settings['offset_minutes'],
                'decades': specific_settings['offset_decades'],
            },
            specific_settings['multiple_decadal_fix_hour']
        )
        return rules

    def add_gio_file(self, file_role, specific_settings):
        """
        Add a giosystemcore.files.GioFile instance to this package.

        :arg file_role: The role of the file to add. It can be either 'input'
            or 'output'.
        :type file_role: str
        :arg specific_settings: The specific settings for the file, in regard
            to this package
        :type specific_settings: dict()
        """

        mult_map = {
            'single_offset': self._create_single_offset_rule,
            'single_age': self._create_single_age_rule,
            'multiple_arbitrary': self._create_multiple_arbitrary_rules,
            'multiple_decadal': self._create_multiple_decadal_rules,
        }
        rules = mult_map[specific_settings['multiplicity']](specific_settings)
        if not isinstance(rules, list):
            rules = [rules]
        role_map = {
            'input': self._inputs,
            'output': self._outputs,
        }
        for rule in rules:
            file_ = files.GioFile.from_settings(
                specific_settings[file_role]['name']
            )
            file_.logger = self.logger.manager.getLogger(
                '{}:{}'.format(self.logger.name, file_.logger.name)
            )
            file_.selection_rule = rule
            eh = specific_settings['except_hours'].split(',')
            role_map[file_role][file_] = {
                'except_hours': [int(h) for h in eh if h != ''],
                'optional': specific_settings['optional'],
                'should_be_archived':
                    specific_settings.get('should_be_archived'),
                'should_be_cleaned':
                    specific_settings.get('should_be_cleaned'),
                'cleaning_offset':
                    specific_settings.get('cleaning_offset'),
            }

    def _send_outputs_to_host(self, host, base_dir=None,
                              host_data_dirs_index=0):
        """Send all outputs to an input GioHost.

        :arg host: The destination host
        :type host: giosystemcore.hosts.GioHost
        :arg base_dir: An optional base directory on the target host that will
                       serve as the root for sending the files. This argument
                       is useful if sending the files to some host role that
                       defines custom directories, like the archive host role.
        :type base_dir: string
        :return: A tuple with the result of sending the files and a string with
                 additional details.
        :rtype: (bool, str)
        """
        base_dir = base_dir or host.data_dirs[host_data_dirs_index]
        result = True
        details = ""
        for out in self.outputs:
            out_result, out_details = self._send_output_to_host(
                out, host, base_dir,
                host_data_dirs_index=host_data_dirs_index,
            )
            result = result and out_result
            details = "; ".join((details, out_details))
        return result, details

    def _send_output_to_host(self, gio_file, host, base_dir="",
                             host_data_dirs_index=0):
        """Send a single output to a single host"""
        found_paths = gio_file.find(use_archive=False, use_io_buffer=False)[1]
        details = ""
        if any(found_paths):
            the_path = gio_file.selection_rule.filter_paths(found_paths)
            out_dir = os.path.dirname(the_path).replace(
                self.host.data_dirs[host_data_dirs_index],
                base_dir
            )
            result, path = gio_file.send_to_host(
                the_path, host, out_dir,
                compress=self.compress_outputs
            )
        else:
            result = True
            details = '{} was not found locally'.format(gio_file.name)
        return result, details

    def _send_outputs_to_hosts(self):
        """Send the package's outputs to the extra hosts."""

        result = True
        details = ""
        for name in self.extra_hosts_to_copy_outputs:
            host = hostfactory.get_host(name)
            host.logger = self.logger.manager.getLogger(
                '{}:{}'.format(self.logger.name,
                               host.logger.name.rpartition(':')[2])
            )
            self.report("Sending outputs to {}".format(host.name))
            host_result, host_details = self._send_outputs_to_host(host)
            if not host_result:
                result = False
                details = "; ".join((details, host_details))
        return result, details

    def _output_must_exist(self, out):
        """
        """

        result = False
        if out.file_type is not None:
            r = self._outputs[out]
            result = False if r['optional'] else True
        return result

    def _update_gio_files(self):
        self._inputs.clear()
        self._outputs.clear()
        pack_settings = self._settings_manager.get_package_settings(self.name)
        for input_ in pack_settings['inputs']:
            self.add_gio_file('input', input_)
        for output in pack_settings['outputs']:
            self.add_gio_file('output', output)

    def __repr__(self):
        return self.name

    @staticmethod
    def is_execution_slot(execution_frequency, timeslot):
        """Is the timeslot allowed to execute?

        Parameters
        ----------
        execution_frequency: str
            The execution frequency
        timeslot: datetime.datetime
            The timeslot of the package

        """

        allowed_days = range(1, 31 + 1)
        allowed_hours = range(24)
        allowed_minutes = [0]
        if execution_frequency == GioPackage.HALF_DAILY_FREQUENCY:
            allowed_hours = [0, 12]
        elif execution_frequency in (GioPackage.DAILY_FREQUENCY,
                                     GioPackage.DECADAL_DAILY_FREQUENCY):
            allowed_hours = [0]
        if execution_frequency in (GioPackage.DECADAL_HOURLY_FREQUENCY,
                                   GioPackage.DECADAL_DAILY_FREQUENCY):
            allowed_days = [1, 11, 21]
        valid_minute = timeslot.minute in allowed_minutes
        valid_hour = timeslot.hour in allowed_hours
        valid_day = timeslot.day in allowed_days
        result = False
        if valid_minute and valid_hour and valid_day:
            result = True
        return result
