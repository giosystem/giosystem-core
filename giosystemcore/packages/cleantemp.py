"""
This module hold the CleanTempInputs class that deletes the temporary input data
from the TEMP_INPUTS folder used by the FetchData class.
"""

from __future__ import absolute_import
import os
import logging

from .basepackage import GioPackage
from ..hosts import hostfactory


logger = logging.getLogger(__name__)


class CleanTempInputs(GioPackage):
    """
    This package's only function is to delete the system temporary
    input data from the local data receiver. As such it only overrides
    the run() method.
    """

    def __init__(self, *args, **kwargs):
        super(CleanTempInputs, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def run(self, progress_callback=None):
        """
        This class needs no further processing.

        The only purpose for the CleanTempInputs classe is to delete the
        temporary input data from the TEMP_INPUTS folder used by the
        FetchData class. As such there is no need to perform additional
        processing.
        """
        self._delete_inputs()
        return True, ''

    def _delete_inputs(self):
        """
        Delete this package's input files from their original location.

        In order to prevent deleting valuable system input files by accident,
        before deleting any file, this method will look for it in the
        archive. If it is not found, then the local file is not deleted.
        """

        for inp, out in self._match_input_output().iteritems():
            if inp.is_system_input:
                found_in_data_receiver = inp.find_in_data_receiver()
                if any(found_in_data_receiver):
                    dr = inp.selection_rule.filter_paths(
                        found_in_data_receiver)
                    found_in_archive = out.find_in_archive()
                    if not any(found_in_archive):
                        archived, archived_path = self.archive_system_input(
                            dr, out)
                        found_in_archive = [archived_path]
                    if any(found_in_archive):
                        ar = out.selection_rule.filter_paths(found_in_archive)
                        if os.path.basename(dr).replace(".bz2", "") == \
                                os.path.basename(ar).replace(".bz2", ""):
                            data_receiver = hostfactory.get_data_receiver()
                            data_receiver.host.remove_file(dr)
                    else:
                        self.logger.warning('Not deleting {} because it has '
                                       'not been archived yet'.format(dr))

    def archive_system_input(self, path, gio_file):
        """Send some system input path to the archive"""

        archive = hostfactory.get_archive()
        arch_host = archive.host
        base_dir = archive.inputs_base_dir
        dest_dir = os.path.join(base_dir, gio_file.search_path)
        if self._outputs[gio_file]["should_be_archived"]:
            result, details = self.host.send(path, dest_dir,
                                             dest_host=arch_host)
        else:
            result = False
            details = "GioFile {0} is not to be archived".format(gio_file)
        return result, details

    def _match_input_output(self):
        """
        Matches each input with its corresponding output.

        The FetchData package assumes that input and output files have the same
        search_pattern.
        """

        matches = dict()
        out_search_patterns_dict = dict([(out.search_pattern, out) for out in
                                         self.outputs])
        for inp in self.inputs:
            match_out = out_search_patterns_dict.get(inp.search_pattern)
            if match_out is not None:
                matches[inp] = match_out
            #for out in self.outputs:
            #    if inp.search_pattern == out.search_pattern:
            #        matches[inp] = out
        return matches


