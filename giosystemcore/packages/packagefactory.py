"""
Factory for retrieving packages. Assumes settings have already been gotten.
"""

from __future__ import absolute_import
import importlib
from datetime import datetime

from ..settings import get_settings
from .basepackage import GioPackage
from .. import errors


def get_package(name, timeslot):
    """
    Instantiate a new GioPackage using the settings that correspond to the 
    input name.

    This function acts as a factory, generating the correct type of GioPackage
    according to the information defined in the online settings application.

    :arg name: The name of the package, as specified in the online settings
    :type name: str

    :arg timeslot: The timeslot to use
    :type timeslot: str or datetime.datetime

    :return: A specialized giosystemcore package that inherits from 
        giosystemcore.packages.base.GioPackage
    :rtype: giosystemcore.packages.base.GioPackage
    """

    settings_manager = get_settings()
    pack_settings = settings_manager.get_package_settings(name)
    pack_class_path = pack_settings.get('core_class')
    module_path, sep, class_name = pack_class_path.rpartition('.')
    the_module = importlib.import_module(module_path)
    the_class = getattr(the_module, class_name)
    pack = the_class.from_settings(name)
    pack.timeslot = timeslot
    return pack
