"""
This module holds classes that calculate the historical stats used as 
input to the cloud mask algorithm.
"""

from __future__ import absolute_import
import os.path
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class ProcessSatDataStat(ExternalAlgorithm):
    """
    This class should not be instatiated directly. Use the appropriate 
    child class instead.
    """

    def __init__(self, *args, **kwargs):
        super(ProcessSatDataStat, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def _write_algorithm_configuration_file(self):
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_AREAS\n',
            'AreaName = \'{}\'\n'.format(self.AREA),
            '/\n',
            '&NAM_STATS\n',
            'MIN_DAYS = {}\n'.format(
                int(self.parameters.get('MIN_DAYS', '25'))
            ),
            '/\n',
        ]
        path = self.host.create_file(acf_path, contents)
        return path


class ProcessSatDataStatGoes(ProcessSatDataStat):

    AREA = 'GOES-Disk'


class ProcessSatDataStatMtsat(ProcessSatDataStat):

    AREA = 'MTSAT-Disk'


class ProcessSatDataStatHimawari(ProcessSatDataStat):

    AREA = 'HMWR-Disk'
