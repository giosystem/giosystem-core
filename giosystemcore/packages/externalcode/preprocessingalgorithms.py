"""
This module holds classes that perform the pre-processing of the input
data.
"""

from __future__ import absolute_import
import logging
import os
import sys

from .processingalgorithms import ExternalAlgorithm



class PreProcessAlgorithm(ExternalAlgorithm):
    """
    This is the base class for packages that pre-process LRIT files. It
    should not be instatiated directly. Use the appropriate child class
    instead.
    """

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    # this is not a dynamic setting -> do not change it!
    COMPRESSION_LEVEL = 9

    AREA = ""

    def execute(self, fetched):
        self._get_external_program()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.sort()
        num_inputs = len(available_files)
        pcf_path = self._write_product_configuration_file(available_files)
        out, result = self._run_external_algorithm(num_inputs, pcf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def _write_product_configuration_file(self, input_paths):
        """
        Write the product configuration file.

        Inputs:

            input_paths - a list of strings with the paths of the inputs
                to the package

        Returns:

            The full path to the newly-written product configuration file.
        """

        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        contents = ['%s\n' % path for path in input_paths]
        path = self.host.create_file(pcf_path, contents)
        return path

    def _run_external_algorithm(self, num_files, pcf_path):
        binary_path = os.path.join(self.working_dir, self.external_command)
        command = ' '.join((binary_path, self.AREA, self.working_dir,
                           self.working_dir, str(num_files), pcf_path))
        self.logger.debug('running command: %s' % command)
        environment = {
            'LD_LIBRARY_PATH': os.path.join(self.host.code_dir, 'lib'),
            'ENDIAN': sys.byteorder.upper(),
        }
        out, result = self.host.run_program(
            command,
            working_directory=self.working_dir,
            env=environment
        )
        return out, result
