"""
This module holds classes that perform the generation of the LST products from
the LST_g2 external code.
"""

from __future__ import absolute_import
import os
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class ProcessLst(ExternalAlgorithm):
    """
    This is the base class for packages that process the land surface
    temperature. It should not be instatiated directly. Use the
    appropriate child class instead.
    """
    def __init__(self, *args, **kwargs):
        super(ProcessLst, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def _write_algorithm_configuration_file(self):
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        # this is not a great way to determine the area... maybe we can find a
        # better alternative in the future
        sat_name = self.outputs[0].source.name
        contents = [
            '&NAM_AREAS\n',
            'AreaName = \'{}\'\n'.format(self.AREA),
            '/\n',
            '&NAM_SAT\n',
            'SatName = \'{}\'\n'.format(sat_name),
            '/\n',
        ]
        path = self.host.create_file(acf_path, contents)
        return path


class ProcessLstGoes(ProcessLst):

    AREA = 'GOES-Disk'


class ProcessLstMtsat(ProcessLst):

    AREA = 'MTSAT-Disk'


class ProcessLstHimawari(ProcessLst):

    AREA = 'HMWR-Disk'
