"""
This module holds classes that perform the pre-processing of the input
LRIT satellite data.
"""

from __future__ import absolute_import
import logging

from .preprocessingalgorithms import PreProcessAlgorithm

logger = logging.getLogger(__name__)

class PreProcessGrib(PreProcessAlgorithm):
    """
    This is the base class for packages that pre-process GRIB files. It
    should not be instatiated directly. Use the appropriate child class
    instead.
    """

    def __init__(self, *args, **kwargs):
        super(PreProcessGrib, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def run_pre_execution(self):
        """Make sure this package does not compress its outputs.

        Since the underlying algorithm generates outputs that use internal
        compression there should be no need to further compress them.
        """
        super(PreProcessGrib, self).run_pre_execution()
        self.compress_outputs = False


class PreProcessGribGoes(PreProcessGrib):
    AREA = "GOES-Disk"


class PreProcessGribMtsat(PreProcessGrib):
    AREA = "MTSAT-Disk"


class PreProcessGribHimawari(PreProcessGrib):
    AREA = "HMWR-Disk"
