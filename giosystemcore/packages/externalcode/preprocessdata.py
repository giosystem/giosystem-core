"""
This module holds classes that perform the pre-processing of the input
LRIT satellite data.
"""

from __future__ import absolute_import
import os
import logging

from .preprocessingalgorithms import PreProcessAlgorithm

logger = logging.getLogger(__name__)

class PreProcessLrit(PreProcessAlgorithm):
    """
    This is the base class for packages that pre-process LRIT files. It
    should not be instantiated directly. Use the appropriate child class
    instead.
    """

    def __init__(self, *args, **kwargs):
        super(PreProcessLrit, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)


class PreProcessLritGoes(PreProcessLrit):

    AREA = "GOES-Disk"


class PreProcessLritMtsat(PreProcessLrit):

    AREA = "MTSAT-Disk"

class PreProcessHritHimawari(PreProcessLrit):

    AREA = "HMWR-Disk"

    # the name of the algorithm configuration file is hardcoded in the
    # external code so this must not be changed!
    ALGORITHM_CONFIGURATION_FILE_NAME = "staticalgoconfig_0_0_1"

    himawari_channels = {
        "VIS": True,
        "IR1": False,
        "IR2": True,
        "IR3": False,
        "IR4": True,
        "B01": False,
        "B02": False,
        "B04": False,
        "B05": False,
        "B06": False,
        "B09": False,
        "B10": False,
        "B11": False,
        "B12": False,
        "B14": True,
        "B16": False,
    }

    def _write_algorithm_configuration_file(self):
        """Write the algorithm configuration file for the external code.

        The external algorithm expects a configuration file to be present and
        (contrary to the main processing algorithms) this should be
        indicated in the processing configuration file.
        """
        acf_path = os.path.join(self.working_dir,
                                self.ALGORITHM_CONFIGURATION_FILE_NAME)
        channels = ",".join(len(self.himawari_channels) * ["{:b}"]).format(
            self.himawari_channels["B01"],
            self.himawari_channels["B02"],
            self.himawari_channels["VIS"],
            self.himawari_channels["B04"],
            self.himawari_channels["B05"],
            self.himawari_channels["B06"],
            self.himawari_channels["IR4"],
            self.himawari_channels["IR3"],
            self.himawari_channels["B09"],
            self.himawari_channels["B10"],
            self.himawari_channels["B11"],
            self.himawari_channels["B12"],
            self.himawari_channels["IR1"],
            self.himawari_channels["B14"],
            self.himawari_channels["IR2"],
            self.himawari_channels["B16"]
        )
        contents = (
            "&NAM_OUTPUT_COMPRESSION\n"
            "compression_level = {}\n"
            "/\n"
            "&NAM_CHANNEL_VARIABLES\n"
            "Channels2Process = {}\n"
            "/\n".format(self.COMPRESSION_LEVEL, channels)
        )
        self.host.create_directory_tree(os.path.dirname(acf_path))
        with open(acf_path, "w") as fh:
            fh.write(contents)
        return acf_path

    def execute(self, fetched):
        self._get_external_program()
        acf_path = self._write_algorithm_configuration_file()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.append(acf_path)
        available_files.sort()
        num_inputs = len(available_files)
        pcf_path = self._write_product_configuration_file(available_files)
        out, result = self._run_external_algorithm(num_inputs, pcf_path)
        details = ''
        if not result:
            details = out
        return result, details
