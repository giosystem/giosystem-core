"""
This module holds classes that perform the pre-processing of the input
LRIT satellite data.
"""

from __future__ import absolute_import
import os
import re
import datetime as dt
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class PreProcessMgrib(ExternalAlgorithm):
    """
    This is the base class for packages that process the solar angles. It
    should not be instantiated directly. Use the appropriate child class
    instead.
    """

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(PreProcessMgrib, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def execute(self, fetched):
        self._get_external_program()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.sort()
        pcf_path = self._write_product_configuration_file(available_files)
        acf_path = self._write_algorithm_configuration_file()
        out, result = self._run_external_algorithm(acf_path, pcf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def run_pre_execution(self):
        # Reimplementation of in/output selection rool.
        # With this implementation only the latest
        # model run grib files will be fetched... 
        for gfile in dict(self._inputs.items() +
                          self._outputs.items()):
            if re.match('.*\{run_hour\}.*', gfile._search_pattern):
                tmp_timeslot = dt.datetime(
                    self.timeslot.year,
                    self.timeslot.month,
                    self.timeslot.day
                )
                if self.timeslot.hour < 12:
                    tmp_timeslot -= dt.timedelta(hours=12)
                for attribute in ('_search_pattern',
                                  '_search_path'):
                    for param in ('year','month',
                                  'day','hour'):
                        exec("gfile.{0} = re.sub('{{{2}}}', "
                             "'{{:02d}}'.format(tmp_timeslot.{1}), "
                             "gfile.{0})".format(
                                 attribute, param,
                                 'run_hour' if param == 'hour'
                                 else param,
                             ))
            if re.match('.*\{step_hour\}.*',
                        gfile._search_pattern):
                gfile._search_pattern = re.sub(
                    '{step_hour}',
                    "{:02d}".format(12 + self.timeslot.hour % 12),
                    gfile._search_pattern
                )

    def _write_algorithm_configuration_file(self):
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_OUTPUT_COMPRESSION\n',
            'compression_level = {}\n'.format(self.COMPRESSION_LEVEL),
            '/\n',
            ]
        path = self.host.create_file(acf_path, contents)
        return path

    def _write_product_configuration_file(self, input_paths):
        """
        Write the product configuration file.

        Inputs:

            input_paths - a list of strings with the paths of the inputs
                to the package

        Returns:

            The full path to the newly-written product configuration file.
        """

        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        yfileinp = 'YFILEINP='
        input_paths.sort()
        for input_path in input_paths:
            yfileinp += '\'%s\',\n' % input_path
        yfileinp = yfileinp[:-2]  # removing the last newline and comma
        output_paths = []
        for out in self.outputs:
            out_path = os.path.join(self.working_dir, out.search_pattern)
            output_paths.append(out_path)
        output_paths.sort()
        yfileout = 'YFILEOUT='
        for output_path in output_paths:
            yfileout += '\'%s\',\n' % output_path
        yfileout = yfileout[:-2]  # removing the last newline and comma
        contents = [
            '&NAM_ALG_MODES\n',
            'MODE = 1\n',
            '/\n',
            '&NAM_ALG_INPUT_FILES_PATH\n',
            '%s\n' % yfileinp,
            '/\n',
            '&NAM_ALG_OUTPUT_FILES_PATH\n',
            '%s\n' % yfileout,
            '/\n',
            '&NAM_STOP_STATUS_RETRIEVAL_FREQ\n',
            'YFREQINSECS = 1\n',
            '/\n',
        ]
        path = self.host.create_file(pcf_path, contents)
        return path


class PreProcessMgribGoes(PreProcessMgrib):

    AREA = 'GOES-Disk'


class PreProcessMgribMtsat(PreProcessMgrib):

    AREA = 'MTSAT-Disk'


class PreProcessMgribHimawari(PreProcessMgrib):

    AREA = 'HMWR-Disk'


