#-*- encoding: utf-8-*-
"""
This module holds classes that calculate the historical stats used as 
input to the lst10s_soam algorithm.
"""

from __future__ import absolute_import
import datetime as dt
import logging
import os

import pystache

from .processingalgorithms import ExternalAlgorithm
from ... import utilities

logger = logging.getLogger(__name__)


class ProcessLst10Dc(ExternalAlgorithm):
    """This class is used to generate packages for the LST10-DC product.
    """

    ACF_TEMPLATE = "lst10-dc-hourly"
    PRODUCT_NAME = "LST10-DC"
    PARENT_IDENTIFIER = "urn:cgls:global:lst10-dc_v1_0.045degree"
    PRODUCT_IDENTIFIER_PATTERN = (
        "{parent_identifier}:{product_name}_{previous_decade:%Y%m%d%H%M}_"
        "GLOBE_GEO_V1.2"
    )
    BINARY_NAME = 'lst10_g2'

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(ProcessLst10Dc, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def run_pre_execution(self):
        """Make sure this package does not compress its outputs.

        If the underlying algorithm generates outputs that use internal
        compression there should be no need to further compress them.

        """

        super(ProcessLst10Dc, self).run_pre_execution()
        if self.COMPRESSION_LEVEL > 0:
            self.compress_outputs = False

    def execute(self, fetched):
        self._get_external_program()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.sort()
        pcf_path = self._write_product_configuration_file(available_files)
        acf_path = self._write_algorithm_configuration_file()
        out, result = self._run_external_algorithm(pcf_path, acf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def archive(self):
        """Make sure this package does not compress its outputs.

        Since the underlying algorithm is creating HDF5 outputs with
        internal compression enabled, it is not necessary to further compress
        the outputs of this package when sending them to the archive.

        """

        if self.COMPRESSION_LEVEL > 0:
            self.compress_outputs = False
        return super(ProcessLst10Dc, self).archive()

    def _write_product_configuration_file(self, input_paths):
        """Write the product configuration file.
        
        Parameters
        ----------
        input_paths: list
            Paths of the inputs to the package

        Returns
        -------
        str
            Full path to the newly-written product configuration file.

        """
        
        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        yfileinp = 'yfileinp='
        input_paths.sort()
        for input_path in input_paths:
            yfileinp += '\'%s\',\n' % input_path
        yfileinp = yfileinp[:-2]  # removing the last newline and comma
        output_paths = []
        for out in self.outputs:
            out_path = os.path.join(self.working_dir,
                                    out.search_pattern.replace("(|.bz2)$", ""))
            output_paths.append(out_path)
        output_paths.sort()
        yfileout = 'yfileout='
        for output_path in output_paths:
            yfileout += '\'%s\',\n' % output_path
        yfileout = yfileout[:-2]  # removing the last newline and comma
        contents = [
            '&NAM_ALG_MODES\n',
            'MODE = 1\n',
            '/\n',
            '&NAM_ALG_INPUT_FILES_PATH\n',
            '%s\n' % yfileinp,
            '/\n',
            '&NAM_ALG_OUTPUT_FILES_PATH\n',
            '%s\n' % yfileout,
            '/\n',
            '&NAM_STOP_STATUS_RETRIEVAL_FREQ\n',
            'yfreqinsecs = 1\n',
            '/\n',
        ]
        path = self.host.create_file(pcf_path, contents)
        return path

    def _write_algorithm_configuration_file(self):
        encoding = "utf-8"
        template_renderer = pystache.Renderer(
            file_encoding=encoding,
            search_dirs=os.path.join(os.path.dirname(__file__),
                                     "acf_templates")
        )
        template = template_renderer.load_template(self.ACF_TEMPLATE)
        context = self._generate_acf_context()
        rendered = template_renderer.render(template, context)
        acf_path = os.path.join(self.working_dir,
                                "algorithm_configuration_file.txt")
        utilities.create_directory_tree(self.working_dir)
        with open(acf_path, "w") as fh:
            fh.write(rendered.encode(encoding))
        return acf_path

    def _generate_acf_context(self):
        previous_decade = utilities.offset_timeslot(
            self.timeslot, offset_decades=-1)
        date_now = dt.datetime.utcnow()
        time_coverage_start = previous_decade
        time_coverage_end = utilities.offset_timeslot(
            self.timeslot, offset_days=-1, offset_hours=23)
        iso_tz_format = "%Y-%m-%dT%H:%M:%SZ"
        context = {
            "timeslot_iso": previous_decade.strftime("%Y-%m-%d %H:%M:%S"),
            "timeslot_iso_tz": previous_decade.strftime(iso_tz_format),
            "creation_date_iso_tz": date_now.strftime(iso_tz_format),
            "time_coverage_start_iso_tz": time_coverage_start.strftime(
                iso_tz_format),
            "time_coverage_end_iso_tz": time_coverage_end.strftime(
                iso_tz_format),
            "europe_satellite_name": "MSG3",
            "europe_satellite_sensor": "SEVI",
            "america_satellite_name": "GOES13",
            "america_satellite_sensor": "IMAG",
            "asia_satellite_name": "HIMAWARI8",
            "asia_satellite_sensor": "AHI",
            "product_identifier": self.PRODUCT_IDENTIFIER_PATTERN.format(
                parent_identifier=self.PARENT_IDENTIFIER,
                previous_decade=previous_decade,
                product_name=self.PRODUCT_NAME,
            ),
            "parent_identifier": self.PARENT_IDENTIFIER,
            "product_name": self.PRODUCT_NAME,
            "year": previous_decade.year
        }
        return context


class ProcessLst10Tci(ProcessLst10Dc):
    PRODUCT_NAME = "LST10-TCI"
    ACF_TEMPLATE = PRODUCT_NAME.lower()
    PARENT_IDENTIFIER = "urn:cgls:global:lst10-tci_v1_0.045degree"
    PRODUCT_IDENTIFIER_PATTERN = (
        "{parent_identifier}:{product_name}_{previous_decade:%Y%m%d%H%M}_"
        "GLOBE_GEO_V1.2"
    )



class MergeDailyCycle(ProcessLst10Dc):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    def _write_algorithm_configuration_file(self):
        """
        Write a namelist file with the required
        settings for the algorithm configuration.
        """

        return ""


class ProcessLstCl(ProcessLst10Dc):
    """
    This class should not be instantiated directly. Use the appropriate
    child class instead.
    """

    def _write_algorithm_configuration_file(self):
        """
        Write a namelist file with the required
        settings for the algorithm configuration.
        """

        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [

            '&NAM_PRODUCT\n',
            'mode = "climate"\n',
            '/\n',
            
            '&NAM_GENERAL_STR_ATTRS\n',
            'CENTRE                    = "IPMA"\n',
            'ARCHIVE_FACILITY          = "IPMA"\n',
            'PRODUCT                   = "LST_CL"\n',
            'PARENT_PRODUCT_NAME       = "LST"\n',
            'PRODUCT_ALGORITHM_VERSION = "1.0"\n',
            'ORBIT_TYPE                = "GEO"\n',
            'TIME_RANGE                = "one year"\n',
            'Conventions               = "CF-1.5"\n',
            'PROJECTION_NAME           = "Plate Carree"\n',
            '/\n',
            
            '&NAM_GENERAL_INT_ATTRS\n',
            'COMPRESSION    = 0\n',
            '/\n',
            
            '&NAM_DATASET_STR_ATTRS\n',
            'PRODUCT        = "MAX_MAX"            , "MIN_MAX"            , "Q_FLAG"               , "MAX_MAX_UPDATE_TIME", "MIN_MAX_UPDATE_TIME"\n',
            'long_name      = "maximum of maximums", "minimum of maximums", "climate quality flags", "max_max update time", "min_max update time"\n',
            'units          = "Celsius"            , "Celsius"            , "Dimensionless"        , "days"               , "days"\n',
            'coordinates    = "lat lon"            , "lat lon"            , "lat lon"              , "lat lon"            , "lat lon"\n',
            'grid_mapping   = "equirectangular"    , "equirectangular"    , "equirectangular"      , "equirectangular"    , "equirectangular"\n',
            '/\n',

            '&NAM_DATASET_INT_ATTRS\n',
            'PRODUCT_ID     =  255 ,  255 ,  255 ,  255 , 255\n',
            '_FillValue     = -8000, -8000, -8000, -8000, -8000\n',
            'NB_BYTES       =  2   ,  2   ,  2   ,  2   ,  2\n',
            '/\n',
            
            '&NAM_DATASET_FLOAT_ATTRS\n',
            'scale_factor = 0.01, 0.01, 1.0, 0.01, 0.01\n',
            'add_offset   = 0.0 , 0.0 , 0.0, 0.0 , 0.0\n',
            '/\n',

            '&NAM_PROJECTION_STR_ATTRS\n',
            'grid_mapping_name = equirectangular\n',
            'spatial_ref = "PROJCS[\\\"WGS 84 / Plate Carree (deprecated)\\\",GEOGCS[\\\"WGS 84\\\",DATUM[\\\"WGS_1984\\\",SPHEROID[\\\"WGS 84\\\",6378137,298.257223563,AUTHORITY[\\\"EPSG\\\",\\\"7030\\\"]],AUTHORITY[\\\"EPSG\\\",\\\"6326\\\"]],PRIMEM[\\\"Greenwich\\\",0,AUTHORITY[\\\"EPSG\\\",\\\"8901\\\"]],UNIT[\\\"degree\\\",0.0174532925199433,AUTHORITY[\\\"EPSG\\\",\\\"9122\\\"]],AUTHORITY[\\\"EPSG\\\",\\\"4326\\\"]],PROJECTION[\\\"Equirectangular\\\"],PARAMETER[\\\"latitude_of_origin\\\",0],PARAMETER[\\\"central_meridian\\\",0],PARAMETER[\\\"false_easting\\\",0],PARAMETER[\\\"false_northing\\\",0],UNIT[\\\"metre\\\",1,AUTHORITY[\\\"EPSG\\\",\\\"9001\\\"]],AXIS[\\\"X\\\",EAST],AXIS[\\\"Y\\\",NORTH],AUTHORITY[\\\"EPSG\\\",\\\"32662\\\"]]"\n',
            '/\n',

            '&NAM_PROJECTION_DOUBLE_ATTRS\n',
            'false_easting = 0.0\n',
            'false_northing = 0.0\n',
            'inverse_flattening = 298.257223563\n',
            'latitude_of_projection_origin = 0.0\n',
            'longitude_of_central_meridian = 0.0\n',
            'longitude_of_prime_meridian = 0.0\n',
            'semi_major_axis = 6378137.0\n',
            '/\n',

            '&NAM_THREADS\n',
            'n_threads = 5\n',
            'max_size_in_Mbytes = 100\n',
            '/\n',

        ]

        path = self.host.create_file(acf_path, contents)
        return path
