"""
This module holds classes that act as wrappers to other specialized
external code packages.
"""

from __future__ import absolute_import
import os
import sys
import logging

from ... import errors
from ..basepackage import GioPackage



class ExternalAlgorithm(GioPackage):

    # these class variables should be reimplemented by child classes
    AREA = ''

    def execute(self, fetched):
        self._get_external_program()
        available_files = [p for p in fetched.values() if p is not None]
        available_files.sort()
        pcf_path = self._write_product_configuration_file(available_files)
        acf_path = self._write_algorithm_configuration_file()
        out, result = self._run_external_algorithm(pcf_path, acf_path)
        details = ''
        if not result:
            details = out
        return result, details

    def _write_algorithm_configuration_file(self):
        """
        Write a namelist file with the name of the area

        Example:

        &NAM_AREAS
        AreaName = 'GOES-Disk'
        /
        """

        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_AREAS\n',
            'AreaName = \'%s\'\n' % self.AREA,
            '/\n',
        ]
        path = self.host.create_file(acf_path, contents)
        return path

    def _write_product_configuration_file(self, input_paths):
        """
        Write the product configuration file.

        Inputs:

            input_paths - a list of strings with the paths of the inputs
                to the package

        Returns:

            The full path to the newly-written product configuration file.
        """

        pcf_path = os.path.join(self.working_dir,
                                'product_configuration_file.txt')
        yfileinp = 'yfileinp='
        input_paths.sort()
        for input_path in input_paths:
            yfileinp += '\'%s\',\n' % input_path
        yfileinp = yfileinp[:-2]  # removing the last newline and comma
        output_paths = []
        for out in self.outputs:
            out_path = os.path.join(self.working_dir, out.search_pattern)
            output_paths.append(out_path)
        output_paths.sort()
        yfileout = 'yfileout='
        for output_path in output_paths:
            yfileout += '\'%s\',\n' % output_path
        yfileout = yfileout[:-2]  # removing the last newline and comma
        contents = [
            '&NAM_ALG_MODES\n',
            'MODE = 1\n',
            '/\n',
            '&NAM_ALG_INPUT_FILES_PATH\n',
            '%s\n' % yfileinp,
            '/\n',
            '&NAM_ALG_OUTPUT_FILES_PATH\n',
            '%s\n' % yfileout,
            '/\n',
            '&NAM_STOP_STATUS_RETRIEVAL_FREQ\n',
            'yfreqinsecs = 1\n',
            '/\n',
        ]
        path = self.host.create_file(pcf_path, contents)
        return path

    def _run_external_algorithm(self, pcf_path, acf_path):
        binary_path = os.path.join(self.working_dir, self.external_command)
        command = ' '.join((binary_path, pcf_path, acf_path))
        self.logger.debug('running command: %s' % command)
        environment = {
            'LD_LIBRARY_PATH': os.path.join(self.host.code_dir, 'lib'),
            'ENDIAN': sys.byteorder.upper(),
        }
        self.logger.debug('environment: %s' % environment)
        out, result = self.host.run_program(
            command,
            working_directory=self.working_dir,
            env=environment
        )
        return out, result

    def _get_external_program(self):
        """
        Copy the external program to the working directory.
        """

        if self.external_command is not None:
            search_path = os.path.join(self.host.code_dir, 'bin',
                                       self.external_command)
            result, sent_path = self.host.send(search_path, self.working_dir)
            if not result:
                raise errors.ProcessingError(
                    "Could not send {}".format(search_path))
        else:
            raise errors.ProcessingError('Undefined external command.')
        return sent_path
