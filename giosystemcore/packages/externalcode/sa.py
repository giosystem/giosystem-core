"""
This module holds classes that perform the pre-processing of the input
LRIT satellite data.
"""

from __future__ import absolute_import
import os
import logging

from .processingalgorithms import ExternalAlgorithm

logger = logging.getLogger(__name__)

class ProcessSolarAngles(ExternalAlgorithm):
    """
    This is the base class for packages that process the solar angles. It
    should not be instantiated directly. Use the appropriate child class
    instead.
    """

    # compression levels:
    # * 0 - uncompressed
    # * 1 - lowest compression rate
    # * 9 - highest compression rate (smaller files)
    COMPRESSION_LEVEL = 9

    def __init__(self, *args, **kwargs):
        super(ProcessSolarAngles, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def _write_algorithm_configuration_file(self):
        acf_path = os.path.join(self.working_dir,
                                'algorithm_configuration_file.txt')
        contents = [
            '&NAM_AREAS\n',
            'AreaName = \'%s\'\n' % self.AREA,
            '/\n',
            '&NAM_OUTPUT_COMPRESSION\n',
            'compression_level = {}\n'.format(self.COMPRESSION_LEVEL),
            '/\n',
            ]
        path = self.host.create_file(acf_path, contents)
        return path


class ProcessSolarAnglesGoes(ProcessSolarAngles):

    AREA = 'GOES-Disk'


class ProcessSolarAnglesMtsat(ProcessSolarAngles):

    AREA = 'MTSAT-Disk'


class ProcessSolarAnglesHimawari(ProcessSolarAngles):

    AREA = 'HMWR-Disk'

