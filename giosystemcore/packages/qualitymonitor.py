"""
This module holds the QualityMonitor class that deals with the
calculation of quality stats
"""

from __future__ import absolute_import
import logging

import json

import requests

from .basepackage import GioPackage
from ..tools import qualitystats


logger = logging.getLogger(__name__)


class QualityMonitor(GioPackage):

    def __init__(self, *args, **kwargs):
        super(QualityMonitor, self).__init__(*args, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)

    def execute(self, fetched):
        database_url = self.parameters.get("database_api")
        catalogue_endpoint = self.parameters.get("catalogue_endpoint")
        unofficial_catalogue_api = self.parameters.get(
            "unofficial_catalogue_api")
        result = True
        details = "Completed"
        for gio_file, path in fetched.items():
            db_timeslot = gio_file.timeslot.strftime("%Y-%m-%d %H:%M:%S")
            for area in (a for a in qualitystats.AREAS if a.name != "GLOBE"):
                area_stats = qualitystats.calculate_quality_stats(
                    path, catalogue_endpoint,
                    unofficial_catalogue_api, area.name
                )
                if database_url is not None:
                    try:
                        search_response = self.search_in_db(
                            database_url, db_timeslot, area_stats.area,
                            area_stats.product
                        )
                        records = search_response.json().get("results", [])
                        if any(records):
                            self.delete_from_db(database_url, records)
                        self.post_to_db(database_url, db_timeslot, area_stats)
                        self.logger.info("Database interaction successfully "
                                    "completed")
                    except ValueError as err:
                        self.logger.error(err)
                        details = "{}, {}".format(details, err)
                        result = False
        return result, details

    def search_in_db(self, database_url, db_timeslot, area, product):
        """Checks if the input file is already registered in the database"""

        parameters = {
            "product_name": product,
            "area": area.capitalize(),
            "timeslot": db_timeslot
        }
        response = requests.get(database_url, params=parameters)
        if response.status_code != 200:
            raise ValueError("ERROR: The database url was not found")
        return response

    def post_to_db(self, database_url, db_timeslot, quality_stats):
        """Records a new entry in the database"""

        stats = quality_stats._asdict()
        del stats["area"]
        del stats["product"]
        new_entry = {
            "product_name": quality_stats.product,
            "area": quality_stats.area.capitalize(),
            "timeslot": db_timeslot,
            "processing_line": "",
            "parameters": self._serialize_parameters(stats)
        }
        response = requests.post(database_url, data=json.dumps(new_entry),
                               headers={"Content-Type": "application/json"})
        if response.status_code != 201:
            raise ValueError("ERROR: Failed to add new entry in "
                             "database: {}".format(response.text))

    def delete_from_db(self, database_url, found_records):
        """Delete an entry of the database"""
        for record in found_records:
            product_id = record["id"]
            param_url = "{}{}".format(database_url, product_id)
            requests.delete(param_url)

    def _serialize_parameters(self, quality_stats):
        parameters = []
        for name, value in quality_stats.items():
            parameters.append({
                "name": name.replace("_", " "),
                "value": value
            })
        return parameters
