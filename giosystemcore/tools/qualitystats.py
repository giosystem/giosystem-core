#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This script calculates the quality stats from LST products"""

from __future__ import absolute_import
from __future__ import division
from collections import namedtuple
import datetime as dt
import logging
import os
import shlex

from netCDF4 import Dataset
import numpy as np

from ..settings import get_settings
from ..catalogue import cswinterface
from ..utilities import get_unofficial_creation_date

logger = logging.getLogger(__name__)

Area = namedtuple("Area", "name land_pixels "
                          "ini_row end_row "
                          "ini_col end_col")

ProductRange = namedtuple("ProductRange", "name minimum maximum")

QualityStats = namedtuple("QualityStats",
                          "area product mean first_quartile median "
                          "third_quartile percentile_5 percentile_95 "
                          "maximum_without_outliers minimum_without_outliers "
                          "timeliness standard_deviation "
                          "maximum minimum "
                          "positive_outliers negative_outliers "
                          "histogram_frequencies "
                          "processed_pixels file_size")

PRODUCT_RANGES = [
    ProductRange(name="LST",
                 minimum=-70.5,
                 maximum=80.5),
]

#land pixels based on 2016-01-01 00:00 netCDF
AREAS = [
    Area(name="NOAM",
         land_pixels=841599,
         ini_row=0, end_row=1494,
         ini_col=0, end_col=3361),
    Area(name="SOAM",
         land_pixels=781012,
         ini_row=1494, end_row=3584,
         ini_col=0, end_col=3361),
    Area(name="EURO",
         land_pixels=443919,
         ini_row=0, end_row=1031,
         ini_col=3361, end_col=5377),
    Area(name="AFRI",
         land_pixels=1432675,
         ini_row=1031, end_row=3584,
         ini_col=3361, end_col=5377),
    Area(name="ASIA",
         land_pixels=981114,
         ini_row=0, end_row=1792,
         ini_col=5377, end_col=8064),
    Area(name="OCEA",
         land_pixels=440658,
         ini_row=1792, end_row=3584,
         ini_col=5377, end_col=8064),
    Area(name="GLOBE",
         land_pixels=4920977,  # sum of land pixels of the other areas
         ini_row=0, end_row=3584,  # max of the end_row of the other areas
         ini_col=0, end_col=8064)  # max of the enc_col of the other areas
]


def calculate_quality_stats(path, catalogue_endpoint, unofficial_catalogue_api,
                            area_name=None, giosystem_settings_url=None,
                            remove_data_offset=True):
    file_name = os.path.basename(path)
    name_parts = file_name.split("_")
    product_name = name_parts[2]
    timeslot = dt.datetime.strptime(name_parts[3], "%Y%m%d%H%M")
    area_name = (area_name or name_parts[4]).upper()
    netcdf_ds = Dataset(path)
    area = [area for area in AREAS if area.name == area_name][0]
    main_var = netcdf_ds.variables[product_name]
    qflags_var = netcdf_ds.variables['Q_FLAGS']

    main = main_var[0, area.ini_row:area.end_row,
                    area.ini_col:area.end_col].compressed()
    values = main if not remove_data_offset else main - main_var.add_offset
    if main.size != 0:
        qflags = qflags_var[0, area.ini_row:area.end_row,
                            area.ini_col:area.end_col]
        logger.debug("Calculating basic stats...")
        basic_stats = calculate_basic_stats(values)
        logger.debug("Calculating percentile box...")
        percentile_box = calculate_percentile_box(values)
        logger.debug("Calculating outliers...")
        outliers_stats = calculate_outliers_stats(values, area.land_pixels,
                                                  product_name)
        logger.debug("Calculating processed pixels percentage...")
        processed_pixels = calculate_processed_pixels(qflags, area.land_pixels)
        logger.debug("Calculating timeliness...")
    else:  # this area has not been generated
        basic_stats = (0, 0, 0, 0)
        percentile_box = (0, 0, 0, 0, 0)
        outliers_stats = (0, 0, 0, 0)
        processed_pixels = 0

    # "http://geoland2.meteo.pt/giosystem/cataloguehacks/api/v1",
    timeliness = calculate_time_lapse(
        product_name, timeslot,
        catalogue_endpoint,
        unofficial_catalogue_api,
        giosystem_settings_url=giosystem_settings_url
    )
    logger.debug("Calculating histogram...")
    frequencies = calculate_histogram(values)
    stats = QualityStats(area=area.name,
                         product=product_name,
                         mean=basic_stats[0],
                         standard_deviation=basic_stats[1],
                         maximum=basic_stats[2],
                         minimum=basic_stats[3],
                         first_quartile=percentile_box[0],
                         median=percentile_box[1],
                         third_quartile=percentile_box[2],
                         percentile_5=percentile_box[3],
                         percentile_95=percentile_box[4],
                         positive_outliers=outliers_stats[0],
                         negative_outliers=outliers_stats[1],
                         maximum_without_outliers=outliers_stats[2],
                         minimum_without_outliers=outliers_stats[3],
                         processed_pixels=processed_pixels,
                         timeliness=timeliness,
                         histogram_frequencies=frequencies,
                         file_size=os.path.getsize(path)
    )
    netcdf_ds.close()
    return stats


def calculate_basic_stats(data):
    """Calculate some statistical values of the input dataset.

    Parameters
    ----------
    data: numpy.array
        The dataset to extract statistical parameters from

    Returns
    -------
    float, float, float, float
        Returns the mean, standard deviation, maximum and minimum value

    """
    
    mean = np.mean(data)
    std = np.std(data)
    maximum = np.max(data)
    minimum = np.min(data)
    return mean, std, maximum, minimum


def calculate_percentile_box(data):
    """Calculate the values to construct the percentile box.

    Parameters
    ----------
    data: numpy.array
        The dataset to extract statistical parameters from

    Returns
    -------
    float, float, float, float
    1st quartile, 2nd quartile, 3th quartile, percentile 5 and 95 percentile

    """
    
    q1 = np.percentile(data, 25)
    q2 = np.percentile(data, 50)
    q3 = np.percentile(data, 75)
    percentile_5 = np.percentile(data, 5)
    percentile_95 = np.percentile(data, 95)
    return q1, q2, q3, percentile_5, percentile_95


def calculate_outliers_stats(data, number_of_land_pixels, product_name):
    """Calculate outliers.

    Calculate the percentage of outliers and the maximum and minimum value of
    the input dataset without outliers.

    Parameters
    ----------
    data: numpy.array
        The dataset to extract statistical parameters from
    number_of_land_pixels: int
        Number of land pixels of the current tile

    Returns
    -------
    float, float, float, float
        Percentage of positive and negative outliers and the maximmum and
        minimum values without outliers

    """

    product_range = [pr for pr in PRODUCT_RANGES if pr.name == product_name][0]

    # outliers
    positive_outliers = (data > product_range.maximum)
    negative_outliers = (data < product_range.minimum)
    pos_out_percentage = (100. * np.sum(positive_outliers) /
                          number_of_land_pixels)
    neg_out_percentage = (100. * np.sum(negative_outliers) /
                          number_of_land_pixels)
    
    # maximum and minimum values without outliers
    positive_outliers_mask = np.ma.array(data, mask=positive_outliers)
    outliers_mask = np.ma.array(positive_outliers_mask, mask=negative_outliers)
    max_outliers = np.nanmax(outliers_mask)
    min_outliers = np.nanmin(outliers_mask)
    
    return pos_out_percentage, neg_out_percentage, max_outliers, min_outliers


def calculate_processed_pixels(qflags, number_of_land_pixels):
    """Calculate the percentage of processed pixels used on the reprojection

    Parameters
    ----------
    qflags: numpy.array
        cloudiness of each pixel
    number_of_land_pixels: int
        Number of land pixels of the current tile

    Returns
    -------
    float
        Fraction of processed pixels

    """
    
    #(qflags[:]>>2) Moves two bits forward to handle with the Sea/Cloud mask bits
    clear_mask = (qflags[:] >> 2 == 1)
    cont_mask = (qflags[:] >> 2 == 2)
    cloud_mask = (qflags[:] >> 2 == 3)
    #processed_pixels_percentage = 100.0 * np.sum( (cloud_mask) |
    #                                             (clear_mask) |
    #                                             (cont_mask) ) \
    #                                             / number_of_land_pixels
    processed_pixels = np.sum((cloud_mask) | (clear_mask) | (cont_mask))
    percentage = 100 * processed_pixels / number_of_land_pixels
    return percentage


def calculate_time_lapse(product_name, timeslot,
                         catalogue_endpoint, unofficial_catalogue_url,
                         giosystem_settings_url=None):
    """Calculate temporal difference between data acquisition and availability.

    Parameters
    ----------
    product_name: str
        Product being analyzed
    timeslot: datetime.datetime
        Datetime of product acquisition
    catalogue_endpoint: str
        Endpoint of the CSW catalogue where products are made available to
        users
    unofficial_catalogue_url: str
        URL for an unofficial API to query the CSW catalogue's database for
        the actual date and time when a product has been published
    giosystem_settings_url: str, optional
        URL where the settings of the system are queried

    Returns
    -------
    float
        The difference in hours between the time the satellite acquired data
        and the time when the finished product has been made available online
        for users.

    """

    if giosystem_settings_url is not None:
        get_settings(giosystem_settings_url, initialize_logging=False)
    csw_iface = cswinterface.get_catalogue(
        cswinterface.CatalogueType.GEONETWORK,
        catalogue_endpoint=catalogue_endpoint
    )
    ids = csw_iface.find_records_by_pattern(
        "%{}%{}%{}%{}%{}%".format(product_name, timeslot.year, timeslot.month,
                                  timeslot.day, timeslot.hour)
    )
    if len(ids) > 0:
        publication_date = get_unofficial_creation_date(
            ids[0], unofficial_catalogue_url)
        datetime_difference = publication_date - timeslot
        time_lapse = datetime_difference.seconds / 3600.
    else:
        time_lapse = -1
    return time_lapse


def calculate_histogram(data):
    """Calculates the histogram

    Parameters
    ----------
    data: numpy.array
        Dataset to extract statistical parameters from

    Returns
    -------
    str
        Histogram frequencies
    """

    #not_nan = data[~n.isnan(data)]
    freqs, bins = np.histogram(data, bins=range(-100, 101))
    freqs_str = str(freqs)
    freqs_op1 = freqs_str.replace("\n", "")
    freqs_op2 = freqs_op1.replace("[", "")
    freqs_op3 = freqs_op2.replace("]", "")
    histogram_frequencies = ",".join(shlex.split(freqs_op3))
    return histogram_frequencies
