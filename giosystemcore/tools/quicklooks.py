"""A module to generate quicklook files from GIO products"""

from __future__ import absolute_import
import os

from PIL import Image
import mapscript

from mapservertools import mapserver
from mapservertools import colormaps
from netcdf_cglops_tools import netcdfanalyzer

from .. import errors


class QuicklookGenerator(object):

    def create_quicklook_from_netcdf(self, nc_path, band, layer):
        nc_dir = os.path.dirname(nc_path)
        processor = mapserver.MapfileProcessor(name="quicklook_generator",
                                               shape_path=nc_dir)
        layer_name = None
        
        try:
            product_info = netcdfanalyzer.ProductInfo(nc_path)
            layer_name = layer       
            color_map = colormaps.colormap.get(
                    "{}_{}".format(product_info.name.lower(),
                                   layer.lower()))
        except AttributeError:
            raise errors.ProcessingError(
                "could not load color_map for {}".format(layer_name))
        else:
            new_layer = mapserver.MapfileLayer(
                name=layer_name,
                copernicus_product_id=product_info.identifier,
                data_path=product_info.variables[layer_name].gdal_path,
                projection="init=epsg:4326",
                classes=color_map,
                bands=[band],
                status=mapscript.MS_ON,
            )
            processor.layers[new_layer.qualified_name] = new_layer
            output_path = os.path.splitext(nc_path)[0]
            data_img = processor.save_image(output_path)
            legend_img = processor.save_legend(output_path)
            complete_quicklook = self._complete_quicklook(
                data_img, legend_img, (255, 255, 255))
            file_type = data_img.rpartition(".")[-1]
            quicklook_path = "{}_quicklook.{}".format(output_path, file_type)
            complete_quicklook.save(quicklook_path)
            return quicklook_path

    def _complete_quicklook(self, quicklook, legend, background_color,
                            resize=0.5):
        """Complete the quicklook by joining the image and legend.

        :arg quicklook: the full path to the quicklook image file
        :type quicklook: str
        :arg legend: the full path to the legend image file
        :type legend: str
        :arg background_color: a three element tuple with integers specifying
            the RGB color for the map's background.
        :type background_color: tuple
        :arg resize: a number specifying the resize ratio for the quicklook.
            The image is resized before joining with the legend, in order to
            preserve readability.
        :type resize: float
        """

        quick_im = Image.open(quicklook)
        xx, yy = quick_im.size
        new_x = int(xx * resize)
        new_y = int(yy * resize)
        resized = quick_im.resize((new_x, new_y))
        leg_im = Image.open(legend)
        joined = self.join_images(resized, leg_im, background_color)
        padded = self.pad_image(joined, 10, background_color)
        return padded

    @classmethod
    def pad_image(cls, image, ammount, background_color):
        """Add some padding to an image."""

        old_width, old_height = image.size
        width = old_width + ammount
        height = old_height + ammount
        padded = Image.new(image.mode, (width, height), background_color)
        padded.paste(image, (ammount, ammount))
        return padded

    @classmethod
    def join_images(cls, first, second, background_color):
        """Join two images side by side.

        :arg first: First image to join
        :type first: PIL.Image.Image
        :arg second: second image to join
        :type second:PIL.Image.Image
        :arg background_color: a three element tuple with integers
            specifying the RGB color for the map's background.
        :type background_color: tuple

        :returns: A PIL image object with the joined inputs side by side.
        :rtype: PIL.Image.Image
        """

        joined_x = first.size[0] + second.size[0]
        joined_y = max(first.size[1], second.size[1])
        joined = Image.new(first.mode, (joined_x, joined_y), background_color)
        joined.paste(first, (0, 0))
        joined.paste(second, (first.size[0], 0))
        return joined
