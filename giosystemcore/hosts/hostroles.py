import logging

from giosystemcore.settings import get_settings
import hostfactory as hf

class IoBufferHost(object):

    def __init__(self, host):
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.host = host
        self.clean_up_threshold = 95

    @classmethod
    def from_settings(cls):
        settings_manager = get_settings()
        ibs = settings_manager.get_io_buffer_settings()
        host = hf.get_host(ibs['host']['name'])
        io_buffer = cls(host)
        io_buffer.clean_up_threshold = ibs['clean_up_threshold']
        return io_buffer

    def __repr__(self):
        return '%r(host=%r)' % (self.__class__.__name__, self.host)


class ArchiveHost(object):

    def __init__(self, host):
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.host = host
        self.inputs_base_dir = ''
        self.outputs_base_dir = ''
        self.backups_base_dir = ''

    @classmethod
    def from_settings(cls):
        settings_manager = get_settings()
        ars = settings_manager.get_archive_settings()
        host = hf.get_host(ars['host']['name'])
        archive = cls(host)
        archive.inputs_base_dir = ars['inputs_base_dir']
        archive.outputs_base_dir = ars['outputs_base_dir']
        archive.backups_base_dir = ars['backups_base_dir']
        return archive

    def __repr__(self):
        return '%r(host=%r)' % (self.__class__.__name__, self.host)


class DataReceiverHost(object):

    def __init__(self, host):
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.host = host
        self.inputs_base_dir = ''

    @classmethod
    def from_settings(cls):
        settings_manager = get_settings()
        drs = settings_manager.get_data_receiver_settings()
        host = hf.get_host(drs['host']['name'])
        data_receiver = cls(host)
        data_receiver.inputs_base_dir = drs['inputs_base_dir']
        return data_receiver

    def __repr__(self):
        return '%r(host=%r)' % (self.__class__.__name__, self.host)


class WebServerHost(object):

    def __init__(self, host):
        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.host = host
        self.base_url = ''
        self.catalogue_base_uri = ''
        self.catalogue_csw_search_uri = ''
        self.catalogue_csw_transaction_uri = ''
        self.catalogue_username = ''
        self.catalogue_password = ''
        self.ows_base_uri = ''
        self.static_quicklook_base_uri = ''

    @classmethod
    def from_settings(cls):
        settings_manager = get_settings()
        wss = settings_manager.get_web_server_settings()
        host = hf.get_host(wss['host']['name'])
        web_server = cls(host)
        web_server.base_url = wss.get('base_url', '')
        web_server.catalogue_base_uri = wss['catalogue_base_uri']
        web_server.catalogue_csw_search_uri = wss['catalogue_csw_search_uri']
        web_server.catalogue_csw_transaction_uri = \
                wss['catalogue_csw_transaction_uri']
        web_server.catalogue_username = wss['catalogue_username']
        web_server.catalogue_password = wss['catalogue_password']
        web_server.ows_base_uri = wss['ows_base_uri']
        web_server.static_quicklook_base_uri = wss['static_quicklook_base_uri']
        return web_server

    def __repr__(self):
        return '%r(host=%r)' % (self.__class__.__name__, self.host)
