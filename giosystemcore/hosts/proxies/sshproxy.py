'''
Script's doctring goes here.
'''

import logging
from pexpect import spawn
import re
import os

class SSHProxy(object):
    '''
    Connect to another server through SSH and perform various actions.
    '''

    SSH_PROMPT = r'.+@.+:.+\$'
    # for removing ASCII escape characters from the connection's response
    # see:
    #    http://en.wikipedia.org/wiki/ANSI_escape_sequences
    #    http://stackoverflow.com/questions/1833873/python-regex-escape-characters/1834669#1834669
    # for more detail
    CSI_Pattern = r'\x1b\[\d+.?\d*\w?'

    def __init__(self, user=None, ip=None, other_options=None):
        '''
        Inputs:

            user - A string with the name of the user.

            host - A string with the ip or the name of the host.

            other_options - A string with extra commands for the ssh login.
        '''

        self.logger = logging.getLogger('.'.join((__name__,
                                        self.__class__.__name__)))
        self.connection = None
        self.user = user
        self.host = ip
        self.other_options = other_options

    def setup(self, user, host, other_options=None):
        '''
        Inputs:

            user - A string with the name of the user.

            host - A string with the ip or the name of the host.

            other_options - A string with extra commands for the ssh login.
        '''

        self.user = user
        self.host = host
        self.other_options = other_options

    def _connect(self):
        if self.connection is not None and not self.connection.closed:
            self.logger.debug('already connected')
        else:
            self.logger.debug('About to connect to %s' % self.host)
            if self.other_options is not None:
                other_options = self.other_options
            else:
                other_options = ''
            self.connection = spawn('ssh %s %s@%s' % (other_options, self.user,
                                    self.host))
            self.connection.expect(self.SSH_PROMPT)
            result = self.connection.after.splitlines()
            if re.search(self.SSH_PROMPT, result[-1]) is not None:
                self.logger.debug('connection successful')

    def run_command(self, command):
        '''
        Run a command and parse the result into a clean list.

        Inputs:

            command - A string with the full command to run.

        Returns:

            A list of strings with the output of the command.
        '''

        self._connect()
        self.connection.sendline(command)
        result = self.connection.expect(self.SSH_PROMPT)
        output = self.connection.after.splitlines()
        # output [0] is the command that was run
        # output [-1] is the SSH_PROMPT
        # that is why they are stripped from the result
        # also removing ASCII escape characters that serve for
        # outputing color information
        result = [re.sub(self._CSIPattern, '', i) for i in output[1:-1]]
        return result

    def list(self, re_pattern=r''):
        '''
        Perform an 'ls' command using the re_pattern.

        this method needs some more work in order to be consistent.
        '''

        search_dir, search_pattern = os.path.split(re_pattern)
        if search_dir == '':
            search_dir = search_pattern
            search_pattern = '.*'
        if search_pattern == '':
            search_pattern = '.*'
        ls_result = self.run_command('ls -1 %s' % search_dir)
        patt = re.compile(search_pattern)
        result = []
        for line in ls_result:
            if patt.search(line) is not None:
                result.append(os.path.join(search_dir,line))
        return result
