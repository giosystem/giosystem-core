"""
A module providing a class for executing file transfer operations through SFTP.
"""

import socket  # needed to check ftplib's error
socket.setdefaulttimeout(10)  # timeout for ftp connections, in seconds
import os
import re
import subprocess
import sys
from traceback import format_exception_only
import logging

import time
import paramiko
import pysftp

logger = logging.getLogger(__name__)


def get_ssh_proxy(hostname, openssh_config_path="~/.ssh/config"):
    """Generate an appropriate ssh proxy for tunneling ssh connections.
    
    This function is able to use the config files used by OpenSSH and that 
    are usually used to defined ssh proxies.
    
    Parameters
    ----------
    hostname: str
        IP or host name of the proxy machine
    openssh_config_path: str, optional
        Path to the openssh configuration file to use
        
    Returns
    -------
    paramiko.ProxyCommand
        The configured proxy object that can be used to setup the tunneled 
        sftp connection
    dict
        Configuration values for the input host name that have been read from 
        the openssh config file
        
    """

    config = paramiko.SSHConfig()
    config.parse(open(os.path.expanduser(openssh_config_path)))
    host_config = _find_host_config(hostname, config)
    if "proxycommand" in host_config:
        proxy_command = host_config["proxycommand"]
        proxy = paramiko.ProxyCommand(
            subprocess.check_output([
                os.environ.get("SHELL", "/bin/bash"),
                "-c",
                "echo {}".format(proxy_command)
            ]).strip()
        )
    else:
        proxy = None
    return proxy, host_config


def _find_host_config(host_name, openssh_config):
    """Find the configuration for the specified host in openssh config file.
    
    This function looks through the openssh configuration file and tries to
    find a host whose name matches the `host_name` parameter. Name matching
    is done based on `Host` and also on `HostName` entries. This is useful
    for allowing hosts to be assigned nicknames in the openssh config file.
    
    Parameters
    ----------
    host_name: str
        IP or host name of the host to find ocnfiguration for. This is 
        searched for both in `Host` and in `HostName` entries in the openssh 
        config file
    openssh_config: paramiko.config.SSHConfig
        The already loaded ssh configuration
        
    Returns
    -------
    dict
        A mapping with the configuration parameters for the input host
    
    """

    available_hostnames = openssh_config.get_hostnames()
    if host_name in available_hostnames:
        host_config = openssh_config.lookup(host_name)
    else:
        for host_nickname in openssh_config.get_hostnames():
            host_config = openssh_config.lookup(host_nickname)
            if host_name == host_config["hostname"]:
                break
        else:
            host_config = {}
    return host_config


def get_ssh_client(gio_host, openssh_config_path="~/.ssh/config", timeout=20):
    proxy, host_info = get_ssh_proxy(gio_host.ip, openssh_config_path)
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(
            host_info["hostname"],
            username=host_info["user"],
            key_filename=host_info.get("identityfile"),
            sock=proxy,
            timeout=timeout
        )
    except KeyError:
        # this host is not defined in the ssh config file, fallback to
        # connecting with the credentials defined in the host
        client.connect(
            host_info["hostname"],
            username=gio_host.username,
            password=gio_host.password,
            timeout=timeout
        )
    return client


class SFTPClient_RETRY(object):

    def __init__(self, host, uname, pw, max_trys=10, sleep=1):
        self.logger = logger.getChild(self.__class__.__name__)
        self.connection_args = {'host':host,
                                'username':uname,
                                'password':pw}
        self.max_trys = max_trys
        self.sleep = sleep
        self.try_num = 0

    def get_connection(self):
        self.try_num += 1
        try:
            connection = pysftp.Connection(**self.connection_args)
        except pysftp.paramiko.SSHException as e:
            if self.try_num <= self.max_trys:
                self.logger.info(
                    "Reconnecting SFTP client...\n"
                    "Try number: {}/{}".format(self.try_num, self.max_trys))
                time.sleep(self.sleep)
                connection = self.get_connection()
            else:
                self.logger.error(e)
                connection = None
        return connection


class SftpProxySshProxy(object):
    """Use paramiko for establishing SFTP connections using an SSH proxy
    
    This class is useful in order to establish SFTP connections when the 
    remote host is being accessed through another machine, as is the case 
    when using an SSH bastion host.
    
    """

    def __init__(self, remote, openssh_config_path=None):
        self.logger = logger.getChild(self.__class__.__name__)
        self.remote_host = remote
        self.openssh_config_path = openssh_config_path or os.path.expanduser(
            "~/.ssh/config")
        self._ssh_client = None

    def _get_ssh_client(self):
        if self._ssh_client is None:
            self._ssh_client = get_ssh_client(
                self.remote_host,
                openssh_config_path=self.openssh_config_path
            )
        return self._ssh_client

    def find(self, path):
        """Find paths on the remote host
        
        Parameters
        ----------
        path: str
            A pattern representing the paths to be found. This pattern is
            interpreted as a regular expression.
            
        Returns
        -------
        list
            An iterable with all the paths on the remote system that match the
            input pattern.
            
        Examples
        --------
        
        >>> from giosystemcore.hosts.hostfactory import get_host
        >>> local = get_host()
        >>> remote = get_host("dissemination_archive")
        >>> sftp = SftpProxySshProxy(local, remote)
        >>> found = sftp.find("/mnt/data1/someuser/outputs/file_bla_bla.*")
        ['/mnt/data1/someuser/outputs/file_bla_bla1']
            
        """

        found = []
        search_dir, search_pattern = os.path.split(path)
        patt_re = re.compile(search_pattern)
        ssh_client = self._get_ssh_client()
        with ssh_client.open_sftp() as sftp_client:
            try:
                sftp_client.chdir(search_dir)
                raw_file_list = sftp_client.listdir()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(search_dir, raw_path))
            except IOError as err:
                self.logger.debug(err)
        return found

    def fetch(self, path, destination):
        """Fetch the input paths from remoteHost.

        The paths are copied to localHost's destination.

        Parameters
        ----------
        paths: str
            Full path of the file to get.
        destination: str
            The directory on this instance's localHost attribute where the 
            file is to be copied to.

        Returns
        -------
        str
            Full path to the newly fetched file.
            
        """

        if not os.path.isdir(destination):
            try:
                os.makedirs(destination)
            except OSError as e:
                if e.args[0] == 17:  # the directory already exists
                    pass
        dir_path, fname = os.path.split(path)
        full_destination = os.path.join(destination, fname)
        ssh_client = self._get_ssh_client()
        with ssh_client.open_sftp() as sftp_client:
            sftp_client.get(path, full_destination)
        return os.path.join(destination, fname)

    def send(self, path, destination):
        """Send the local path to the remote server.

        Parameters
        ----------
        path: str
            Path in the local file system to be sent to the remote. It is 
            assumed to contain the full path to a file and not a search 
            pattern.

        destination: str
            Path to a directory on the remote server where the path will be 
            put. The directory tree will be created in case it doesn't exist.

        Returns
        -------
        bool
            Overall result of the operation.
            
        """

        result = True
        directory, fname = os.path.split(path)
        ssh_client = self._get_ssh_client()
        with ssh_client.open_sftp() as sftp_client:
            self._create_remote_dirs(destination)
            logger.debug(
                "Sending {!r} to {!r}...".format(
                    path, os.path.join(destination, fname))
            )
            put_result = sftp_client.put(
                path,
                os.path.join(destination, fname)
            )
            logger.debug("put_result: {}".format(put_result))
        return result

    def _create_remote_dirs(self, path):
        ssh_client = self._get_ssh_client()
        stdin, stdout, stderr = ssh_client.exec_command(
            "mkdir -p {}".format(path))
        error_msg = stderr.read()
        if error_msg != "":
            raise RuntimeError(error_msg)



class SftpProxyStateless(object):
    """
    Use SFTP to find and fetch files on a remote location

    This class does not cache the SFTP connection.
    """

    def __init__(self, local, remote):
        self.logger = logger.getChild(self.__class__.__name__)
        self.local_host = local
        self.remote_host = remote

    def find(self, path):
        self.logger.debug("using the stateless sftp to find: {}".format(path))
        found = []
        search_dir, search_pattern = os.path.split(path)
        patt_re = re.compile(search_pattern)
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                sftp.chdir(search_dir)
                current_dir = sftp.getcwd()
                raw_file_list = sftp.listdir()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(current_dir, raw_path))
        except (IOError, pysftp.SSHException) as err:
            self.logger.critical(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.error("Unforeseen exception: {} {}".format(
                err.__class__.__name__, error_msg))
            raise
        return found

    def fetch(self, path, destination):
        self.logger.critical("using the stateless sftp to fetch: {}".format(path))
        copied_path = None
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                old_dir = os.getcwd()
                if not os.path.isdir(destination):
                    try:
                        os.makedirs(destination)
                    except OSError as e:
                        if e.args[0] == 17:
                            # the directory already exists, because some other
                            # concurrent process has created it
                            pass
                os.chdir(destination)
                dir_path, fname = os.path.split(path)
                sftp.get(path)
                copied_path = os.path.join(destination, fname)
                self.logger.critical("contents of destination dir: {}".format(
                                os.listdir(destination)))
                self.logger.critical("copied_path exists: {}".format(
                                os.path.isfile(copied_path)))
                os.chdir(old_dir)
        except (IOError, pysftp.SSHException) as err:
            self.logger.critical(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.critical("Unforeseen exception: {} {}".format(
                            err.__class__.__name__, error_msg))
            raise
        return copied_path

    def send(self, path, destination):
        self.logger.warning("using the stateless sftp to send: {}".format(path))
        self.logger.debug('path: %s' % path)
        self.logger.debug('destination: %s' % destination)
        result = True
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                directory, fname = os.path.split(path)
                dirs = self._create_remote_dirs(destination, sftp)
                if dirs:
                    ret_code = sftp.put(
                        path,
                        os.path.join(destination, fname)
                    )
                else:
                    raise
        except pysftp.SSHException as err:
            self.logger.error(err)
        except Exception as err:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_msg = "".join(format_exception_only(exc_type, exc_value))
            self.logger.critical("Unforeseen exception: {} {}".format(
                            err.__class__.__name__, error_msg))
            raise
        return result

    def delete_file(self, path):
        """
        Delete the remote path.

        :arg path: The remote path to delete
        :type path: str
        """
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                sftp.remove(path)
        except (IOError, pysftp.SSHException) as err:
            self.logger.debug(err)

    def run_command(self, command, local_bin, working_dir=None):
        result = None
        try:
            sftp_retry = SFTPClient_RETRY(
                             self.remote_host.ip, 
                             self.remote_host.username,
                             self.remote_host.password
                         )
            sftp_retry.logger = self.logger.manager.getLogger(
                "{}:{}".format(self.logger.name, sftp_retry.logger.name)
            )
            with sftp_retry.get_connection() as sftp:
                if working_dir is not None:
                    old_dir = sftp.execute("pwd")[0].strip()
                    if local_bin:
                        result = sftp.execute(
                            "cd {} && ./{}".format(working_dir, command))
                    else:
                        result = sftp.execute(
                            "cd {} && {}".format(working_dir, command))
                    sftp.chdir(old_dir)
                else:
                    result = sftp.execute(command)
        except pysftp.SSHException as err:
            self.logger.error(err)
        return result

    def close_connection(self):
        """
        This method is not used by this class. It is kept only for
        maintaining API compatibility.
        """
        self.logger.warning("using the stateless sftp in the close_connection "
                       "method")
        pass

    def _create_remote_dirs(self, path, connection):
        """
        Create the directory structure specified by 'path' on the remote host.
        """

        result = False
        out_list = connection.execute('mkdir -p %s' % path)
        if len(out_list) > 0:
            # Either the path already exists or it could not be created
            if 'exists' in out_list[0]:
                self.logger.info(out_list[0])
                result = True
            else:
                self.logger.error(out_list[0])  # something went wrong
        else:
            result = True
        return result

class SFTPProxy(object):
    """
    Connect to another server through SFTP and perform various actions.

    This class stores the SFTP connection in a local cache.
    """

    def __init__(self, local, remote):
        """
        Inputs:

            host - A G2Host object specifying the host that started this
                connection.
        """

        self.logger = logger.getChild(self.__class__.__name__)
        self.local_host = local
        self.remote_host = remote
        self.connection = None

    def _connect(self):
        result = True
        if self.connection is None:
            try:
                self.logger.debug('Connecting to %s...' % \
                                  self.remote_host.ip)

                sftp_retry = SFTPClient_RETRY(
                                 self.remote_host.ip, 
                                 self.remote_host.username,
                                 self.remote_host.password
                             )
                sftp_retry.logger = self.logger.manager.getLogger(
                    "{}:{}".format(self.logger.name, sftp_retry.logger.name)
                )
                self.connection = sftp_retry.get_connection()
                if self.connection is None:
                    result = False
            except pysftp.paramiko.AuthenticationException:
                self.connection = None
                result = False
        return result

    def find(self, path):
        """
        Return a list of paths that match the 'path' argument.

        Inputs:

            path - A string specifying a regular expression to be
                interpreted as the full directory plus a pattern for
                the file's name
        """

        found = []
        if self._connect():
            search_dir, search_pattern = os.path.split(path)
            patt_re = re.compile(search_pattern)
            try:
                self.connection.chdir(search_dir)
                current_dir = self.connection.getcwd()
                raw_file_list = self.connection.listdir()
                for raw_path in raw_file_list:
                    if patt_re.search(raw_path) is not None:
                        found.append(os.path.join(current_dir,
                                     raw_path))
            except IOError as err:
                self.logger.debug(err)
        else:
            self.logger.error('Not connected to the remote SFTP host')
        return found

    def run_command(self, command, local_bin, working_dir=None):
        result = None
        if self._connect():
            if working_dir is not None:
                old_dir = self.connection.execute('pwd')[0].strip()
                if local_bin:
                    result = self.connection.execute('cd %s && ./%s' % \
                                                     (working_dir, command))
                else:
                    result = self.connection.execute('cd %s && %s' % \
                                                     (working_dir, command))
                self.connection.chdir(old_dir)
            else:
                result = self.connection.execute(command)
        return result

    def delete_file(self, path):
        """
        Delete the remote path.

        :arg path: The remote path to delete
        :type path: str
        """

        if self._connect():
            try:
                self.connection.remove(path)
            except IOError as err:
                self.logger.debug(err)
        else:
            self.logger.error('Not connected to the remote SFTP host')

    def fetch(self, path, destination):
        """
        Fetch the input paths from remoteHost.

        The paths are copied to localHost's destination.

        Inputs:

            paths - A string with the full path of the file to get.

            destination - The directory on this instance's localHost attribute
                where the file is to be copied to.

        Returns:

            The full path to the newly fetched file.
        """

        copied_path = None
        if self._connect():
            old_dir = os.getcwd()
            if not os.path.isdir(destination):
                try:
                    os.makedirs(destination)
                except OSError as e:
                    if e.args[0] == 17:
                        # the directory already exists, because some other
                        # concurrent process has created it
                        pass
            os.chdir(destination)
            dir_path, fname = os.path.split(path)
            self.connection.get(path)
            copied_path = os.path.join(destination, fname)
            os.chdir(old_dir)
        else:
            self.logger.error("Not connected to the remote SFTP host")
        return copied_path

    def send(self, path, destination):
        """
        Put the local path to the remote server.

        Inputs:

            path - A string with the path in the local file system. It is
                assumed to contain the full path to the file and not a search
                pattern.

            destination - The directory on the remote server where the
                path will be put. It will be created in case it doesn't
                exist.

        Returns:

            A boolean specifying the overall result of the operation.
        """

        self.logger.debug('path: %s' % path)
        self.logger.debug('destination: %s' % destination)
        result = True
        if self._connect():
            directory, fname = os.path.split(path)
            dirs = self._create_remote_dirs(destination)
            if dirs:
                ret_code = self.connection.put(
                    path,
                    os.path.join(destination, fname)
                )
            else:
                raise
        else:
            self.logger.error('Not connected to the remote SFTP host')
            result = False
        return result

    def _create_remote_dirs(self, path):
        """
        Create the directory structure specified by 'path' on the remote host.
        """

        result = False
        if self._connect:
            out_list = self.connection.execute('mkdir -p %s' % path)
            if len(out_list) > 0:
                # Either the path already exists or it could not be created
                if 'exists' in out_list[0]:
                    self.logger.info(out_list[0])
                    result = True
                else:
                    # something went wrong
                    self.logger.error(out_list[0])
            else:
                result = True
        else:
            self.logger.error('Not connected to the remote SFTP host')
        return result

    def close_connection(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None



