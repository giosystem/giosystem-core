import os
import re
import bz2
import gzip
import shutil
import logging
from subprocess import Popen, PIPE, STDOUT
from zipfile import ZipFile
import socket

from pathlib2 import Path

from ..settings import get_settings
from .. import errors
from .proxies import ftpproxy
from .proxies import sftpproxy
from .proxies import sshproxy

logger = logging.getLogger(__name__)


class GioHost(object):
    """
    This is the base class for all GioHosts.

    It should be derived for creating custom hosts.
    When creating child classes be sure to adapt the get_host() factory
    with the logic for instantiating them.

    Every derived class must implement the following methods:

        - from_settings(cls, name, settings), a classmethod used to create
            a host from previously available settings
    """

    SSH_PROTOCOL = "ssh"
    FTP_PROTOCOL = "ftp"
    SFTP_PROTOCOL = "sftp"
    SFTP_PROXY_PROTOCOL = "sftp_proxy"

    @property
    def network_name(self):
        return ".".join((self.name, self.domain)) if self.domain != "" \
            else self.name

    def __init__(self, name, data_dirs=None, code_dir="", temp_dir="",
                 description="", domain="", ip="", username="",
                 password="", remote_protocol=None):
        self._settings_manager = None
        self.connections = dict()
        self.name = name
        self.description = description
        self.ip = ip or "127.0.0.1"
        self.domain = domain
        self.username = username
        self.password = password
        self.data_dirs = data_dirs or [str(Path.home() / "data")]
        self.code_dir = code_dir or str(Path.home() / "code")
        self.temp_dir = temp_dir or "/tmp/giosystem"
        self.remote_protocol=remote_protocol or self.SFTP_PROTOCOL
        self.logger = logger.getChild(self.__class__.__name__)

    def close_connections(self):
        """
        This method should always be called when a host is not going to be
        used anymore in order to prevent paramiko errors.
        """

        for connection, conn_dict in self.connections.items():
            sftp_proxy = conn_dict.get("sftp")
            if sftp_proxy is not None:
                sftp_proxy.close_connection()
        self.connections = dict()

    def __repr__(self):
        return "%r(name=%r)" % (self.__class__.__name__, self.network_name)


class GioLocalHost(GioHost):

    @classmethod
    def from_settings(cls, name):
        """
        Inputs:

            name - The name of the host, as defined in the settings
        """

        settings_manager = get_settings()
        host_settings = settings_manager.get_host_settings(name)
        configured_dir = host_settings.get("data_dir")
        h = cls(
            name,
            data_dirs=[configured_dir] if configured_dir is not None else None,
            code_dir=host_settings.get("code_dir", ""),
            temp_dir=host_settings.get("temp_dir", ""),
        )
        h._settings_manager = settings_manager
        h.ip = host_settings.get("ip", "")
        h.username = host_settings.get("username", "")
        h.password = host_settings.get("password", "")
        h.description = host_settings.get("description", "")
        h.domain = host_settings.get("domain", "")
        return h

    def create_file(self, path, contents):
        """
        Create a new text file.

        Inputs:

            path - A string with the path to the new file.

            contents - A list of strings with the lines of text that the
                new file should contain.

        Returns:

            A string with the full path to the newly written file.
        """

        self.create_directory_tree(os.path.dirname(path))
        with open(path, "w") as fh:
            fh.writelines(contents)
        return path

    def create_directory_tree(self, target_directory):

        if not os.path.isdir(target_directory):
            try:
                os.makedirs(target_directory)
            except OSError as e:
                if e.errno == os.errno.EEXIST:
                    pass

    def compress(self, path):
        """
        Compress the files and leave them in the same directory.

        Returns:

            A list with the full paths to the newly compressed files.
        """

        if path.endswith('.bz2'):
            result = path
        else:
            out, return_ = self.run_program('bzip2 -f %s' % path)
            if return_:
                result = '.'.join((path, 'bz2'))
            else:
                raise errors.ExternalProgramError(out)
        return result

    def create_zip(self, paths, zip_path=None):
        if zip_path is None:
            zip_path = os.path.join(os.path.dirname(paths[-1]), "zipped.zip")
        zip_dir, zip_name = os.path.split(zip_path)
        if not self.dir_exists(zip_dir):
            self.create_directory_tree(zip_dir)
        with ZipFile(zip_path, "w") as zip_handler:
            for path in paths:
                zip_handler.write(path, os.path.basename(path))
        return zip_path

    def decompress_old(self, path):
        """
        Decompress the input path leaving it in the same directory.

        Inputs:

            path - Full path to the file that to be decompressed

        Returns:

            A string with the full path to the newly decompressed file.
        """

        self.logger.debug('about to decompress %s' % path)
        if path.endswith('.bz2'):
            out, return_ = self.run_program('bunzip2 %s' % path)
            if return_:
                result = path.replace('.bz2', '')
            else:
                raise errors.ExternalProgramError(out)
        else:
            result = path
        return result

    def decompress(self, path, delete_original=True, buffer_size=-1):
        """
        Decompress the input path, leaving it in the same directory.

        :param path:
        :param buffer_size:
        :return:
        """
        out = os.path.splitext(path)[0]
        with open(path, "rb", buffer_size) as compressed, \
              open(out, "wb", buffer_size) as new_file:
                try:
                    if path.endswith('.bz2'):
                        d = bz2.BZ2Decompressor()
                        for read_data in compressed:
                            new_file.write(d.decompress(read_data))
                    elif path.endswith('.gz'):
                        with gzip.GzipFile(fileobj=compressed) as gzfid:
                            new_file.write(gzfid.read()) 
                    else:
                        raise IOError
                except IOError:
                    self.logger.error('Error decompressing data.'
                                 'File: {}'.format(path))
                    out = None
        if delete_original and out is not None:
            self.remove_file(path)
        return out

    def dir_exists(self, path):
        return os.path.isdir(path)

    def find(self, path):
        """Search for the path in the local directory tree.

        This method is to return all the paths that are found, even if there
        are duplicate files or the same file in compressed and uncompressed
        forms. It will be up to the client code to sort out which paths are
        interesting.

        Parameters
        ----------
        path: str
            Path for the files to search. It is treated as a regular
            expression, so there may be more than one found file.

        Returns
        -------
        list
            Full paths to the found files. Most of the time the list will be
            composed of a single element. It can also hold multiple elements,
            if they all match the regular expression in the input `path`.
            The list will be empty in case no file is found.

        """

        found = []
        if path.startswith(os.path.sep):
            paths_to_scan = [path]
        else:
            paths_to_scan = (os.path.join(d, path) for d in self.data_dirs)
        found = []
        for p in paths_to_scan:
            self.logger.debug("looking for {}".format(p))
            found.extend(self._find_files(p))
        return found

    def fetch(self, path, destination_directory, source=None,
              protocol=None, **kwargs):
        """Fetch the files from the input source host.

        Parameters
        ----------
        path: str
            Full path to the file that should be copied.
        destination_directory: str
            The directory on the local host where the file will be copied to. 
            It will be created in case it doesn't exist.
        source: hosts.GioHost
            A G2host instance representing the host where the file will be 
            copied from. If None (the default) files are fetched from the 
            local host
        protocol: str
            Protocol to use for copying the files
        kwargs: dict
            Extra parameters used to configure a remote host's connection

        Returns
        -------
        str
            Full path to the newly fetched file.
            
        """

        protocol = protocol or self.remote_protocol
        if source is None or source is self:
            self.logger.debug('About to perform a local fetch...')
            result = self._fetch_from_local(path, destination_directory)
        else:
            self.logger.debug('About to perform a remote fetch...')
            result = self._fetch_from_remote(path, destination_directory,
                                             source, protocol, **kwargs)
        return result

    def is_empty_dir(self, path):
        """
        Test if an input directory is empty.

        Raises OSError if directory does not exist.
        """

        result = True
        if any(os.listdir(path)):
            result = False
        return result

    def remove_directory(self, path):
        """
        Remove the input directory and any empty parents

        The directory will be removed along with any contents it may have.
        """

        if self.dir_exists(path):
            parent = os.path.dirname(path)
            shutil.rmtree(path)
            try:
                os.removedirs(parent)
            except OSError:
                pass

    def remove_file(self, path):
        """
        Remove the input file path.
        """

        os.remove(path)

    def run_program(self, command, working_directory=None, env=None,
                    shell=False):
        """Run the external program and wait for it to finish.

        :arg command: The external command to be executed
        :type command: basestring
        :arg working_directory: Directory in which the input command should
                                be run.
        :type working_directory: basestring
        :arg env: environment variables that will be set in the new process
                  that runs the command. A value of None causes the new
                  process to inherit the current environment
        :type env: dict or None
        :return: A two-element tuple with the executed command's standard
                 output and standard error combined into a single string
                 and a boolean with the result.
        """

        old_dir = os.getcwd()
        if working_directory is not None:
            os.chdir(working_directory)
        if env is not None:
            the_env = os.environ
            the_env.update(env)
        else:
            the_env = None
        if shell:
            cmd = command
        else:
            cmd = [c for c in command.split(' ') if c != '']
        self.logger.info('cmd: {}'.format(cmd))
        external_process = Popen(cmd, stdout=PIPE, stderr=STDOUT,
                                 # cwd=working_directory, env=the_env,
                                 env=the_env, shell=shell,
                                 close_fds=True)
        still_running = True
        process_out = ''
        while still_running:
            out_line = external_process.stdout.readline()
            process_out += out_line
            if out_line != '':
                try:
                    self.logger.debug(out_line.strip().encode('utf-8'))
                except UnicodeDecodeError as err:
                    self.logger.warning(err)
                    pass
            else:
                still_running = False

        exit_code = external_process.wait()
        os.chdir(old_dir)
        result = True if exit_code == 0 else False
        return process_out, result

    def send(self, path, dest_dir, dest_host=None, protocol=None, **kwargs):
        """Copy file to another directory, located on a G2Host machine.

        Inputs:
            paths - A string with the full path of the file to send.

            dest_dir - A string with the relative path on the destination
                host where the file is to be sent to.

            dest_host - A G2Host instance, specifying the machine that will
                receive the file. A value of None (which is also the default)
                is interpreted as meaning a local host.

            protocol - a string with the name of the protocol to use
                when sending the file to a remote host.

        Returns:
        """

        protocol = protocol or self.remote_protocol
        if (dest_host is self) or (dest_host is None):
            result, sent_path = self._send_to_local(path, dest_dir)
        else:
            self.logger.debug('About to perform a remote send...')
            result, sent_path = self._send_to_remote(
                path, dest_dir, dest_host, protocol, **kwargs)
        return result, sent_path

    def _find_files(self, regular_expression_path):
        found_files = []
        search_dir, search_pattern = os.path.split(regular_expression_path)
        patt = re.compile(search_pattern)
        if os.path.isdir(search_dir):
            for item in os.listdir(search_dir):
                if patt.search(item) is not None:
                    found_files.append(os.path.join(search_dir, item))
        return found_files

    def _fetch_from_local(self, path, destination_directory):
        """
        Copy a path to a specified destination directory.

        Inputs:

            path - A string with the full path to copy

            destination_directory - The path to the directory where
                the path is to be copied to. It will be created if
                it does not exist.
        Returns:

            The full path to the new copied file.
        """

        self.create_directory_tree(destination_directory)
        try:
            self.logger.debug('path: %s' % path)
            self.logger.debug('destination_directory: %s' %
                              destination_directory)
            shutil.copy(path, destination_directory)
            new_path = os.path.join(destination_directory,
                                    os.path.basename(path))
        except shutil.Error:
            self.logger.error('Unable to copy %s to %s' % (path,
                                                      destination_directory))
            new_path = None
        return new_path

    def _fetch_from_remote(self, path, destination_directory, remote_host,
                           protocol, **kwargs):
        """Copy a path to a specified destination directory.

        Parameters
        ----------
        path: str
        destination_directory: str
        remote_host: hosts.GioRemoteHost

        Returns
        -------
        str
            Full path to the newly fetched files.
            
        """

        connection = self._get_connection(remote_host, protocol, **kwargs)
        new_path = connection.fetch(path, destination_directory)
        return new_path

    def _send_to_local(self, path, dest_dir, data_dir_index=0):
        """Perform a local copy operation.

        Parameters
        ----------
        path: str
            The file that is to be copied
        dest_dir: str
            Where to copy the file to. This method behaves differently
            according to the type of dest_dir given:

            * An absolute path (one that starts with '/' means that a normal
              copy from `path` to `dest_dir` will happen;
            * A relative path (one that does not start with '/') means that
              `path` will be prepended with one of the available `data_dirs`
              on the object. The used data_dir can be specified with the
              optional `data_dir_index` keyword argument.
        data_dir_index: int, optional
            This parameter can be used to specify which of the object's
            `data_dirs` directory will be used for relative copies.

        Returns
        -------
        tuple
            A boolean specifying the overall result of the operation and also
            the list of full paths to the newly sent file's location.

        """

        if dest_dir.startswith("/"):
            full_dest_dir = dest_dir
        else:
            full_dest_dir = os.path.join(self.data_dirs[data_dir_index],
                                         dest_dir)
        full_dest_path = os.path.join(full_dest_dir, os.path.basename(path))
        result = None
        sent = True
        try:
            self.create_directory_tree(full_dest_dir)
            if full_dest_path == path:
                self.logger.debug("The file is already in place.")
            else:
                if os.path.isfile(full_dest_path):
                    self.logger.debug("There is an old file present. "
                                      "Overwriting...")
                    os.remove(full_dest_path)
                shutil.copy(path, full_dest_dir)
            result = os.path.join(full_dest_path)
        except IOError:
            sent = False
            self.logger.exception("Couldn't copy {} to {}".format(path,
                                                             full_dest_dir))
        return sent, result

    def _send_to_remote(self, path, dest_dir, dest_host, protocol,
                        remote_data_dir_index=0, **kwargs):
        """Copy a local file to a remote host

        Parameters
        ----------
        path: str
            The file that is to be copied
        dest_dir: str
            Where to copy the file to. This method behaves differently
            according to the type of dest_dir given:

            * An absolute path (one that starts with '/' means that a normal
              copy from `path` to `dest_dir` will happen;
            * A relative path (one that does not start with '/') means that
              `path` will be prepended with one of the available `data_dirs`
              on the object. The used data_dir can be specified with the
              optional `data_dir_index` keyword argument.
        dest_host: GioRemoteHost
            The remote host where the input path is to be copied to
        protocol: str
            What protocol to use for sending the file
        data_dir_index: int, optional
            This parameter can be used to specify which of the object's
            `data_dirs` directory will be used for relative copies.

        """

        connection = self._get_connection(dest_host, protocol, **kwargs)
        if not dest_dir.startswith("/"):
            dest_dir = os.path.join(
                dest_host.data_dirs[remote_data_dir_index], dest_dir)
        return_result = connection.send(path, dest_dir)
        if return_result:  # sending went OK
            remote_path = os.path.join(dest_dir, os.path.basename(path))
        else:
            remote_path = None
        return return_result, remote_path

    def _connect(self, host, protocol, **kwargs):
        """
        Establish a connection between the local host and the 'host' argument
        using 'protocol'.

        Inputs:

            host - A G2Host object

            protocol -
        """

        if self.connections.get(host.name) is None:
            # create the dictionary that will hold the FTP and SSH connections
            self.connections[host.name] = dict()
        if self.connections.get(host.name).get(protocol) is None:
            if protocol == self.SSH_PROTOCOL:
                self.connections[host.name][protocol] = sshproxy.SSHProxy(
                    host.user, host.ip)
            elif protocol == self.FTP_PROTOCOL:
                self.connections[host.name][protocol] = ftpproxy.FTPProxy(
                    self, host)
            elif protocol == self.SFTP_PROTOCOL:
                #self.connections[host.name][protocol] = sftpproxy.SFTPProxy(
                #    self, host)
                self.connections[host.name][protocol] = \
                    sftpproxy.SftpProxyStateless(self, host)
            elif protocol == self.SFTP_PROXY_PROTOCOL:
                sftp_proxy = sftpproxy.SftpProxySshProxy(
                    host,
                    openssh_config_path=kwargs.get("openssh_config_path")
                )
                self.connections[host.name][protocol] = sftp_proxy

        return self.connections[host.name][protocol]

    def _get_connection(self, host, protocol, **kwargs):
        """
        Return the relevant connection object.

        Inputs:

            host - A G2Host object.

            protocol - A string with the name of the connection protocol.
                Currently planned protocols are 'ftp' and 'sftp'.
        """

        host_connections = self.connections.get(host.name)
        if host_connections is not None:
            connection = host_connections.get(protocol)
            if connection is not None:
                self.logger.debug('Reusing previously opened connection to %s.' \
                                  % host.name)
                result = connection
            else:
                self.logger.debug('Creating connection to %s with protocol %s' %
                                  (host.name, protocol))
                result = self._connect(host, protocol, **kwargs)
        else:
            self.logger.debug(
                'Creating the first connection to %s with protocol %s' %
                (host.name, protocol))
            result = self._connect(host, protocol, **kwargs)
        return result


class GioRemoteHost(GioHost):
    def __init__(self, name, local_host, **kwargs):
        super(GioRemoteHost, self).__init__(name, **kwargs)
        self.logger = logger.getChild(self.__class__.__name__)
        self._local_connection = {
            self.SSH_PROTOCOL: sshproxy.SSHProxy(self.username, self.ip),
            self.FTP_PROTOCOL: ftpproxy.FTPProxy(local_host, self),
            #self.SFTP_PROTOCOL: sftpproxy.SFTPProxy(local_host, self),
            self.SFTP_PROTOCOL: sftpproxy.SftpProxyStateless(local_host, self),
            self.SFTP_PROXY_PROTOCOL: sftpproxy.SftpProxySshProxy(local_host,
                                                                  self),
        }

    @classmethod
    def from_settings(cls, name):
        local_host = GioLocalHost.from_settings(socket.gethostname())
        settings_manager = get_settings()
        host_settings = settings_manager.get_host_settings(name)
        configured_dir = host_settings.get("data_dir")
        h = cls(
            name, local_host,
            username=host_settings.get("username", ""),
            password=host_settings.get("password", ""),
            data_dirs=[configured_dir] if configured_dir is not None else None,
            code_dir=host_settings.get("code_dir"),
            temp_dir=host_settings.get("temp_dir"),
            domain=host_settings.get("domain", ""),
            ip=host_settings.get("ip", ""),
        )
        h._settings_manager = settings_manager
        return h

    def find(self, path, protocol=None):
        """Search for the path in the local directory tree.

        This method is to return all the paths that are found, even if there
        are duplicate files or the same file in compressed and uncompressed
        forms. It will be up to the client code to sort out which paths are
        interesting.

        Parameters
        ----------
        path: str
            Path for the files to search. It is treated as a regular
            expression, so there may be more than one found file.
        protocol: str, optional
            Name of the protocol used to find the files. Available values are
            'sftp' (the default) and 'ftp'.

        Returns
        -------
        list
            Full paths to the found files. Most of the time the list will be
            composed of a single element. It can also hold multiple elements,
            if they all match the regular expression in the input `path`.
            The list will be empty in case no file is found.

        """

        if path.startswith(os.path.sep):
            paths_to_scan = [path]
        else:
            paths_to_scan = (os.path.join(d, path) for d in self.data_dirs)
        found = []
        protocol = protocol or self.remote_protocol
        for p in paths_to_scan:
            self.logger.debug("looking for {}".format(p))
            found.extend(self._local_connection[protocol].find(p))
        return found

    def remove_file(self, path, protocol=None):
        """
        Remove the input file path.

        :arg path: The full path on the remote host to the file is to be 
                   deleted
        :type path: str

        :arg protocol: the backend protocol to use for connecting to the remote
                       server.
        :type protocol: str
        """

        protocol = protocol or self.remote_protocol
        self._local_connection[protocol].delete_file(path)

    def close_connections(self):
        super(GioRemoteHost, self).close_connections()
        self._local_connection['sftp'].close_connection()
