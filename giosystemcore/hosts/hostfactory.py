"""
Various factory functions for retrieving hosts for giosystem.

All these functions assume that a global object that has the settings for
operations has already been initialized and will throw an error otherwise.
"""

import socket
import logging

from .. import errors
from ..settings import get_settings
from . import hosts
from . import hostroles

logger = logging.getLogger(__name__)

# caching hosts
_hosts = dict()
_io_buffer = None
_archive = None
_data_receiver = None
_web_server = None


def clear_all_hosts():
    global _hosts
    global _io_buffer
    global _archive
    global _data_receiver
    global _web_server
    for host in _hosts.values():
            host.close_connections()
    _hosts = dict()
    _io_buffer = None
    _archive = None
    _data_receiver = None
    _web_server = None


def get_host(name=None):
    """A Factory for creating hosts.

    This function implements a caching mechanism and will reuse any previously
    created hosts with the same name as the input argument. This should
    prevent creating different objects for representing the same host and
    opening up unneeded network connections.

    Parameters
    ----------
    name: str, optional
        Name of the host to create. If None (the default) the name used will
        be the local host's name.

    """

    global _hosts
    local_name = socket.gethostname()
    name = local_name if name is None else name
    if name not in _hosts.keys():
        settings_manager = get_settings()
        try:
            host_settings = settings_manager.get_host_settings(name)
            if host_settings['active']:
                if name == local_name:
                    result = hosts.GioLocalHost.from_settings(name)
                else:
                    result = hosts.GioRemoteHost.from_settings(name)
                _hosts[name] = result
            else:
                raise errors.InvalidSettingsError(
                    "Host {} is disabled in the settings.".format(name))
        except errors.InvalidSettingsError as err:
            _hosts[name] = hosts.GioLocalHost(name)
            logger.warning(err)
    remote_protocol = "sftp_proxy" if "dissemination" in name else "sftp"
    _hosts[name].remote_protocol=remote_protocol
    return _hosts[name]


def get_io_buffer():
    global _io_buffer
    if _io_buffer is None:
        _io_buffer = hostroles.IoBufferHost.from_settings()
    return _io_buffer


def get_archive():
    global _archive
    if _archive is None:
        _archive = hostroles.ArchiveHost.from_settings()
    return _archive


def get_data_receiver():
    global _data_receiver
    if _data_receiver is None:
        _data_receiver = hostroles.DataReceiverHost.from_settings()
    return _data_receiver


def get_web_server():
    global _web_server
    if _web_server is None:
        _web_server = hostroles.WebServerHost.from_settings()
    return _web_server
