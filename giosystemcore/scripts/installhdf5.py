'''
A script to automate the installation of HDF5 and assorted libraries.
'''

import argparse
import os

import giosystemcore.settings
from giosystemcore.hosts import hostfactory

HDF5_URL = ('http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.14/src/'
            'hdf5-1.8.14.tar.gz')
SZIP_URL = ('http://www.hdfgroup.org/ftp/lib-external/szip/2.1/src/'
           'szip-2.1.tar.gz')
ZLIB_URL = 'http://zlib.net/zlib-1.2.11.tar.gz'
EMOSLIB_URL = ('https://software.ecmwf.int/wiki/download/attachments/'
               '3473472/emos_000392.tar.gz')
BOOST_URL = ('http://sourceforge.net/projects/boost/files/boost/1.57.0/'
             'boost_1_57_0.tar.gz/download')


def build_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('settings_url', help='The full URL to the giosystem'
                        'settings app. Example: http://gio-gl.meteo.pt/'
                        'giosystem/settings/api/v1/')
    return parser


def install_boost(gio_host):
    found = _find_lib(gio_host, 'libboost_date_time') and\
            _find_lib(gio_host, 'libboost_regex')
    if not found:
        print("Could not find boost. Downloading and compiling...")
        build_dir = os.path.join(gio_host.code_dir, 'build')
        if not os.path.isdir(build_dir):
            os.makedirs(build_dir)
        boost_name = BOOST_URL.split('/')[-2]
        file_name = boost_name.replace('.tar.gz', '')
        _download_lib(gio_host, build_dir, BOOST_URL, download_name=boost_name)
        gio_host.run_program('tar -zxvf {}.tar.gz'.format(file_name),
                             working_directory=build_dir)
        gio_host.run_program('./bootstrap.sh --prefix={} --with-libraries='
                             'date_time,regex'.format(gio_host.code_dir),
                             working_directory=os.path.join(build_dir,
                             file_name), shell=True)
        gio_host.run_program('./b2 install', working_directory=os.path.join(
                             build_dir, file_name), shell=True)
        gio_host.remove_directory(build_dir)


def install_hdf5(gio_host):
    install_zlib(gio_host)
    install_szip(gio_host)
    found = _find_lib(gio_host, 'libhdf5')
    if not found:
        print('could not find hdf5. Downloading and compiling...')
        output_path = gio_host.code_dir
        flags = 'CFLAGS="-std=c99" --enable-fortran --enable-cxx ' \
                '--enable-threadsafe --enable-unsupported --with-szlib={} ' \
                '--with-zlib={}'.format(output_path, output_path)
        _compile_lib(gio_host, HDF5_URL, flags=flags)


def install_zlib(gio_host):
    found = _find_lib(gio_host, 'libz')
    if not found:
        print('could not find zlib. Downloading and compiling...')
        _compile_lib(gio_host, ZLIB_URL)


def install_szip(gio_host):
    found = _find_lib(gio_host, 'libsz')
    if not found:
        print('could not find szip. Downloading and compiling...')
        url = 'https://bitbucket.org/ipmagio/giosystem-packages-tools/' \
              'downloads/szip-2.1.tar.gz'
        _compile_lib(gio_host, SZIP_URL)


def install_emoslib(gio_host):
    found = _find_lib(gio_host, 'libemosR64.a')
    if not found:
        print('could not find emoslib. Downloading and compiling...')
        _compile_emos_lib(gio_host, EMOSLIB_URL)


def _find_lib(gio_host, file_prefix):
    path = os.path.join(gio_host.code_dir, 'lib', file_prefix)
    found = any(gio_host.find(path))
    return found


def _compile_lib(gio_host, url, flags=''):
    archive_name = url.rpartition('/')[-1].partition('.tar')[0]
    #archive_name = url.rpartition('/')[-1].partition('.tar.gz')[0]
    build_dir = os.path.join(gio_host.code_dir, 'build')
    if not os.path.isdir(build_dir):
        os.makedirs(build_dir)
    _download_lib(gio_host, build_dir, url)
    #gio_host.run_program('tar -zxvf {}.tar.gz'.format(archive_name),
    gio_host.run_program('tar -{}xvf {}.tar{}'.format(\
    'z' if '.gz' in url.rpartition('/')[-1] else '', archive_name,\
    '.gz' if '.gz' in url.rpartition('/')[-1] else ''),
                         working_directory=build_dir)
    gio_host.run_program('./configure --prefix={} {}'.format(gio_host.code_dir,
                         flags), working_directory=os.path.join(build_dir, 
                         archive_name), shell=True)
    gio_host.run_program('make', working_directory=os.path.join(build_dir,
                         archive_name), shell=True)
    gio_host.run_program('make install', working_directory=os.path.join(
                         build_dir, archive_name), shell=True)
    gio_host.remove_directory(build_dir)


def _compile_emos_lib(gio_host, url, flags=''):
    '''
    This function will try to execute the build_library, which is an ECMWF
    developped script. The script is interactive. Reply to the questions with:

    * Do you want to work with GNU gfortran/gcc compiler? **YES**
    * Do you want 64 bit reals? **YES**
    * Do you want to build with grib_api? **NO**
    * Default directory for the tables? Choose the <gio_host.code_dir>/lib
      directory
    '''

    archive_name = url.rpartition('/')[-1].partition('.tar.gz')[0]
    build_dir = os.path.join(gio_host.code_dir, 'build')
    if not os.path.isdir(build_dir):
        os.makedirs(build_dir)
    _download_lib(gio_host, build_dir, url)
    gio_host.run_program('tar -zxvf {}.tar.gz'.format(archive_name),
                         working_directory=build_dir)
    emos_build_dir = os.path.join(build_dir, archive_name)
    gio_host.run_program('./build_library', working_directory=emos_build_dir)
    gio_host.run_program('./install', working_directory=emos_build_dir)
    gio_host.remove_directory(build_dir)


def _download_lib(gio_host, target_dir, url, download_name=None):
    if download_name is None:
        download_name = url.rpartition('/')[-1]
    file_name = download_name.partition('.tar.gz')[0]
    target_path = os.path.join(target_dir, file_name)
    if not any(gio_host.find(target_path)):
        cmd = 'wget {}'.format(url)
        if download_name is not None:
            cmd = '{} --output-document={}'.format(cmd, download_name)
        gio_host.run_program(cmd, working_directory=target_dir)
    else:
        print('file {} already accessible, skipping download'.format(
              file_name))
    return target_path


def main():
    parser = build_parser()
    args = parser.parse_args()
    giosystemcore.settings.get_settings(args.settings_url)
    host = hostfactory.get_host()
    install_hdf5(host)
    install_emoslib(host)
    install_boost(host)


if __name__ == '__main__':
    main()
