giosystemcore.packages.quicklooks
=================================

.. autoclass:: giosystemcore.packages.quicklooks.StaticQuicklookBase
   :show-inheritance:
   :member-order: bysource
   :members:

.. autoclass:: giosystemcore.packages.quicklooks.LstStaticQuicklook
   :show-inheritance:
   :member-order: bysource
   :members:

.. autoclass:: giosystemcore.packages.quicklooks.SwiStaticQuicklook
   :show-inheritance:
   :member-order: bysource
   :members:

