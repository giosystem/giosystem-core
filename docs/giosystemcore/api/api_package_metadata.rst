giosystemcore.packages.metadata
===============================

.. autoclass:: giosystemcore.packages.metadata.MetadataCreator
   :show-inheritance:
   :member-order: bysource
   :members:
