.. giosystemcore documentation master file, created by
   sphinx-quickstart on Tue Jan 14 15:46:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

giosystem documentation
=======================

giosystem is the name of the software system used for managing the processing
lines and related functionality of the IPMA Copernicus Global Land project. It
consists of serveral components:

:doc:`giosystemcore/index`
    giosystemcore is a package that facilitates working with files and 
    algorithms used in the IPMA GIO-GL processing lines.

:doc:`giosystemdjangobase/index`
    A django project that forms a base for installing the various web
    applications that are part of giosystem.
    It includes a django-pywps, which is a WPS server used to process 
    IPMA Copernicus GL products on demand.

:doc:`giosystemdjangobase/coresettings`
    A web application that is used to store settings for the processing lines.
    These settings include hosts, file paths and patterns, processing packages,
    processing line execution dependencies, etc.

    It provides a web accessible API that can be used to query the system's 
    settings programatically. This API is used by `giosystemcore` for creating
    all of the specialized classes.

:doc:`giosystemdjangobase/downloads`
    A web application that provides individual products, quicklooks, etc to web
    clients on demand. It responds to HTTP requests, delivering the products to
    the requesting clients.

:doc:`giosystemdjangobase/operations`
    A web application that stores information on the various tasks performed by
    the processing lines. It stores information on the execution status and
    results of the tasks defined in the processing lines.

:doc:`giosystemprocessing/index`
    A group of algorithms implemented in Fortran and C++ that perform the
    actual product generation.
    These algorithms are available as individual projects. The installation of
    these algorithms into the giosystem framework is managed and configured in
    the giosystem-processing component.
    Each algorithm is managed individually in its own repository on bitbucket.

:doc:`giosystemecflow/index`
    ecFlow processing lines. This package interfaces with the `ecflow`_
    software, managing its configuration for the processing lines of the
    giosystem.

.. _ecflow: https://software.ecmwf.int/wiki/display/ECFLOW/Home

:doc:`giosystemorders/index`
    An ordering server (OGC OSEO) that responds to order requests. It
    interfaces with the external GIO-GL portal and is responsible for managing
    and processing order and subscription requests for the products generated
    at IPMA's processing lines.

These components interact with each other, forming the whole giosystem big
picture. The following diagram provides a general overview on this interaction.

.. digraph:: giosystem
   :alt: giosystem components interaction

   "giosystem-django-base" [shape="box"];
   "django-giosystem-settings" [shape="box"];
   "django-giosystem-downloads" [shape="box"];
   "django-giosystem-completed-tasks" [shape="box"];
   "django-pywps" [shape="box"];
   "giosystem-core" [shape="box"];
   "giosystem-ecflow" [shape="box"];
   "giosystem-product-algorithms" [shape="box"];
   "giosystem-orders" [shape="box"];
   "django-giosystem-settings" -> "giosystem-core";
   "django-giosystem-settings" -> "giosystem-ecflow";
   "giosystem-core" -> "giosystem-ecflow";
   "giosystem-core" -> "django-giosystem-downloads";
   "giosystem-core" -> "django-pywps";
   "giosystem-product-algorithms" -> "django-pywps";
   "giosystem-ecflow" -> "giosystem-pywps";


Contents:

.. toctree::
   :maxdepth: 4
   :hidden:

   giosystemcore/index
   giosystemdjangobase/index
   giosystemecflow/index
   giosystemdjangobase/coresettings
   giosystemprocessing/index
   giosystemecflow/index
   giosystemorders/index
   giosystem_code
   debugging
   workshop/index
   api_teste/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

