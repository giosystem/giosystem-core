giosystem-django-base
=====================

The giosystem includes several web applications:

* django-giosystem-settings
* django-giosystem-completed-tasks
* django-giosystem-web
* django-pywps

As indicated by their names, all these applications are django applications. As
such, they must be installed inside a django project. Giosystem-django-base is
a predefined django project suitable for installing all these apps.

It can be installed in each of the machines that are part of the giosystem
network and configured to include the necessary apps.

Installation
------------

Detailed installation instructions are available at the projects source code
repository:

http://bitbucket.org/ipmagio/giosystem-django-base
