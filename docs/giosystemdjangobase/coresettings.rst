Introduction to giosystem-settings-django
=========================================

Giosystem-settings-django is a django application that stores the settings for
the giosystem. It has an administration web interface that can be used to edit
the settings. It also exposes a REST API that can be queried programatically in
order to retrieve the settings from any host. This makes it possible to use the
same settings for all the giosystem hosts.

The settings app can be installed in any host. This provides a convenient way
to both having:

* A set of operational settings, available on a secure host. These settings are
  used by all hosts that execute product generating packages in the main
  processing lines. They are rarely edited and access controlled, providing
  a stable and secure environment for production.

* Other settings instances, installed in development machines, that can be used
  for testing

Therefore, when working with the giosystem code, we must decide on what 
settings app will be used and provide the URL to its REST API to the
:py:func:`giosystemcore.settings.get_settings` function.

Operational settings host
-------------------------

Currently the operational settings are available at the gio-gl host (only
accessible from within IPMA's private network).

    http://gio-gl.meteo.pt/giosystem/settings

In order to access the administration interface, a specific user account must
be used. Authentication credentials are available internally at GIO-GL's `web
based collaboration tool`_

.. _web based collaboration tool: http://geo4.meteo.pt/fengoffice

Installation
------------

Detailed installation instructions are available at the projects 
`source code repository`_

.. _source code repository: https://bitbucket.org/ipmagio/giosystem-settings-django

Editing the settings
--------------------

The settings can be inspected and edited using the online administration
interface. It is available at http://gio-gl.meteo.pt/giosystem/settings/admin/

REST API
--------

The settings can be queried by using the API provided at
http://gio-gl.meteo.pt/giosystem/settings/api/v1/ This API is generated using
the `django tastypie`_ project.

.. _django tastypie: http://django-tastypie.readthedocs.org/en/latest/
