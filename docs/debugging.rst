Debugging giosystemcore errors
==============================

In order to debug giosystemcore troubles it is usefull to use an interactive
ipython shell.

.. code:: bash

   cd <giosystem_code_location>
   source venv/bin/activate
   ipython

Whenever there is some error during a package execution, it is usually usefull
to instantiate the package and check that both its properties and those of its
inputs and outputs are set correctly.

.. code:: python

   import giosystemcore.settings
   import giosystemcore.packages.packagefactory as pf
   giosystemcore.settings.get_settings('<settings_url>')
   p = pf.get_package('<troublesome_package_name>', '<troublesome_timeslot>')
   # check that the package and its files have the expected timeslots
   p.timeslot
   [f.timeslot for f in p.inputs + p.outputs]
   # check that the search paths and search patterns are correct
   inp = p.inputs[0]
   inp.search_path, inp.search_pattern
   # check whether the io_buffer and archive are being used as expected
   p.use_io_buffer_for_searching
   p.use_archive_for_searching
   # make sure not to copy any of these debug outputs to the io_buffer or the
   # archive
   p.copy_outputs_to_io_buffer = False
   p.copy_outputs_to_archive = True
   # try to find the inputs
   found = p.find_inputs()
   # check where is the working directory for the package
   p.working_dir
   # make sure not to remove the working directory
   p.remove_working_directory = False
