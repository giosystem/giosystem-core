import io
from os.path import dirname, join

from setuptools import setup, find_packages


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()


setup(
    name='giosystemcore',
    version=read("VERSION"),
    description='',
    long_description='',
    author='Ricardo Silva',
    author_email='ricardo.silva@ipma.pt',
    url='',
    classifiers=[''],
    platforms=[''],
    license='',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'install_giosystem_hdf5 = giosystemcore.scripts.installhdf5:main',
            ('install_giosystem_algorithms = giosystemcore.scripts.'
             'installalgorithms:main'),
            'fetch_copsystem_inputs = giosystemcore.scripts.fetchinputs:main',
        ],
    },
    include_package_data=True,
    install_requires=[
        'ecdsa',
        'paramiko',
        'pexpect',
        'pycrypto',
        'pysftp',
        'requests',
        'python-dateutil',
        'six',
        'wsgiref',
        'pystache',
        'owslib',
        'Pillow',
        'pycountry',
        'sphinx_rtd_theme',
        'pytest',
    ]
)
